package Plugins::BBCiPlayer::BBCOnAir;

# Plugin to play live and on demand BBC radio streams
# (c) Triode, 2007-2015, triode1@btinternet.com
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Module: BBCOnAir.pm
# Function: Experimental http polling to obtain on-air BBC radio programme
#           information, possibly with real-time track artist/title.
#           Based on code from RadioVis.pm.
# Created: 04/2015 by utgg
#

use strict;

use base qw(Slim::Utils::Accessor);

use Scalar::Util qw(weaken);

use Slim::Networking::Async;
use Slim::Utils::Errno;
use JSON::XS::VersionOneAndTwo;

use Data::Dumper;

use Slim::Utils::Log;
use Slim::Utils::Prefs;

__PACKAGE__->mk_accessor( rw   => qw(path session text rpid segments) );
__PACKAGE__->mk_accessor( weak => qw(song) );

use constant POLL_INTERVAL => 30; # Polling interval in seconds

my $log   = logger('plugin.bbciplayer.bbconair');
my $prefs = preferences('plugin.bbciplayer');

my %radioplayerid = (
	"bbc_radio_london"     				=> 300,
	"bbc_radio_berkshire"     			=> 301,
	"bbc_radio_bristol"     			=> 302,
	"bbc_radio_cambridge"     			=> 303,
	"bbc_radio_cornwall"     			=> 304,
	"bbc_radio_coventry_warwickshire"	=> 305,
	"bbc_radio_cumbria" 				=> 306,
	"bbc_radio_derby"     				=> 307,
	"bbc_radio_devon"     				=> 308,
	"bbc_radio_essex"     				=> 309,
	"bbc_radio_gloucestershire"     	=> 310,
	"bbc_radio_guernsey"     			=> 311,
	"bbc_radio_hereford_worcester"		=> 312,
	"bbc_radio_humberside"     			=> 313,
	"bbc_radio_jersey"     				=> 314,
	"bbc_radio_kent"     				=> 315,
	"bbc_radio_lancashire"     			=> 316,
	"bbc_radio_leeds"     				=> 317,
	"bbc_radio_leicester"     			=> 318,
	"bbc_radio_lincolnshire"  			=> 319,
	"bbc_radio_manchester" 				=> 320,
	"bbc_radio_merseyside"				=> 321,
	"bbc_radio_newcastle" 				=> 322,
	"bbc_radio_norfolk"     			=> 323,
	"bbc_radio_northampton"     		=> 324,
	"bbc_radio_nottingham"     			=> 325,
	"bbc_radio_oxford"     				=> 326,
	"bbc_radio_sheffield"     			=> 327,
	"bbc_radio_shropshire"     			=> 328,
	"bbc_radio_solent"     				=> 329,
	"bbc_radio_somerset_sound"      	=> 330,
	"bbc_radio_stoke"     				=> 331,
	"bbc_radio_suffolk" 				=> 332,
	"bbc_radio_surrey"      			=> 333,
	"bbc_radio_sussex" 					=> 334,
	"bbc_radio_wiltshire" 				=> 335,
	"bbc_radio_york"      				=> 336,
	"bbc_tees"      					=> 337,
	"bbc_three_counties_radio"			=> 338,
	"bbc_wm"   	   						=> 339,
	"bbc_radio_one"       				=> 340,
	"bbc_1xtra"           				=> 341,
	"bbc_radio_two"       				=> 342,
	"bbc_radio_three"     				=> 343,
	"bbc_radio_fourfm"     				=> 344,
	"bbc_radio_five_live" 				=> 345,
	"bbc_radio_five_live_sports_extra" 	=> 346,
	"bbc_6music"      					=> 347,
#	"bbc_???"      					=> 348,
	"bbc_asian_network"      			=> 349,
	"bbc_world_service"      			=> 350,
	"bbc_radio_scotland_fm"				=> 351,
	"bbc_radio_nan_gaidheal"			=> 352,
	"bbc_radio_ulster" 	    	 		=> 353,
	"bbc_radio_foyle" 					=> 354,
	"bbc_radio_wales_fm"   				=> 355,
	"bbc_radio_cymru"     				=> 356,
	"bbc_radio_fourlw"    				=> 357,
	"bbc_radio_four_extra"     			=> 358,
	"bbc_radio_cymru_2"    				=> 375,
	"bbc_radio_scotland_mw"				=> 372,
	"cbeebies_radio"					=> 376,
# New station not in plugin menu.opml yet
#	solent west dorset 370
#    shetland 373
#    orkney 374
#
	
	);
#  Was going to map resolutiononto a an acceptable but it seems all standard ones are OK.  Errors happen for non standard such as 320x240
my %acceptable_track_resolutions = (
		'160x160'  =>  '160x160',
		'240x240'  =>  '240x240',
		'320x320'  =>  '320x320',
		'480x480'  =>  '480x480',
		'160x90'   =>  '160x90',
		'240x135'  =>  '240x135',
		'320x180'  =>  '320x180',
		'480x270'  =>  '480x270',
		'640x360'  =>  '640x360',
		);

my $bbcsoundsicon = 'https://ichef.bbci.co.uk/images/ic/{recipe}/p078716v.jpg';

sub new {
	my $ref      = shift;
	my $path     = shift;
	my $song     = shift;
	my $segments = shift;

	my $self = $ref->SUPER::new;

	$log->info("bbconair: $path");
	my $weakself = $self;
	weaken($weakself); # to ensure object gets destroyed as this is stored inside parser

	$self->session(int(rand(1000000)));
	$self->path($path);
	$self->song($song);
	$self->rpid($radioplayerid{$path});
	$self->segments($segments);

	# swap the lines functions for all synced players
	if (1) {
		for my $client ($song->owner->allPlayers) {

			$log->debug("storing lines functions for $client");

			my $info = $client->pluginData('bbconair') || {};

			$info->{'session'}  = $self->session;
			$info->{'custom'} ||= $client->customPlaylistLines;
			$info->{'lines2'} ||= $client->lines2periodic;

			$client->pluginData('bbconair', $info);

			$client->customPlaylistLines(\&ourNowPlayingLines);
			$client->lines2periodic(\&ourNowPlayingLines);

			if (Slim::Buttons::Common::mode($client) =~ /playlist|screensaver/) {
				# force our lines if already in Playlist display
				$client->lines(\&ourNowPlayingLines);
			}
		}
	}

	# set title for url in case getCurrentTitle is called (avoiding standardTitle as we mix up artist title etc)
	Slim::Music::Info::setCurrentTitle($song->can('streamUrl') ? $song->streamUrl : $song->{'streamUrl'}, $song->track->title);

   # Initial fetch - will poll at regular intervals thereafter
	$weakself->_fetchOnAir();

	return $self;
}

sub _fetchOnAir {
	my $self = shift;
	my $song = $self->song || return;
	if (!$song->owner->isPlaying && $song->pluginData('bbconair')) {
		# force destruction of bbconair object if no longer playing
		# but only after we've actually returned from new!
		$log->info(" force destruction of BBCOnAir object as no longer playing");		
		$song->pluginData->{'bbconair'} = undef;
		return;
	}

	$log->info("Current Segments Flag=".$self->segments. "   Path=" . $self->path . " RadioPlayerid=" . $radioplayerid{$self->path});

	if ($self->segments) {
		my $segment_url = "https://rms.api.bbc.co.uk/v2/services/" . $self->path . "/segments/latest?limit=1" ;
		$log->info("Fetching Radio segments info $segment_url");

		Slim::Networking::SimpleAsyncHTTP->new(
			\&_parseSegments,
			sub {
			# If http fails and nothing playing, poll doing nothing until the stream finishes
			# It's better than destroying our own object, as we'll just come back again!
				if (!$song->owner->isPlaying) {
					$log->error("error fetching BBC segments data but nothing is playing so pollAfterError");
					$self->_pollAfterError();
				} else {
					$log->error("error fetching BBC segments data while playing so try fetching again");
				}
			},
			{
				cache   => 0, # disallow cacheing of responses
				obj     => $self,
			},
		)->get($segment_url);
	};
	
	my $url = "https://np.radioplayer.co.uk/qp/v3/events?rpId=". $self->rpid . "&nameSize=200&descriptionSize=200";
	$log->info("Fetching radioplayer $url");

	Slim::Networking::SimpleAsyncHTTP->new(
		\&_parseOnAirRadioPlayer,
		sub {
			# If http fails and nothing playing, poll doing nothing until the stream finishes
			# It's better than destroying our own object, as we'll just come back again!
			if (!$song->owner->isPlaying) {
				$log->error("error fetching RadioPlayer BBC data but nothing is playing so pollAfterError");
				$self->_pollAfterError();
			} else {
				$log->error("error fetching RadioPlayer BBC  data while playing so try fetching again");
				Slim::Utils::Timers::setTimer($self, Time::HiRes::time() + POLL_INTERVAL, \&_fetchOnAir);
			}
		},
		{
			cache   => 0, # disallow cacheing of responses
			obj     => $self,
		},
	)->get($url);
	
}

sub _parseSegments {
	my $http = shift;
	my $self = $http->params('obj');
	my $song = $self->song || return;
	
	my $segments_from_json = eval { from_json($http->content) };

	if ( $@ || $segments_from_json->{error} ) {
		$log->warn("error parsing BBCSegments json data" . $@ . $segments_from_json->{error});
		# If we get an error, poll doing nothing until the stream finishes
		# It's better than destroying our own object, as we'll just come back again!
		$self->_pollAfterError();
		return;
	}

	if (!$song->owner->isPlaying) {
		# force destruction of bbconair object if no longer playing
		$log->info(" force destruction of BBCOnAir object as nothing is playing");		
		$song->pluginData->{'bbconair'} = undef;
		return;
	}
	if ($segments_from_json->{'total'} eq 0) {
		$log->info( "No segments from ".$http->url)  ;
		$song->pluginData->{'track' } = undef;
		$song->pluginData->{'artist'} = undef;
		$song->pluginData->{'info'}   = undef; # need to do this to clear key
	} else {
#
#  Normal only one segment returned as limit=1 used in URL
#		
		my $topsegment = $segments_from_json->{'data'}->[0];
		my $when      =  $topsegment->{'offset'}->{'label'} ;
		
# To allow for player buffering - if track was played within last 1 minute  - assume it is current track
		
		if (($when eq 'Now Playing') || ($when eq 'Less Than a Minute Ago') ) {  #  more than 1 minute seems to be too long "||($when eq '1 Minute Ago'"
			$song->pluginData->{'track' } = $topsegment->{'titles'}->{'secondary'};
			$song->pluginData->{'artist'} = $topsegment->{'titles'}->{'primary'};
			my $icon = $topsegment->{'image_url'};

#  For many track there is no image and so a BBC Sounds icon appears - this bit of ciode tries to use program icon if defined. 
			$icon = $song->pluginData->{'rp_icon'} if (($icon eq $bbcsoundsicon) && defined($song->pluginData->{'rp_icon'})) ;
			
			my $imageres = $prefs->get('imageres');
			$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\{recipe\}/ichef.bbci.co.uk\/images\/ic\/$imageres\//;
			$song->pluginData->{'icon'} = $icon ;

		} else {
			$song->pluginData->{'track' } = undef;
			$song->pluginData->{'artist'} = undef;
			$song->pluginData->{'info'}   = undef; # need to do this to clear key
			$song->pluginData->{'icon'} =  undef ;
			$song->pluginData->{'vfd_livetext'} = undef;
		}
	}
}


sub _parseOnAirRadioPlayer {
	my $http = shift;
	my $self = $http->params('obj');
	my $song = $self->song || return;

	my $metadata = eval { from_json($http->content) };
	if ( $@ || $metadata->{error} ) {
		$log->warn("error parsing radioplayer metadata json data" . $@ . $metadata->{error});
		# If we get an error, poll doing nothing until the stream finishes
		# It's better than destroying our own object, as we'll just come back again!
		$self->_pollAfterError();
		return;
	}

	if ($metadata->{'total'} != 1) {
		$log->error (" Unexpected RP results ". Dumper(\$metadata));
		$self->_pollAfterError();
		return;
	}
	
	if (!$song->owner->isPlaying) {
		# force destruction of bbconair object if no longer playing
		$log->info(" force destruction of BBCOnAir object as nothing is playing");
		$log->error(" force destruction of BBCOnAir object as nothing is playing");
		$song->pluginData->{'bbconair'} = undef;
		return;
	}
	
	my $nowresult = $metadata->{'results'}->{'now'};
	if (! defined ($nowresult) ){
		$log->warn("error no Now result in radioplayer metadata json data" . Dumper(\$metadata));
		# If we get an error, poll doing nothing until the stream finishes
		# It's better than destroying our own object, as we'll just come back again!
		$self->_pollAfterError();
	}
	if ($nowresult->{'type'} eq 'PI' ) {
		$song->pluginData->{'rp_starttime'}   = $nowresult->{'startTime'};
		$song->pluginData->{'rp_stoptime'}    = $nowresult->{'stopTime'};
		$song->pluginData->{'rp_name'}        = $nowresult->{'programmeName'};
		$song->pluginData->{'rp_description'} = $nowresult->{'programmeDescription'};
		$song->pluginData->{'rp_servicename'} = $nowresult->{'serviceName'};
		$song->pluginData->{'rp_icon'}        = $nowresult->{'imageUrl'};

# Fix up the program icon resolution.
		
		my $imageres = $prefs->get('imageres');
		$song->pluginData->{'rp_icon'} =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;

		$log->info ( sprintf("UTC diff %6.2f secs  start/stop %10d/%10d Service %12s Name %20s Descr %20s", 
							$song->pluginData->{'dash_timediff'}, $song->pluginData->{'rp_starttime'},  $song->pluginData->{'rp_stoptime'}  ,
							'"'.$song->pluginData->{'rp_servicename'}.'"' ,'"'.$song->pluginData->{'rp_name'}.'"','"'.$song->pluginData->{'rp_description'} . '"')
				);
	} else {
		delete $song->pluginData->{'rp_starttime'};
		delete $song->pluginData->{'rp_stoptime'};
		delete $song->pluginData->{'rp_name'} ;
		delete $song->pluginData->{'rp_description'};
		delete $song->pluginData->{'rp_servicename'};
		delete $song->pluginData->{'rp_icon'};
	}


	
	$song->owner->master->currentPlaylistUpdateTime(Time::HiRes::time());
	my $notify = Slim::Utils::Versions->compareVersions($::VERSION, "7.5") >= 0 ? [ 'newmetadata' ] : [ 'playlist', 'newsong' ];
	Slim::Control::Request::notifyFromArray($song->owner->master, $notify);
	
	# Simply update (as soon as we can) every 30 seconds or so
	Slim::Utils::Timers::setTimer($self, Time::HiRes::time() + POLL_INTERVAL, \&_fetchOnAir);
}


sub _pollAfterError {
	my $self = shift;
	my $song = $self->song || return;
	$log->info("Poll after error called ");
	if (!$song->owner->isPlaying && $song->pluginData('bbconair')) {
		$log->error(" Poll after Error - Force destruction of BBCOnAir object as nothing is playing");
		# force destruction of bbconair object if no longer playing
		# but only after we've actually returned from new!
		$log->info(" Poll after Error - Force destruction of BBCOnAir object as nothing is playing");
		$song->pluginData->{'bbconair'} = undef;
		return;
	}

	Slim::Utils::Timers::setTimer($self, Time::HiRes::time() + POLL_INTERVAL, \&_pollAfterError);
}

sub ourNowPlayingLines {
	my $client = shift;
	my $args   = shift || {};

	if (!Slim::Buttons::Playlist::showingNowPlaying($client) && !$args->{'screen2'}) {
		# fall through to show other items in playlist
		return Slim::Buttons::Playlist::lines($client, $args);
	}

	my $parts;

	my $song = $client->streamingSong;
	if (!$args->{'trans'} && $song && $song->pluginData && (my $self = $song->pluginData->{'bbconair'})) {
		
#
# If SB3 UI is only UI active then this routine have to call iplayer getmetadafor to update the vfd_title & vfd_livetext value.
#		
		my $url = $song->track()->url ;
		my ($type, $params) = $url =~ /iplayer:\/\/(live|aod)\?(.*)$/;

		my $title    =  $song->pluginData('vfd_title');
		my $livetext =  $song->pluginData('vfd_livetext');

# Only call getmetadatafor if it is a "live"  - audio-on-demand (aod) has metadata already setup 
# and getmetadatfor is not being called by other UIs.

		if ($type eq 'live' && ($title eq ''))  {
			
				$log->info(" Calling getmetadatafor - assumed no other UI active ");

# 				have 1st param as undef - this will be $class value - as normally called from a handler.  
				Plugins::BBCiPlayer::iPlayer::getMetadataFor(undef, $client, $url);
				$title    =  $song->pluginData('vfd_title');
				$livetext =  $song->pluginData('vfd_livetext');

#				Reset the variable for next time as a check no other UI is active.
				$song->pluginData('vfd_title','');
		}
		
		my ($complete, $queue) = $client->scrollTickerTimeLeft($args->{'screen2'} ? 2 : 1);#
		
		my $ticker;
		if (defined($livetext)) {
			$ticker = $complete == 0 ? $livetext : "";
		} else {
			$ticker = $complete == 0 ? $title    : "";
		}

		if ($prefs->get('livetxt_classic_line') == 0) {
			# scrolling on top line
			$parts = {
				line    => [ undef, $title ],
				overlay => [ undef, $client->symbols('notesymbol') ],
				ticker  => [ $ticker, undef ],
			};
		} else {
			# scrolling on bottom line (normal scrolling line)
			$parts = {
				line    => [ $title, undef ],
				ticker  => [ undef, $ticker ],
			};

			$client->nowPlayingModeLines($parts);
		}

		# special cases for Transporter second display
		if ($args->{'screen2'}) {
			$parts = { screen2 => $parts };
		} elsif ($client->display->showExtendedText) {
			$parts->{'screen2'} = {};
		}

	} else {

		$parts = $client->currentSongLines($args);
	}

	return $parts;
}

sub DESTROY {
	my $self  = shift;
	my $path  = $self->path;

	$log->info("close: $path");

	for my $client (Slim::Player::Client::clients()) {

		my $info = $client->pluginData('bbconair');

		next unless $info && $info->{'session'} == $self->session;

		$log->debug("restoring lines functions for $client");

		# reset current lines if in playlist mode
		if (Slim::Buttons::Common::mode($client) =~ /playlist|screensaver/) {
			$client->lines($info->{'custom'} || \&Slim::Buttons::Playlist::lines);
		}

		$client->customPlaylistLines($info->{'custom'});
		$client->lines2periodic($info->{'lines2'});
	}
}

1;
