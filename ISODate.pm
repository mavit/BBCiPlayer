
package Plugins::BBCiPlayer::ISODate;
#
# Based on HTTP::Date but remove checks for negative epoch time (secs before 1970).
# This does nto handle all ISO8601 date formats 
# nor are patterns wholly correct for UTF \d means any numeric in any language should be replaced by [0-9]
#

use strict;
require Time::Local;

my %GMT_ZONE = (GMT => 1, UTC => 1, UT => 1, Z => 1);

sub iso2time ($;$)
{
    my $str = shift;
    return undef unless defined $str;

    my @d = parse_isodate($str);
    return undef unless @d;
    $d[1]--;        # month

    my $tz = pop(@d);
    unless (defined $tz) {
	unless (defined($tz = shift)) {
	    return eval { my $frac = $d[-1]; $frac -= ($d[-1] = int($frac));
			  my $t = Time::Local::timelocal(reverse @d) + $frac;
#			  $t < 0 ? undef : $t;
			};
		}
    }

    my $offset = 0;
    if ($GMT_ZONE{uc $tz}) {
	# offset already zero
    }
    elsif ($tz =~ /^([-+])?(\d\d?):?(\d\d)?$/) {
		$offset = 3600 * $2;
		$offset += 60 * $3 if $3;
		$offset *= -1 if $1 && $1 eq '-';
	}
	else {
		eval { require Time::Zone } || return undef;
		$offset = Time::Zone::tz_offset($tz);
		return undef unless defined $offset;
	}

	return eval { my $frac = $d[-1]; $frac -= ($d[-1] = int($frac));
		  my $t = Time::Local::timegm(reverse @d) + $frac;
#		  $t < 0 ? undef : $t - $offset;
	};
}


sub parse_isodate ($)
{
    local($_) = shift;
    return unless defined;

    # More lax parsing below
    s/^\s+//;  # kill leading space

    my($day, $mon, $yr, $hr, $min, $sec, $tz, $ampm);

    # ISO 8601 format '1996-02-29 12:00:00 -0100' and variants
    (($yr, $mon, $day, $hr, $min, $sec, $tz) =
	/^
	  (\d{4})              # year
	     [-\/]?
	  (\d\d?)              # numerical month
	     [-\/]?
	  (\d\d?)              # day
	 (?:
	       (?:\s+|[-:Tt])  # separator before clock
	    (\d\d?):?(\d\d)    # hour:min
	    (?::?(\d\d(?:\.\d*)?))?  # optional seconds (and fractional)
	 )?                    # optional clock
	    \s*
	 ([-+]?\d\d?:?(:?\d\d)?
	  |Z|z)?               # timezone  (Z is "zero meridian", i.e. GMT)
	    \s*$
	/x)

    ||
    return;  # unrecognized format

    # Translate month name to number
    $mon = ($mon =~ /^\d\d?$/ && $mon >= 1 && $mon <= 12 && int($mon)) ||
           return;

    # If the year is missing, we assume first date before the current,
    # because of the formats we support such dates are mostly present
    # on "ls -l" listings.
    unless (defined $yr) {
		my $cur_mon;
		($cur_mon, $yr) = (localtime)[4, 5];
		$yr += 1900;
		$cur_mon++;
		$yr-- if $mon > $cur_mon;
    }
    elsif (length($yr) < 3) {
	# Find "obvious" year
		my $cur_yr = (localtime)[5] + 1900;
		my $m = $cur_yr % 100;
		my $tmp = $yr;
		$yr += $cur_yr - $m;
		$m -= $tmp;
		$yr += ($m > 0) ? 100 : -100
	    if abs($m) > 50;
    }

    # Make sure clock elements are defined
    $hr  = 0 unless defined($hr);
    $min = 0 unless defined($min);
    $sec = 0 unless defined($sec);

    return($yr, $mon, $day, $hr, $min, $sec, $tz)
	if wantarray;

    if (defined $tz) {
		$tz = "Z" if $tz =~ /^(GMT|UTC?|[-+]?0+)$/;
    }
    else {
		$tz = "";
    }
    return sprintf("%04d-%02d-%02d %02d:%02d:%02d%s",
		   $yr, $mon, $day, $hr, $min, $sec, $tz);
}

