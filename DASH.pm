package Plugins::BBCiPlayer::DASH;

# DASH protocol handler
#
# (c) Triode, 2015, triode1@btinternet.com, B. Alton
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;

use base qw(IO::Handle);

use Slim::Utils::Errno;
use Slim::Utils::Log;
use Slim::Utils::Prefs;
use XML::Simple;
use HTTP::Date;
use URI;
use POSIX qw(ceil);

use URI::Escape qw(uri_unescape);
use Encode;

use Plugins::BBCiPlayer::DashUtils ;
use Plugins::BBCiPlayer::ISODate ;

use Slim::Utils::Versions;

use AnyEvent::HTTP;

use Data::Dumper;

use bytes;

use constant PRE_FETCH => 2; # number of chunks to prefetch
use constant SEGMENT_QUOTA => 2 ;  # Number of segements to fetch if On Demand.
use constant FIRST_DEC_2015 =>  1448928000;  # 1st Dec 2015 in Epoch time

use constant DASH_FETCHDELAY_WARNING => 1000; # msecs
use constant DASH_PERSISTENT_TIMEOUT => 20;
use constant HTTP_GET_TIMEOUT        => 30;
use constant DASH_FLAC_FUDGE         => 2; # 50
use constant UTC_1_Jan_2017          => 1483228800;

use constant IO_SELECT_FIXED => (Slim::Utils::Versions->compareVersions($::VERSION, '7.9.1') >= 0);

my $log = logger('plugin.bbciplayer.dash');
my $prefs = preferences('plugin.bbciplayer');

Slim::Player::ProtocolHandlers->registerHandler('dash', __PACKAGE__);
Slim::Player::ProtocolHandlers->registerHandler('dashflac', __PACKAGE__);

my $codecIds = {
	"mp4a.40.1" => "AAC",
	"mp4a.40.2" => "AAC LC",
	"mp4a.40.5" => "AAC SBR",
	"flac"      => "Flac",
};
#  Maps pref settings into DASH speed values.
my  @dashspeeds = (5000000, 10000, 320000, 128000, 96000, 48000); # Highest & lowest are arbitrary values than highest/lowest used by BBC

my $useragentstring = Slim::Utils::Misc::userAgentString() ;

sub uri_unescape_utf8 {
	my $str = shift;
	return Encode::decode('utf8', uri_unescape($str));
}

sub new {
	my $class = shift;
	my $args = shift;

	my $song     = $args->{'song'};
	my $url      = ($song->can('streamUrl') ? $song->streamUrl : $song->{'streamUrl'}) || $args->{'url'};
	my $seekdata = $song->can('seekdata') ? $song->seekdata : $song->{'seekdata'};
	my $start    = $seekdata->{'timeOffset'};

# if playing a dynamic streams liveskipbackstate can be either undefined, pending, live or active
#	undefined - only when stream is just started normally - will be initialised to live
#   live  is playing in realtime - no delay.
#   pending - skip back request in progress
#   active - live streams is playing with a delay 
#    
#
	$log->info(" New DASH stream - Jump to time - seek offset = $start");
	
	$log->info(sprintf("  -- Skip back state : \"%7s\"  time : %12s  req : %12s  streamtime: %12s" ,
				(defined ($song->pluginData('liveskipbackstate')) ? $song->pluginData('liveskipbackstate') : '----'), 
				(defined ($song->pluginData('liveskipbacktime'))  ? $song->pluginData('liveskipbacktime') : '----') , 
				(defined ($song->pluginData('liveskipbackreq'))   ? $song->pluginData('liveskipbackreq') : '----')  ,  $song->pluginData('streamtime')));
	            
	if (!defined($song->pluginData('liveskipbackstate'))) {
		$song->pluginData('liveskipbackstate','live');
		$song->pluginData('liveskipbacktime',0);
		
	} elsif ($song->pluginData('liveskipbackstate') eq 'pending') {
		$song->pluginData('liveskipbackstate','active');
#
# Do sanity check on time skip
#
#		Check is skip time is delta or exact skip to program start - if delta make sure it is less than 2 hrs.
		my $skipbacktime = $song->pluginData('liveskipbacktime');
		if ($song->pluginData('liveskipbackreq') < FIRST_DEC_2015) {
			$skipbacktime += $song->pluginData('liveskipbackreq');
			$log->info(" Timeshift buffer depth = " . $song->pluginData('timeShiftBufferDepth') . " skipbacktime =$skipbacktime");
			$skipbacktime = $song->pluginData('timeShiftBufferDepth') if ( $skipbacktime > $song->pluginData('timeShiftBufferDepth')); #  Cannot be greater than timeshift bufferdepth
		} else {
			$skipbacktime = $song->pluginData('liveskipbackreq');
		}

		$song->pluginData('liveskipbacktime', $skipbacktime);
		$song->pluginData('liveskipbackreq',0);
	} 

	if ($start) {
		if($song->can('startOffset')) {
			$song->startOffset($start);
		} else {
			$song->{startOffset} = $start;
		}
		$args->{'client'}->master->remoteStreamStartTime(Time::HiRes::time() - $start);
	}

	$log->info(sprintf("  -- Skip back state : \"%7s\"  time : %12s  req : %12s  streamtime: %12s" ,
				(defined ($song->pluginData('liveskipbackstate')) ? $song->pluginData('liveskipbackstate') : '----'), 
				(defined ($song->pluginData('liveskipbacktime'))  ? $song->pluginData('liveskipbacktime') : '----') , 
				(defined ($song->pluginData('liveskipbackreq'))   ? $song->pluginData('liveskipbackreq') : '----')  ,  $song->pluginData('streamtime')));
	$url =~ s/^dashflac/http/;
	$url =~ s/^dash/http/;
	$url =~ s/\|$//;

	$log->debug("open $url $start");

	my $self = $class->SUPER::new;

	${*$self}{'song'}              = $song;
	${*$self}{'_chunks'}           = [];
	${*$self}{'_pl_noupdate'}      = 0;
	${*$self}{'_played'}           = 0;
	${*$self}{'_pl_mpdfetch'}      = undef;
	${*$self}{'_startoffset'}      = $start if ($start) ;
	${*$self}{'_liveskipbacktime'} = $song->pluginData('liveskipbacktime') ;
	if (  $args->{'url'} =~/^dashflac/ ) {
		${*$self}{'contenttype'}	 = 'flc';	
		$song->pluginData('contenttype','flc');
	} else {
		${*$self}{'contenttype'}	 = 'aac';	
		$song->pluginData('contenttype','aac');
	}

	$self->_fetchMPD($url, $start);

	return $self;
}


#
#  Parse XSD Duration time 
#
#     PT0H3M1.63S
#
#   Only deal with Hours, Mins and secs ans return secs.
#
sub parse_duration {
    my $xsd_duration = shift;
    my ($neg, $year, $month, $day, $hour, $min, $sec, $fracsec);

    if ($xsd_duration =~ /^(-)?
                          P
                          ((\d+)Y)?
                          ((\d+)M)?
                          ((\d+)D)?
                          (
                          T
                          ((\d+)H)?
                          ((\d+)M)?
                          (((\d+)(\.(\d+))?)S)?
                          )?
                         $/x)
    {
        ($neg, $year, $month, $day, $hour, $min, $sec, $fracsec) =
        ($1,   $3,    $5,      $7,   $10,   $12,  $15,  $17);
        unless (grep {defined} ($year, $month, $day, $hour, $min, $sec)) {
            $log->error("Duration contained no duration ". $xsd_duration);
            return;
        }
    } else {
        $log->error( "Duration string does not match standard: ". $xsd_duration);
        return;
    }
    
    my $duration_secs = $hour * 3600 + $min * 60  + $sec  + "0.$fracsec";
    
    return $duration_secs;
}

sub _parseUTCTime {

	my $http = shift;
	my $self = $http->params('obj');
	my $url  = $http->params('url');
	my $xml       = $http->params('xml');
	my $mpd_url   = $http->params('mpd_url');
	my $start     = $http->params('start');
	my $refetch   = $http->params('refetch');

	$log->debug("Received content:" . $http->content);
	my ($year, $month, $day, $hour, $min, $sec, $tz) = HTTP::Date::parse_date($http->content);
	my $epochtime = str2time($http->content);
#	my $epochtime = Plugins::BBCiPlayer::ISODate::iso2time($http->content);
#	$log->error("Parsed ISO time epoch=$epochtime  y=$year m=$month d=$day h=$hour m=$min s=$sec z=$tz ");

	${*$self}{'_dash_utctime'}   = $epochtime;
	${*$self}{'_dash_timediff'}  = ${*$self}{'_dash_utctime'} -  Time::HiRes::time() ;
	$log->info(" Epochtime=$epochtime Time diff = ". ${*$self}{'_dash_timediff'});
	my $song = ${*$self}{'song'};
	$song->pluginData('dash_timediff', ${*$self}{'_dash_timediff'});
	$self->_parseMPD_part2($xml, $mpd_url, $start, $refetch);
}

sub _getUTCTime {
	my $self = shift;
	my $url  = shift;
	my $xml  = shift;
	my $mpd_url = shift;
	my $start   = shift;
	my $refetch = shift;
	
	$log->debug("fetch UTCTime: $url");

	Slim::Networking::SimpleAsyncHTTP->new(
		\&_parseUTCTime, 
		sub { 
			my $http  = shift;
			my $error = shift;
			my $error_url   = $http->url;
			my $self = $http->params('obj');
			$log->warn("error fetching time  $url - $error");
			if ( $error =~ /^(Connect timed out)/i ) {
				$log->error("- DASH asynchttp failed Get from $error_url - $error  ");
			} else {
				$log->error(" Stream closing error fetching UTC time from $error_url - $error  ");
				$self->close;
			}
		}, 
		{ obj => $self, url => $url, xml => $xml, mpd_url => $mpd_url, start => $start, refetch =>  $refetch },
	)->get($url);
}	

sub _make_BaseUrl {

	my $mpdurl  = shift;
	my $baseurl = shift;
	
	if ($baseurl =~ /^http:\/\//) {
	  return $baseurl;
	}
	  
	my $url = URI->new_abs($baseurl,$mpdurl);
	return $url->canonical->as_string;
}

sub _calc_liveedge {
	
	my $fetch_time             = shift;
	my $availability_starttime = shift;
	my $segment_duration_secs  = shift;
	my $start_number		=    shift;

	my $live_edge              = $start_number + int(($fetch_time - $availability_starttime) / $segment_duration_secs) - 2;
	$log->debug ("Live edge value = $live_edge  availability start time $availability_starttime");
	return $live_edge;

}

sub _processInitUrlTemplate {
	my $seg_template          = shift;
	my $seg_representation_id = shift;

	my $rep_id_pat     = '\$RepresentationID\$';
	
	my $result = $seg_template;

    if ($seg_representation_id) {
		$result =~ s/$rep_id_pat/$seg_representation_id/g;
	}
	return $result;
}

sub _processUrlTemplate {
	my $seg_template          = shift;
	my $seg_representation_id = shift;
	my $seg_bandwidth         = shift; 
	my $seg_time              = shift;
	my $seg_number            = shift;
	
	my $result = $seg_template;
	my $value;
	
	my $rep_id_pat     = '\$RepresentationID\$';
	my $bandwidth_pat  = '\$Bandwidth\$';
	my $time_pat       = '\$Time\$';
	
    if ($seg_representation_id) {
		$result =~ s/$rep_id_pat/$seg_representation_id/g;
	}

    if ($seg_bandwidth) {
		$result =~ s/$bandwidth_pat/$seg_bandwidth/g;
	}

    if ($seg_time ) {
		$result =~ s/$time_pat/$seg_time/g;
    }
		
    if ($seg_number) {
		my $nstart = 0;
        while( 1 ) {
			$nstart = index($result,'$Number$', $nstart);
			if ($nstart == -1) {
				$nstart = index($result,'$Number%', $nstart);
				last if ($nstart == -1);
			}
						  
			my $nend = index($result,'$', $nstart+1) ;
            if ($nend >= 0){
                my $var = substr($result,$nstart+1,$nend - $nstart -1);

                if ($var =~ m/^Number%/ ) {
                    $value = sprintf(substr($var,6),(int($seg_number)));
				} else {
                    $value = $seg_number;
                }
                my $pat = '\$' . $var .'\$';
				$result =~ s/$pat/$value/;
			}
			$nstart = $nstart +1;
		}
    }          
    $result =~ s/\$\$/\$/g;
    return $result ;
}

sub _makeInitUrl {
	
	my $seg_template          = shift;
	my $seg_representation_id = shift;
	my $mpdurl  			  = shift;
	my $baseurl 			  = shift;

    my $result         = _processInitUrlTemplate ( $seg_template,$seg_representation_id);
	my $segmentbaseurl = _make_BaseUrl($baseurl,$mpdurl);
  
	$result = URI->new_abs($result,$segmentbaseurl);
	return $result->as_string;
}

sub _UpdateChunkList {

	my $template 		        = shift @_;
	my $timediff 				= shift @_;
	
	my $repId                  	= $template->{'representationId'};
	my $bandwidth               = $template->{'bandwidth'};
	my $media					= $template->{'media'};
	my $segmentbaseurl			= $template->{'segmentbaseurl'};
	my $currentNumber			= $template->{'currentNumber'};
	my $finalNumber				= $template->{'finalNumber'};
	
	my @chunklist;
		
	my $lastnum   = $currentNumber + $template->{'segmentquota'};
	$lastnum = $template->{'finalNumber'} if ($lastnum > $template->{'finalNumber'});

	my $epochtime = Time::HiRes::time()  ;

	my $liveedge = $template->{'startNumber'} + int( ($epochtime + $timediff - $template->{'availabilitystarttime'}) / ( $template->{'segmentDuration'} / $template->{'timescale'}))- 2 ;

	$lastnum = $liveedge + 1 if ( ($template->{'dashtype'} eq 'dynamic') && ($liveedge < $lastnum));
	
	for (my $segnum = $template->{'currentNumber'}; $segnum <= $lastnum; $segnum++){
		my $result    =  _processUrlTemplate ( $media ,$repId, $bandwidth, 0, $segnum);
		my $seg_url   = URI->new_abs($result,$segmentbaseurl)->as_string;
		push @chunklist, { url => $seg_url, chunknum => $segnum };
	}

	$log->debug("timediff =".$timediff. " Live edge =".$liveedge . "lastnum=". $lastnum . " diff=". ($liveedge-$lastnum) . " currentplusQuota=". ($currentNumber + $template->{'segmentquota'}));
	$template->{'currentNumber'} = $lastnum;
	
  return @chunklist;	
}	

sub _fetchMPD {
	my $self = shift;
	my $url  = shift;
	my $start= shift || 0;

	$log->info("fetch playlist: $url start: $start, refetch=" . ${*$self}{'_pl_refetch'});

	my $old_refetch = ${*$self}{'_pl_refetch'} ;
	${*$self}{'_pl_refetch'} = undef;	

# Save guard object so that timer etc get cancelled if stream is stopped when ${*$self} object is destroyed.
	${*$self}{'_pl_mpdfetch'} = http_request (	GET => $url,
					headers => { 'User-Agent' => $useragentstring },
					timeout => HTTP_GET_TIMEOUT,
					
					sub {
							my ($body, $hdr) = @_;
							if ($hdr->{'Status'} eq '200') {
								_parseMPD($body, $hdr, $self, $url, $start, $old_refetch);
							} else {
								$log->error("Closing stream - Fetch MPD error HTTP status = ". $hdr->{'Status'} . " Reason: ". $hdr->{'Reason'});
						  		${*$self}{'_pl_refetch'} = time() + 10;
								${*$self}{'_pl_url'}  = $url;
								${*$self}{'_pl_mpdfetch'} = undef;
								$self->close;
							}
					});
}

 sub _parseMPD {

	my ($body, $hdr, $self, $url, $start, $refetch) = @_;

	my $mpd_url = $hdr->{'URL'};

	my $is_debug = $log->is_debug;

	$is_debug && $log->debug("got: $url start: $start  refetch=$refetch");
	
	my $xml = eval {
		XMLin( 
			$body,
			KeyAttr => [],
			ForceContent => 1,
			ForceArray => [ 'AdaptationSet', 'Representation', 'Period' ]
		)
	};

	if ($@) {
		$log->error("$@");
		$self->close;
		return;
	}
	$log->debug("DASH Content \n" . Dumper($xml)) ;

#
#  MPD type  Static is BBC Listen Again - Dynamic is "live"
#
    my $mpd_profiles = $xml->{'profiles'};
    if ( $mpd_profiles !~ /urn:dvb:dash:profile:dvb-dash:isoff-ext-live:2014/) {
		$log->error("Unsupported mpd profile: $mpd_profiles");
		return;
	} 

	if ($xml->{'UTCTiming'}) {
# not sure what the difference between xsdate and and iso date but xs:date is not the "favored" declaration
		if (($xml->{'UTCTiming'}->{'schemeIdUri'} eq "urn:mpeg:dash:utc:http-iso:2014") || ( $xml->{'UTCTiming'}->{'schemeIdUri'} eq "urn:mpeg:dash:utc:http-xsdate:2014" )) {
			$self->_getUTCTime($xml->{'UTCTiming'}->{'value'},$xml,$mpd_url,$start,$refetch);
			return;
		} else {
			$log->error("Unsupported time scheme ". $xml->{'UTCTiming'}->{'schemeIdUri'});
			return;
		}
	} 
	$self->_parseMPD_part2 ($xml, $mpd_url, $start, $refetch);
}
#
#   For static program MPD parsing parseMPd2 is called as part of parseMPD.  
#   For dynamic where there is a need to get the server time before processing can continue.
#   So parseMPD_part2 is called from the routine which receives the time.
#

sub _parseMPD_part2 {
	my $self      = shift;
	my $xml       = shift;
	my $mpd_url   = shift;
	my $start     = shift;
	my $refetch   = shift;

	my $minimumUpdatePeriod ;
	my $timeShiftBufferDepth ;
	my $mediaPresentationDuration ;
	my $maxSegmentDuration ;    
	my $minBufferTime  ;     
	
	my $song = ${*$self}{'song'};
	$log->debug(" live skip back = ". ${*$self}{'_liveskipbacktime'});

	my $availabilityStartTime = Plugins::BBCiPlayer::ISODate::iso2time($xml->{'availabilityStartTime'});
	$log->info("Availability Starttime > 0 ( $availabilityStartTime  )" ) if $availabilityStartTime > 0;
	$song->pluginData('availabilitystarttime', $availabilityStartTime);
	my $mpd_dynamic = (exists($xml->{'type'})) && ($xml->{'type'} eq 'dynamic') ;

#  BBC uses availabilityStartTime to indicate start time of some live broadcast. 
#  As availabilityStartTime can sometimes have a negative value or if undefined then zero  first test to make it is a recent time.
 
	if ( $mpd_dynamic && ($availabilityStartTime > UTC_1_Jan_2017) ) {
		$log->info(" Live stream and an availabilitystartTime after 1-Jan-2017  $availabilityStartTime  utc =". ${*$self}{'_dash_utctime'});
		if( $availabilityStartTime > ${*$self}{'_dash_utctime'}) {
			$log->error(" Stream closed as not available until $xml->{'availabilityStartTime'} UTC= $availabilityStartTime current UTC time = ". ${*$self}{'_dash_utctime'} );
			$self->close;
			return;
		}
	}
	
	if ($mpd_dynamic)  { 
	
		$minimumUpdatePeriod       = parse_duration($xml->{'minimumUpdatePeriod'});
		$maxSegmentDuration        = parse_duration($xml->{'maxSegmentDuration'});
		$minBufferTime             = parse_duration($xml->{'minBufferTime'});

		$timeShiftBufferDepth      = parse_duration($xml->{'timeShiftBufferDepth'});
		$log->error(" Timeshift buffer not defined" ) unless exists($xml->{'timeShiftBufferDepth'});

		$song->pluginData('timeShiftBufferDepth', $timeShiftBufferDepth);
		${*$self}{'_dash_type'} = 'dynamic';
		$song->pluginData('transport', 'dash-dynamic');

		$log->info (" Live delay start pref=". $prefs->get('livedelaystart'));
		if (($prefs->get('livedelaystart') != 0) && (${*$self}{'_liveskipbacktime'} == 0) ) {
			$song->pluginData('liveskipbackstate','active');
			$song->pluginData('liveskipbacktime', int($prefs->get('livedelaystart')));
			${*$self}{'_liveskipbacktime'} = int($prefs->get('livedelaystart'));
		}

	} else {
		$mediaPresentationDuration = parse_duration($xml->{'mediaPresentationDuration'}) ;
		$maxSegmentDuration        = parse_duration($xml->{'maxSegmentDuration'}) ;
		$minBufferTime             = parse_duration($xml->{'minBufferTime'}) ;
		$log->info(" media presentation duration = $mediaPresentationDuration  secs");      
		${*$self}{'_dash_type'}    = 'static';

		$song->pluginData('transport', 'dash-static');
		
#		${*$self}{'_pl_noupdate'}  = 1;
		${*$self}{'song'}->duration(int($mediaPresentationDuration));
		
		my $remotetrack = Slim::Schema::RemoteTrack->fetch(${*$self}{'song'}->streamUrl);
		$remotetrack->secs(int($mediaPresentationDuration));		
	}
    
    $log->info("maxSegmentationDuration=$maxSegmentDuration      minbufferTime=$minBufferTime");
    $log->info("No of Period = ". scalar(@{$xml->{'Period'}})) ;
    if (scalar(@{$xml->{'Period'}}) != 1) {
		$log->error("Plugin currently support mpd with only one period.  Periods in this mpd  = ". scalar(@{$xml->{'Period'}}));
	} ;
	
    my $adaptationSet = $xml->{'Period'}[0]->{'AdaptationSet'} ; 
    
    my @reps =();
    
    foreach my $adaptation (@$adaptationSet) {
		
		if (($adaptation->{'contentType'} == 'audio'    ) 
		    && ($adaptation->{'mimeType'}    == 'audio/mp4') 
		    && ($codecIds->{$adaptation->{'codecs'}}         ) 
			) {
			
			foreach my $representation (@{$adaptation->{'Representation'}}) {
				push (@reps,{'id'             => $representation->{'id'}, 
				             'bandwidth'      => int($representation->{'bandwidth'}), 
				             'representation' => $representation, 
				             'adaptation'     => $adaptation,
				             'period'         => 0});
			}
	    }
	}
#
#  Choose a representation based on speed preference
#
#  0 = Min speed, 1 = Max speed, 2 = Max 320, 3 = Max 128, 4 = Max 96, 5= max 48
#

	my $speedpref = $prefs->get( ($mpd_dynamic ? 'Dashspeed_live' : 'Dashspeed_aod') ) ;
	if ($prefs->get( ($mpd_dynamic ? 'Dashspeed_live' : 'Dashspeed_aod') ) ) {
# Non zero means sort in descending order
		@reps = sort { $b->{'bandwidth'} <=> $a->{'bandwidth'}	} @reps;
	} else {
		@reps = sort { $a->{'bandwidth'} <=> $b->{'bandwidth'}	} @reps;
	};

#  check if specific max speed limit  - if so then @reps entries will be sorted in descending bandwidth
	if ($speedpref >=2 ) {
# Remove elements which are faster than max preferred speeds or until only one left
		while ( scalar @reps > 1)  {
			$log->error(" Checking entry ". $reps[0]->{'bandwidth'}  . " against ". $dashspeeds[$speedpref] );
			last if $reps[0]->{'bandwidth'} <= $dashspeeds[$speedpref] ;
			shift @reps;
		}
	}
	
    my $select_rep = $reps[0];
    
	$log->error('Pref used:'      .              ($mpd_dynamic ? 'Dashspeed_live' : 'Dashspeed_aod') . 
				' pref value : ' . $prefs->get( ($mpd_dynamic ? 'Dashspeed_live' : 'Dashspeed_aod')).
				' pref speeds:  ' .$dashspeeds[$prefs->get( ($mpd_dynamic ? 'Dashspeed_live' : 'Dashspeed_aod'))].
				' Chosen rep id '. $select_rep->{'id'} . 
				' Bandwidth = '  . $select_rep->{'bandwidth'} .
				' Codecs = '     . $select_rep->{'adaptation'}->{'codecs'});

	$song->pluginData('codec', $codecIds->{$select_rep->{'adaptation'}->{'codecs'}});

#  Set up threshold for diagnostic of late chunks
# for bandwidth 48kbps and 96kbps - allow 0.8 secs
	if ($select_rep->{'bandwidth'} < 100000) {
		${*$self}{'chunkdelay'}= 800; # 0.8 secs
# for bandwidth between 128kbps and 320kbps  - allow 1 sec 
	} elsif ($select_rep->{'bandwidth'} < 330000) {
		${*$self}{'chunkdelay'} = 1000;  #1 sec
	} else {
		${*$self}{'chunkdelay'} = 1500;  #1.5 secs - this is for Flac streams - bandwith 365000 and higher 
	};

	$log->info(" Bandwidth ".$select_rep->{'bandwidth'} . "Chunk delay ". ${*$self}{'chunkdelay'});
#
#  make up URL from Template and Base.
#
    my $baseUrl ;
    if    (defined($select_rep->{'representation'}->{'BaseURL'}->{'content'})) {
		$baseUrl = $select_rep->{'representation'}->{'BaseURL'}->{'content'} ;
	}
	elsif (defined($select_rep->{'adaptation'}->{'BaseURL'}->{'content'})) {
		$baseUrl = $select_rep->{'adaptation'}->{'BaseURL'}->{'content'} ;
	}
	elsif (defined($xml->{'Period'}[0]->{'BaseURL'}->{'content'})) {
		$baseUrl = $xml->{'Period'}[0]->{'BaseURL'}->{'content'};
	}
	elsif (defined($xml->{'BaseURL'}->{'content'})) {
		$baseUrl = $xml->{'BaseURL'}->{'content'}
	}	
	else {
		$log->info(" No BaseURL definition found");
	}
	
    $log->debug("Before make Baseurl = $baseUrl" ); 
    
    $baseUrl = _make_BaseUrl($mpd_url,$baseUrl);
    
    $log->info("After make Baseurl = $baseUrl" ); 
#
#  This is BBC - assume only SegmentTemplate in Adaptation Set
#   
    my $segment_template ; 
    if ( defined($select_rep->{'adaptation'}->{'SegmentTemplate'})) {
		$segment_template = $select_rep->{'adaptation'}->{'SegmentTemplate'};
	} else { 
		$log->error(" No expected SegmentTemple in Adaptation Set in rep id ".$select_rep->{'id'});
		return;

    }
	my $seg_representation_id = $select_rep->{'id'} ;
	if (!exists($segment_template->{'startNumber'})) {
		$log->info("startNumber not defined - setting it to 1");
		$segment_template->{'startNumber'} = "1";
	}

    my $segment_number = $segment_template->{'startNumber'} ;
	my $init_url       = $segment_template->{'initialization'} ;
	my $media_url      = $segment_template->{'media'} ;
	
	${*$self}{'_dash_template'} = {
		'startNumber'		=> int($segment_template->{'startNumber'}), 
		'initialization'	=> $segment_template->{'initialization'},
		'media'			=> $segment_template->{'media'},
		'segmentDuration'	=> $segment_template->{'duration'} ,
		'timescale'		=> $segment_template->{'timescale'} ,
		'bandwidth'		=> $select_rep->{'bandwidth'},
		'representationId'	=> $select_rep->{'id'},
		'availabilitystarttime' => $availabilityStartTime,
		'minimumUpdatePeriod'	=> $minimumUpdatePeriod,
		'timeShiftBufferDepth'	=> $timeShiftBufferDepth,
		'minBufferTime'		=> $minBufferTime ,
		'mediaPresentationDuration'   => $mediaPresentationDuration,
		'baseurl'		=> $baseUrl,
		'dashtype'		=> ${*$self}{'_dash_type'},
	};

	my @chunks = ();

	my $result	= _processInitUrlTemplate ( $segment_template->{'initialization'},$seg_representation_id);
	my $init_url	= URI->new_abs($result,$baseUrl)->as_string;
	${*$self}{'_dash_template'}->{'initurl'} = $init_url;
	$log->info("Init URL $init_url");
	
 	push @chunks, { url => $init_url, chunknum => 0 };

#
#   Time to process template for main body - only create a short playlist - add more when needed
#

#  How many segment in whole program
	my $segment_duration_secs  =  $segment_template->{'duration'} / $segment_template->{'timescale'}  ;

#   Create a list of duration larger of either 60 secs(1 mins) or minbuffertime
	my $num_segments  = ceil($mediaPresentationDuration / $segment_duration_secs)  ;
	my $segment_quota = int(($minBufferTime / $segment_duration_secs )) +1 ;
	$log->info("Segment quota = $segment_quota  type=".${*$self}{'contenttype'});

	if ( !$mpd_dynamic && (60 > $minBufferTime) ) {
		$segment_quota = int (60/ $segment_duration_secs);
	}
	
	if (defined(${*$self}{'_startoffset'})) {
		my $segmentsoffset = int(${*$self}{'_startoffset'} / $segment_duration_secs);
		${*$self}{'_dash_template'}->{'currentNumber'}    = int($segment_template->{'startNumber'}) + $segmentsoffset;
	} else {
		${*$self}{'_dash_template'}->{'currentNumber'}    = int($segment_template->{'startNumber'});
	}
	if ( $mpd_dynamic) {
		${*$self}{'_dash_template'}->{'currentNumber'}    = _calc_liveedge (${*$self}{'_dash_utctime'}, 
										$availabilityStartTime,$segment_duration_secs, 
										$segment_template->{'startNumber'}) ;
		if ($select_rep->{'adaptation'}->{'codecs'} eq 'flac') {
			${*$self}{'_dash_template'}->{'currentNumber'}  = ${*$self}{'_dash_template'}->{'currentNumber'}  - DASH_FLAC_FUDGE;
		}
		$log->debug(" Live Current segment number = ". ${*$self}{'_dash_template'}->{'currentNumber'});

# Check is skip forward back is delta or exact when skip to program start -   Exact times with live will be after 1 Dec 2015.
		if (${*$self}{'_liveskipbacktime'} > FIRST_DEC_2015) {  
			${*$self}{'_dash_template'}->{'currentNumber'} = int( ${*$self}{'_liveskipbacktime'}  / $segment_duration_secs);																	
			$log->info(" skip time to specific time = ". ${*$self}{'_liveskipbacktime'} . " currentNUmber=". ${*$self}{'_dash_template'}->{'currentNumber'});
		} else {
			$log->debug(" skip time - delta time = ". ${*$self}{'_liveskipbacktime'} . " currentnumber=" . ${*$self}{'_dash_template'}->{'currentNumber'} );
			${*$self}{'_dash_template'}->{'currentNumber'} -= int( ${*$self}{'_liveskipbacktime'}  / $segment_duration_secs);																	

		}
		$song->pluginData('startnumber', ${*$self}{'_dash_template'}->{'currentNumber'});
		$song->pluginData('segmentduration', $segment_duration_secs );
		$song->pluginData('streamtime', int( ${*$self}{'_dash_template'}->{'currentNumber'} * $segment_duration_secs  ));
		$log->info("Initial setting of streamtime to " . $song->pluginData('streamtime') . " using current number " . ${*$self}{'_dash_template'}->{'currentNumber'});
		$log->debug(" Skip Current segment number = ". ${*$self}{'_dash_template'}->{'currentNumber'});

		$num_segments = ${*$self}{'_dash_template'}->{'currentNumber'} + 5000000; # make live stream play a long time.
		$segment_quota = SEGMENT_QUOTA if ($segment_quota <= 1);
	}
	
	$log->info(" Starting Current number ". ${*$self}{'_dash_template'}->{'currentNumber'} . " start number ". int($segment_template->{'startNumber'}). " final number $num_segments" );
	${*$self}{'_dash_template'}->{'segmentquota'}     = int($segment_quota) ;
	${*$self}{'_dash_template'}->{'finalNumber'}      = int($num_segments);
	${*$self}{'_dash_template'}->{'representationId'} = $seg_representation_id;
	${*$self}{'_dash_template'}->{'bandwidth'}        = $select_rep->{'bandwidth'};
	${*$self}{'_dash_template'}->{'codecs'}           = $select_rep->{'adaptation'}->{'codecs'};
	${*$self}{'_dash_template'}->{'media'}            = $segment_template->{'media'};
	${*$self}{'_dash_template'}->{'segmentbaseurl'}   = $baseUrl;
	
	
	my $stream = ${*$self}{'song'}->streamUrl;
	my $track  = Slim::Schema::RemoteTrack->fetch($stream);
	$track->bitrate($select_rep->{'bandwidth'});
	my $songTrack = ${*$self}{'song'}->track;
	Slim::Music::Info::setBitrate($songTrack, $select_rep->{'bandwidth'},1); # VBR
	$AnyEvent::HTTP::PERSISTENT_TIMEOUT = DASH_PERSISTENT_TIMEOUT;

	push @chunks, _UpdateChunkList(${*$self}{'_dash_template'}, ${*$self}{'_dash_timediff'});
	$log->debug(" After Update list  Chunks ". Dumper(@chunks). "\n template ". Dumper(${*$self}{'_dash_template'}));
	
	if (scalar (@chunks) > 0 ) {
		${*$self}{'_chunks'} = \@chunks;

		for my $i(0 .. PRE_FETCH) {
			$self->_fetchChunk($chunks[$i]) if $chunks[$i];
		}
		${*$self}{'song'}->owner->master->currentPlaylistUpdateTime(Time::HiRes::time());
	}

	if (!${*$self}{'_pl_noupdate'}) {
		${*$self}{'_pl_refetch'} = time() + 10;
		$log->debug("refetch time ". ${*$self}{'_pl_refetch'} );		
		${*$self}{'_pl_url'}  = $mpd_url;
	}
}

sub _fetchChunk {
	my $self  = shift;
	my $chunk = shift;

	if (my $url = $chunk->{'url'}) {
#		$log->error("fetching [$chunk->{chunknum}]: $url");
		$chunk->{'fetching'} = 1;
		$chunk->{'requesttime'} = Time::HiRes::time();
		$chunk->{'requestobj'}  = http_request (	GET => $url,
						headers => { 'User-Agent' => $useragentstring },
						timeout => HTTP_GET_TIMEOUT,
					sub {
							my ($body, $hdr) = @_;
							my $fetchtime = int((Time::HiRes::time() - $chunk->{'requesttime'}) *1000);

#							Status of 20x means HTTP request completed OK
							if ($hdr->{'Status'} =~ /20[01234]/ ) { 
								if ($log->is_debug && ($fetchtime > DASH_FETCHDELAY_WARNING)) {
									$log->debug("Chunk fetch status ". $hdr->{'Status'} . " Long chunk fetch time $fetchtime" );
								} elsif ($fetchtime > ${*$self}{'chunkdelay'} ) {
									$log->error("Chunk fetch status ". $hdr->{'Status'} . " Long chunk fetch time $fetchtime   ". ${*$self}{'chunkdelay'}   );
								};
								$log->is_info && $log->info("Fetched [$chunk->{chunknum}] size " . length($body));
#								$log->error("Fetched chunk [$chunk->{chunknum}] size " . length($body));
								delete $chunk->{'fetching'};
								delete $chunk->{'requestobj'};
								$chunk->{'chunkref'} = \$body;
							} elsif ( ($hdr->{'Status'} >= 400) && ($hdr->{'Status'} <= 511)) {
								$log->error("Aborting stream - fatal error fetching [$chunk->{chunknum}] Status=" . $hdr->{'Status'} . " Reason: ". $hdr->{'Reason'}. " Fetch time $fetchtime ms ");
								$log->error("Failed URL: ".$hdr->{'URL'} );
								$log->debug("*************Dump of hdr \n". Dumper($hdr));
								$self->close;
								delete $chunk->{'fetching'};
								delete $chunk->{'requestobj'};
							} else {
								$log->error("Retrying after error fetching [$chunk->{chunknum}] Status=" . $hdr->{'Status'} . " Reason: ". $hdr->{'Reason'}. " Fetch time $fetchtime ms ");
								delete $chunk->{'fetching'};
								delete $chunk->{'requestobj'};
							}; 	
						}
			);
	}
}

sub isRemote { 1 }

sub isAudio { 1 }

sub canSeek {
	my ($class, $client, $song) = @_;

	$log->info("DASH can seek called ");
	return 1;
	return $song->duration ? 1 : 0;
}

sub canDoAction {
	my ( $class, $client, $url, $action ) = @_;
	my $song = $client->currentSongForUrl($url); 
	
	$log->info("DASH Action=$action after ". $client->songElapsedSeconds . " secs");
	$log->info(sprintf("  -- Skip back state : %7s  time : %12s  req : %12s  streamtime: %12s" ,
				(defined ($song->pluginData('liveskipbackstate')) ? $song->pluginData('liveskipbackstate') : '----'), 
				(defined ($song->pluginData('liveskipbacktime'))  ? $song->pluginData('liveskipbacktime') : '----') , 
				(defined ($song->pluginData('liveskipbackreq'))   ? $song->pluginData('liveskipbackreq') : '----')  ,  $song->pluginData('streamtime')));
	if ($action eq 'pause') {
		$song->pluginData('elapsedseconds',int($client->songElapsedSeconds));
		if ($song->pluginData('liveskipbackstate') eq 'live') {
			$song->pluginData('liveskipbackreq',($song->pluginData('streamtime') -18));
			$song->pluginData('liveskipbackstate','pending');			
		} 		
	};
	
	if ($action eq 'rew') {
		if ($song->pluginData('liveskipbackstate') eq 'active') {
			my $resumetime = $song->pluginData('liveskipbacktime') + $song->pluginData('elapsedseconds');
			$song->pluginData('liveskipbacktime', $resumetime) ;
			$song->pluginData('elapsedseconds',0);
		} elsif ($song->pluginData('liveskipbackstate') eq 'live') {
			$song->pluginData('elapsedseconds',0);
		}
	}

 return 1;	
}

sub getSeekData {
	my ($class, $client, $song, $newtime) = @_;
	$log->debug("Getseekdata $newtime");
	return { timeOffset => $newtime };
}

sub contentType { 
	my $self = shift;
	return ${*$self}{'contenttype'};
}


sub getIcon {
	my ($class, $url) = @_;

	my $handler;

	if ( ($handler = Slim::Player::ProtocolHandlers->iconHandlerForURL($url)) && ref $handler eq 'CODE' ) {
		return &{$handler};
	}
	
	return Plugins::BBCiPlayer::Plugin->_pluginDataFor('icon');
}

sub getMetadataFor {
	my $class  = shift;
	my $client = shift;
	my $url    = shift;

	$log->info(" DASH getmetadatafor $url");

	if ($client && (my $song = $client->currentSongForUrl($url))) {
		
		my $baseUrl = $song->pluginData('baseUrl');	
		$log->info(" plugindata BaseURl=$baseUrl");
		my ($type, $params) = $baseUrl =~ /iplayer:\/\/(live|aod)\?(.*)$/;
		my %params = map { $_ =~ /(.*?)=(.*)/; $1 => $2; } split(/&/, $params);
		my $title = uri_unescape_utf8($params{'title'}) || Slim::Music::Info::getCurrentTitle($client, $url) || Slim::Music::Info::title($url);
		my $desc  = uri_unescape_utf8($params{'desc'});
		my $icon = $params{'icon'};

		if ($type eq 'aod')  {
			my $imageres = $prefs->get('imageres');
			$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;			
	
			return {
				title    => $title,
				artist   => $desc,
				cover    => $icon,
				icon     => $icon,
			};
			
		} elsif ($type eq 'live')  {
			if ($song->pluginData('bbconair')  && ! $prefs->get('showmenuicon') && $song->pluginData->{'rp_icon'} ) {
				$icon = $song->pluginData->{'rp_icon'} ;
			}
	
			my $imageres = $prefs->get('imageres');
			$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;			
			return {
				title    => $title . ': '. $song->pluginData('track'),
				artist   => $song->pluginData('artist') ,
				album    => $song->pluginData('album') , 
				cover    => $icon,
				icon     => $icon,
			};
		}
	}
	return undef;
}


sub close {
	my $self = shift;
#	Slim::Utils::Misc::bt();
	$log->info(" DASH stream closing");
	${*$self}{'_close'} = 1;
	my $song = ${*$self}{'song'};
	if ($song->pluginData('liveskipbackstate') ne 'pending') {
		$song->pluginData('liveskipbacktime',0);
		$song->pluginData('liveskipbackstate','live');	
	}
}

sub sysread {
#
# $self                        = $_[0] 
# where data is to be returned = $_[1]
# count of max bytes           = $_[2]
# offset into $_[1]            = $_[3]
#  
	
	my $self  = $_[0];
	my $maxBytes = $_[2];
    my $offset = $_[3] || 0;


	if (${*$self}{'_close'}) {
		$log->info ("Stream closed for some reason");
		return 0;
	}
	my $chunks = ${*$self}{'_chunks'};
	my $chunkref;
	my $ret = 0;

	if (!defined(${*$self}{'_dash_type'} )) {
		$log->info("MPD not received - no DASH type defined");
		$! = EINTR;
		return undef;
	}
		
	$log->info("sysread - checking chunks : " . scalar @$chunks .  " played :" .${*$self}{'_played'} );

	my $song = ${*$self}{'song'};
	$song->pluginData('currentnumber', ${*$self}{'_dash_template'}->{'currentNumber'} );
#    $song->pluginData('streamtime', int( ${*$self}{'_dash_template'}->{'currentNumber'} * ( ${*$self}{'_dash_template'}->{'segmentDuration'} / ${*$self}{'_dash_template'}->{'timescale'}  )));
    if ( ${*$self}{'_played'} > 0 ) {
		 $song->pluginData('streamtime', int( ${*$self}{'_played'} * ( ${*$self}{'_dash_template'}->{'segmentDuration'} / ${*$self}{'_dash_template'}->{'timescale'}  )));
	}
    $log->debug("Setting of streamtime to " . $song->pluginData('streamtime') . " last played ". ${*$self}{'_played'} . " Current number ".${*$self}{'_dash_template'}->{'currentNumber'} );

    
    my $client = $song->master();
    
#
# Check if any chunks need to be converted from MPEG4 to ADTS/Flac to stream to SBs.

#  Jul 2017 - Special support for Binaural live stream.	
#  Binaural 
#         'ftyp' => {
#                     'minor_version' => 0,
#                     'compatible_brands' => [
#                                               'iso9',
#                                               'mp42',
#                                               'dash',
#                                               'caaa'
#                                             ],
#                      'brand' => 'iso9'
#                    }
#
#
#
#
#  Normal AAC
#         'ftyp' => {
#                      'minor_version' => 0,
#                      'compatible_brands' => [
#                                               'iso6',
#                                               'dash'
#                                             ],
#                      'brand' => 'iso6'
#                    },





	if (scalar @$chunks) {
		my %dashfile;
		if ($chunks->[0]->{'chunkref'}) {
			%dashfile = Plugins::BBCiPlayer::DashUtils::parseMpeg4(${$chunks->[0]->{'chunkref'}});

			if ((defined($dashfile{'ftyp'}))&& (($dashfile{'ftyp'}->{'brand'} eq 'iso6') || ($dashfile{'ftyp'}->{'brand'} eq 'iso9') ) ){
			
				$log->info( "Got ftyp dash Compatible brand=". $dashfile{'ftyp'}->{'compatible_brands'}[1]);
				my $dashheader = $dashfile{'moov'}->{'trak'}->{'mdia'}->{'minf'}->{'stbl'}->{'stsd'}->{'entries'};

				if (exists ( $dashheader->{'mp4a'}) && exists ( $dashheader->{'mp4a'}->{'esds'})) {
					${*$self}{'_dash_mp4esds'} = $dashheader->{'mp4a'}->{'esds'};		
				}

				if (exists ( $dashheader->{'fLaC'}) && exists ( $dashheader->{'fLaC'}->{'dfLa'}->{'metablockheader'})) {
					${*$self}{'_dash_mp4dash'} = $dashheader->{'entries'}->{'fLaC'}->{'dfLa'}->{'metablockheader'};		
					${*$self}{'_dash_mp4dashinit'} = undef;# 'fLaC';# . ${*$self}{'_dash_mp4dash'} ;	
				}

				my $played = shift @$chunks;
				${*$self}{'_played'} = $played->{'chunknum'};
				$log->is_info && $log->info("played [$played->{chunknum}]");
				$! = IO_SELECT_FIXED ? EWOULDBLOCK : EINTR;
				return undef;
			} elsif ( (defined($dashfile{'moof'}))) {
					if ( ${*$self}{'_dash_template'}->{'codecs'} eq 'flac') {
						$chunks->[0]->{'flacchunkref'} = Plugins::BBCiPlayer::DashUtils::convertDashSegtoFlac($dashfile{'mdat'}->{'data'}, 
								$dashfile{'moof'}->{'traf'}->{'trun'}); 
						$chunkref =  $chunks->[0]->{'flacchunkref'};
						$log->debug("flac converted length ". length($chunkref));
						
					}else {
						$chunks->[0]->{'adtschunkref'} = Plugins::BBCiPlayer::DashUtils::convertDashSegtoADTS(${*$self}{'_dash_mp4esds'},$dashfile{'mdat'}->{'data'}, 
								$dashfile{'moof'}->{'traf'}->{'trun'}); 
						$chunkref = $chunks->[0]->{'adtschunkref'};
						$log->debug("adts converted length ". length($chunkref));
					}
								
			} else {
				$log->error("Unexpected segment type  length chunkref ". 
					length(${$chunks->[0]->{'chunkref'}}) . "\n". Dumper($chunks->[0]));
				return 0;
			}
		}
	} elsif ( ${*$self}{'_played'} == ${*$self}{'_dash_template'}->{'finalNumber'}) {
			
		$log->debug("No more chunks left closing: last played " . 
		               ${*$self}{'_played'} . " last chunk num ". ${*$self}{'_dash_template'}->{'finalNumber'});
		return 0;
	} else {
		$log->debug("Last else chunks left". scalar(@$chunks). " last played " . 
		               ${*$self}{'_played'} . " last chunk num ". ${*$self}{'_dash_template'}->{'finalNumber'});
		
	}
#
#  Finally - service the sysread with data. 
#	
	if ($chunkref) {
		$_[1] = "";
		my $nextpos = $chunks->[0]->{'nextpos'} ||= 0;
		
#
#  Loop while still unused data in  current chunk and requestor needs more data
#

		if ($nextpos <= length($chunkref) && length($_[1]) < $maxBytes ) {
			my $availbytes = (length($chunkref) - $nextpos);
			$availbytes = $maxBytes if ($availbytes > $maxBytes); 
			$_[1] .= substr($chunkref, $nextpos, $availbytes);
			$nextpos += $availbytes;
			$ret  = $availbytes;
			$log->info ("sysread - returned $availbytes bytes - requested $maxBytes  nextpos =$nextpos length ". length($chunkref));
		}
#
#  Check to see if current chunk all used up
#
		if ($nextpos >= length($chunkref)) {
			my $played = shift @$chunks;
			${*$self}{'_played'} = $played->{'chunknum'};
			$log->is_info && $log->info("played [$played->{chunknum}]");
		} else {
			$chunks->[0]->{'nextpos'} = $nextpos;
			$log->debug ("sysread - update chunknum " . $chunks->[0]->{'chunknum'} . "  nextpos set to $nextpos");
		}
	}

#
#  Add more chunk to playlist - if count below x ) 

	if ( (${*$self}{'_dash_template'}->{'currentNumber'} < ${*$self}{'_dash_template'}->{'finalNumber'}) &&
		(scalar(@$chunks) < ${*$self}{'_dash_template'}->{'segmentquota'})) {

		${*$self}{'_dash_template'}->{'currentNumber'} ++;
		push @$chunks, _UpdateChunkList(${*$self}{'_dash_template'}, ${*$self}{'_dash_timediff'});
		$log->debug(" After sysread Update list  Chunks length". scalar(@$chunks). "\n template ". Dumper(${*$self}{'_dash_template'}));
	}	
			
	if (${*$self}{'_pl_refetch'} && time() > ${*$self}{'_pl_refetch'}) {
		$log->info(" ---------------------------------- refetch due time= ". time() . "   scheduled=". ${*$self}{'_pl_refetch'} );
		${*$self}{'_pl_refetch'} = time() +  10;
	}

	# fetch more chunks
	for my $i (0 .. PRE_FETCH) {
		my $new = $chunks->[$i];
		if ($new && !$new->{'chunkref'} && !$new->{'fetching'}) {
			$self->_fetchChunk($new);
			last;
		}
	}

	$log->debug(" DASH sysread returning $ret (" . length( $_[1] ) .")  utc time=". ${*$self}{'_dash_utctime'} );	
	return $ret if $ret;

	# otherwise come back later - use EINTR as we don't have a file handle to put on select
	# In July 2018 a bug in Select was fixed - so can returns EWOULDBLOCK nnecessary for no delay with live streams on Windows 10
	$! = IO_SELECT_FIXED ? EWOULDBLOCK : EINTR;
 		
	return undef;
}

1;
