package Plugins::BBCiPlayer::iPlayer;

# Plugin to play live and on demand BBC radio streams
# (c) Triode, 2007-2015, triode1@btinternet.com
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Modified: 04/2015 by utgg
#           Added http polling for OnAir text information
#

use strict;

use XML::Simple;
use URI::Escape qw(uri_unescape);
use Encode;
use JSON::XS::VersionOneAndTwo;

use Scalar::Util qw(blessed);
use Slim::Utils::Strings qw(cstring);

use Slim::Utils::Log;
use Slim::Utils::Prefs;

use Data::Dumper;

my $log   = logger('plugin.bbciplayer');
my $logonair   = logger('plugin.bbciplayer.bbconair');

my $prefs = preferences('plugin.bbciplayer');

Slim::Player::ProtocolHandlers->registerHandler('iplayer', __PACKAGE__);

use constant TTL_EXPIRE => 10;
use constant THREE_HOURS => 10800;

sub isRemote { 1 }


sub uri_unescape_utf8 {
	my $str = shift;
	return Encode::decode('utf8', uri_unescape($str));
}


sub canDoAction {
	my ( $class, $client, $url, $action ) = @_;
	if (lc($url) =~/^dash:\/\//) {
	  return  Plugins::BBCiPlayer::DASH::canDoAction($class, $client, $url, $action);	
	}
	
 return 1;	
}

# convert iplayer url into playlist of placeholder tracks which link to each playlist url without actually scanning
# each placeholder is only scanned when trying to play it so we avoid scanning multiple playlists for one item
sub scanUrl {
	my ($class, $url, $args) = @_;

	$log->info("$url");
	
	$log->debug("scanURL called $url");

	my $song  = $args->{'song'};
	my $track = $song->track;
	my $client = $args->{'client'};

	my ($type, $params) = $url =~ /iplayer:\/\/(live|aod)\?(.*)$/;
	my %params = map { $_ =~ /(.*?)=(.*)/; $1 => $2; } split(/&/, $params);

	my $playlist = Slim::Schema::RemoteTrack->fetch($url, 1);

	$song->pluginData(baseUrl => $url);

	my @prefOrder = $class->prefOrder($client, $type);

	$log->info("baseUrl: $url type: $type prefOrder:" . join(',', @prefOrder));

	# allow title to be overridden in iplayer url, or use existing title
	my $title = uri_unescape_utf8($params{'title'}) || Slim::Music::Info::getCurrentTitle($client, $url) || Slim::Music::Info::title($url);
	my $desc  = uri_unescape_utf8($params{'desc'});
	my $icon = $params{'icon'};

	if ($title) {
		$playlist->title($title);
	}
 
	# record duration in our RemotePlaylist object
	# Slim::Menu::TrackInfo obtains 'more-info' data from this object
	if ($params{'dur'}) {
		$playlist->secs($params{'dur'});
	}

	# convert pref order into playlist of ordered placeholder tracks which link to urls to scan
	# the playback code will call getNextTrack for each placeholder and allow it to be scanned when needed
	my @placeholders;

	for my $t (@prefOrder) {

		if ($params{$t}) {
			push @placeholders, { t => $t, url => "iplayer://$type?$t=$params{$t}&type=$t" };
		}

		if ($t =~ /flashaac|flashmp3|wma/ && $params{'ms'}) {
			push @placeholders, { t => $t, url => "iplayer://$type?ms=$params{ms}&type=$t" };
		}
	}

	my @pl;

	for my $placeholder (@placeholders) {

		$log->info("placeholder: $placeholder->{url}");

		my $ct = $placeholder->{'t'};
		$ct =~ s/flash//;
		$ct =~ s/hls/aac/;
		$ct =~ s/dashflac/flc/;
		$ct =~ s/dash/aac/;
		

		push @pl, Slim::Schema::RemoteTrack->updateOrCreate($placeholder->{'url'}, {
			title        => $title,
			secs         => $params{'dur'},
			content_type => $ct,
		});
	}
	$log->debug("Placeholders \n". Dumper(@placeholders));
	
	$log->debug("Playlist \n". Dumper(@pl));

	unless (scalar @pl) {
		# Can't find a suitable stream. Perhaps user's preferred playback types are too restrictive.
		$log->error("Can't find a suitable media URL within the iPlayer URL that matches one of the preferred playback types: " . join(',', @prefOrder));
		$log->error("iPlayer URL is: $url");
		# Precautionary measure - kill any timers that may have been created. I'm not sure that any will have been...
		Slim::Utils::Timers::killTimers($client, \&_checkStream);
		# Signal failure, and return forthwith. (We have been called by Slim::Player::Song::getNextSong.)
		
		$args->{'cb'}->(undef,$client->string('PLUGIN_BBCIPLAYER_MISSINGPLAYBACKFORMAT'));
		return;
	}

	$playlist->setTracks(\@pl);
	$song->_playlist(1);

	$args->{'cb'}->($playlist);

	Slim::Utils::Timers::killTimers($client, \&_checkStream);
	Slim::Utils::Timers::setTimer($client, Time::HiRes::time() + 1, \&_checkStream, $url);
}

sub getNextTrack {
	my $class= shift;
	my $song = shift;
	my $cb   = shift;
	my $ecb  = shift;

	my $track   = $song->currentTrack();
	my $baseUrl = $song->pluginData('baseUrl');

	my ($type, $handler, $url, $t) = $track->url =~ /iplayer:\/\/(live|aod)\?(.*?)=(.*?)&type=(.*)/;

	$log->info("scanning: $type: $handler $url for $t");

	my $callback = sub {
		my ($obj, $error) = @_;

		if ($obj) {

			my $playlist = Slim::Schema::RemoteTrack->fetch($baseUrl)->tracksRef;
			my $pl;

			if (ref $obj eq 'ARRAY') {
				$pl = $obj;
			} elsif (blessed $obj) {
				$pl = $obj->isa('Slim::Schema::RemotePlaylist') ? $obj->tracksRef : [ $obj ];
			}

			$log->info("scanned: $url");

			if (scalar @$pl) {

				my $highBr;
				my @highPl;
				my @lowPl;

				for my $foundTrack (@$pl) {
					$log->info(" found: " . $foundTrack->url . " bitrate: " . $foundTrack->bitrate);
					$highBr ||= $foundTrack->bitrate;
					if (!$highBr || $foundTrack->bitrate == $highBr) {
						push @highPl, $foundTrack;
					} else {
						push @lowPl, $foundTrack;
					}
				}

				# find ourselves in the existing playlist and splice in new high bitrate track objects after our placeholder
				# add remaining tracks to end of playlist so if all formats have been parsed we try lower bitrates 
				my $i = 0;
				for my $entry (@$playlist) {
					if ($entry->url eq $track->url) {
						$log->info("adding " . scalar @highPl . " tracks at placeholder, " . scalar @lowPl . " at end of playlist");
						splice @$playlist, $i+1, 0, @highPl;
						push @$playlist, @lowPl;
						last;
					}
					++$i;
				}
				
				# move on to first new track
				$song->_getNextPlaylistTrack();
				
				$cb->();
				return;
			}

		} elsif ($error) {
			$log->warn("error: $error");
		}

		$log->warn("no track objects found!");

		# move on to next placeholder track and recurse
		if ($song->_getNextPlaylistTrack()) {

			$class->getNextTrack($song, $cb, $ecb);
			return;
		}
		
		# no tracks left - return error
		$ecb->();
	};

	if ($handler =~ /hls/){
		# adjust url so protocol handler logic will pick hls handler
		$url =~ s/^https/hls/;
		$url =~ s/^http/hls/;
		$url =~ s/$/\|/;
		
		my $obj = Slim::Schema::RemoteTrack->updateOrCreate($url, {
			title        => $track->title,
			secs         => $track->secs,
			content_type => 'aac',
		});

		$callback->($obj);
		return;
	}
	if ($handler =~ /dashflac/){
		# adjust url so protocol handler logic will pick dashflac handler
		$url =~ s/^https/dashflac/;
		$url =~ s/^http/dashflac/;
		$url =~ s/$/\|/;
		
		my $obj = Slim::Schema::RemoteTrack->updateOrCreate($url, {
			title        => $track->title,
			secs         => $track->secs,
			content_type => 'flc',
		});

		$callback->($obj);
		return;
	}
	if ($handler =~ /dash/){
		# adjust url so protocol handler logic will pick dash handler
		$url =~ s/^https/dash/;
		$url =~ s/^http/dash/;
		$url =~ s/$/\|/;
		
		my $obj = Slim::Schema::RemoteTrack->updateOrCreate($url, {
			title        => $track->title,
			secs         => $track->secs,
			content_type => 'aac',
		});

		if ($type eq 'aod') {
			_getSegmentsInfo($url,$song);
		}

		$callback->($obj);
		return;
	}

	if ($handler =~ /aac|wma|mp3/) {
		Slim::Utils::Scanner::Remote->scanURL($url, { cb => $callback, song => $song, title => $track->title });
		return;
	}

	if ($handler eq 'ms') {
		Slim::Networking::SimpleAsyncHTTP->new(
			\&_msParse, 
			sub { 
				$log->warn("error fetching ms");
				$ecb->(); 
			}, 
			{ 
				cache   => 1, # allow cacheing of responses
				expires => TTL_EXPIRE,
				cb      => $callback,
				song    => $song,
				args    => { 
					title => $track->title, url => $url, type => $type, t => $t, dur => $track->secs,
				},
			},
		)->get($url);
		return;
	}

	$ecb->();
}

sub _checkStream {
	my ($client, $url) = @_;

	my $sc = $client->master->controller;
	my $streaming = $sc->streamingSong;

	if ($sc->streamingSong && $streaming->track->url eq $url) {

		$log->info("consecutiveErrors: $sc->{consecutiveErrors} songElapsed: " . $sc->playingSongElapsed);

		if ($sc->{'consecutiveErrors'} == 0 && $sc->playingSongElapsed >= 2) {

			$log->info("stream playing - disabling playlist state");

			$streaming->_playlist(0);

		} else {

			Slim::Utils::Timers::setTimer($client, Time::HiRes::time() + 1, \&_checkStream, $url);
		}
	}
}

sub _msParse {
	my $http = shift;
	my $cb   = $http->params('cb');
	my $song = $http->params('song');
	my $args = $http->params('args');
	my $update = $http->params('update');

	$log->info("parsing: " . $http->url);

	my $xml = eval { XMLin($http->contentRef, KeyAttr => undef, ForceArray => 'media' ) };
	if ($@) {
		$log->error("error: $@");
		$cb->();
		return;
	}

	my @streams;
	my $swf = '';

	for my $media (@{$xml->{'media'} || []}) {

		my ($type) = $media->{'type'} =~ /audio\/(.*)/;

		for my $c (@{$media->{'connection'} || []}) {

			if ($c->{'protocol'} eq 'http') {

				next unless ($type =~ /wma|x\-ms\-asf/ && $args->{'t'} eq 'wma' || 
							 $type eq 'mp4' && $args->{'t'} eq 'aac');

				push @streams, {
					type    => $args->{'t'},
					service => $media->{'service'},
					br      => $media->{'bitrate'},
					url     => $c->{'href'},
				};

			} elsif ($c->{'protocol'} eq 'rtmp') {

				if ($c->{'supplier'} eq 'akamai') {

					next unless ($type eq 'mp4'  && ($update || $args->{'t'} eq 'flashaac') || 
								 $type eq 'mpeg' && ($update || $args->{'t'} eq 'flashmp3'));

					my ($ct) = $args->{'t'} =~ /flash(aac|mp3)/;
					my $live = $c->{'application'} && $c->{'application'} eq 'live';
					my $appl = $c->{'application'} || "ondemand";

					push @streams, {
						supplier => $c->{'supplier'},
						type    => $args->{'t'},
						service => $media->{'service'},
						ct      => $ct,
						br      => $media->{'bitrate'},
						url     => Plugins::BBCiPlayer::RTMP->packUrl({
							host       => $c->{'server'},
							port       => 1935,
							swfurl     => $swf,
							streamname => "$c->{identifier}?$c->{authString}" . ($live ? "&aifp=v001" : ""),
							subscribe  => $live ? "$c->{identifier}" : undef,
							tcurl      => "rtmp://$c->{server}:1935/$appl?_fcs_vhost=$c->{server}&$c->{authString}",
							app        => "$appl?_fcs_vhost=$c->{server}&$c->{authString}",
							live       => $live,
							ct         => $ct,
							br         => $media->{'bitrate'},
							url        => "$args->{url}#$media->{service}",
							duration   => $args->{'dur'},
							update     => __PACKAGE__,
							ttl        => time() + TTL_EXPIRE,
						}),
					};

				} elsif ($c->{'supplier'} eq 'limelight') {

					next unless $type eq 'mp4' && ($update || $args->{'t'} eq 'flashaac');
					
					push @streams, {
						supplier => $c->{'supplier'},
						type    => 'flashaac',
						service => $media->{'service'},
						ct      => 'aac',
						br      => $media->{'bitrate'},
						url     => Plugins::BBCiPlayer::RTMP->packUrl({
							host       => $c->{'server'},
							port       => 1935,
							swfurl     => $swf,
							streamname => "$c->{identifier}",
							tcurl      => "rtmp://$c->{server}:1935/$c->{application}?$c->{authString}",
							app        => "$c->{application}?$c->{authString}",
							ct         => 'aac',
							br         => $media->{'bitrate'},
							url        => "$args->{url}#$media->{service}",
							duration   => $args->{'dur'},
							update     => __PACKAGE__,
							ttl        => time() + TTL_EXPIRE,
						}),
					};

				}
			}
		}
	}

	# refresh rtmp params only
	if ($update) {
		for my $stream (@streams) {
			if ($stream->{'service'} eq $update->{'stream'}) {
				$log->info("updated stream: $stream->{url}");
				$song->streamUrl($stream->{'url'});
				Slim::Music::Info::setRemoteMetadata($stream->{'url'}, {
					bitrate => $stream->{'br'},
					ct      => $stream->{'ct'},
				});
			}
		}
		$update->{'cb'}->();
		return;
	}

	# sort into bitrate order, preferring higher bitrates
	if ( $prefs->get('supplier_pref')) {
		@streams = sort { $a->{'supplier'} cmp $b->{'supplier'} or
				  $b->{'br'} <=> $a->{'br'} 
				} @streams;

	} else {
		@streams = sort { $b->{'supplier'} cmp $a->{'supplier'} or
				  $b->{'br'} <=> $a->{'br'} 
				} @streams;
	}
#	@streams = sort { $b->{'br'} <=> $a->{'br'} } @streams;

	$log->info("Dump of available streams ". Dumper(@streams));
	$log->info("Supplier pref ".$prefs->get('supplier_pref'));

	my @tracks;
	my $pending = 0;

	for my $stream (@streams) {

		if ($stream->{'type'} eq 'wma') {

			++$pending;
			Slim::Utils::Scanner::Remote->scanURL($stream->{'url'}, { song => $song, cb => sub {
				my ($obj, $error) = @_;
				push @tracks, $obj->isa('Slim::Schema::RemotePlaylist') ? $obj->tracks : $obj;
				if (!--$pending) {
					$cb->(\@tracks);
				}
			} });

		} else {

			push @tracks, Slim::Music::Info::setRemoteMetadata($stream->{'url'}, {
				title   => $args->{'title'},
				bitrate => $stream->{'br'},
				ct      => $stream->{'ct'},
			}),
		}
	}

	if ($pending == 0) {
		$cb->(\@tracks);
	}
}

sub update {
	my $class = shift;
	my $song  = shift;
	my $params= shift;
	my $cb    = shift;
	my $ecb   = shift;

	my ($url, $stream) = $params->{'url'} =~ /(.*)#(.*)/;
	$url ||= $params->{'url'};

	$log->info("update: $url $stream");

	Slim::Networking::SimpleAsyncHTTP->new(
		\&_msParse,
		sub { 
			$log->warn("unable to fetch xml feed: " . shift->url);
			$ecb->();
		},  
		{ song => $song, update => { stream => $stream, cb => $cb }, args => { url => $params->{'url'} } }
	)->get($url);
}


sub getMetadataFor {
	my $class  = shift;
	my $client = shift;
	my $url    = shift;

	unless ( $client ) {
		$log->error(" Unexpected - Client is undefined ");
		return {}
	} ;

# Get iplayer type 'live' or 'aod' and also any standard URL params after "?" 
	my ($type, $params) = $url =~ /iplayer:\/\/(live|aod)\?(.*)$/;
	my %params = map { $_ =~ /(.*?)=(.*)/; $1 => $2; } split(/&/, $params);

	my $params_title;
	my $params_desc;
	my $params_icon ; 
	
	$params_icon   = $params{'icon'};
	$params_title  = uri_unescape_utf8($params{'title'}) if (defined($params{'title'}));
	$params_desc   = uri_unescape_utf8($params{'desc'})  if (defined($params{'desc'}));

#  As supplied URL might be for a playlist item and one that is not actually streaming so use currentSongForUrl instead of streamingSong or streamingSongforUrl
	my $song = $client->currentSongForUrl($url) ;
	
	$logonair->info(" Playing Title:$params_title URL $url Current Track:" . ($params{'track'} eq 'on' ? 'Enabled' : 'Disabled'));

#  if $song is not defined - then get metadata for an item queued in playlist
	if (! $song ) {
		$logonair->debug(" Get Metadata for playlist item $url");
		if (! defined($params_title)) {
			my $trackObj = Slim::Schema->objectForUrl( { url => $url } );
			if ($trackObj && blessed $trackObj) {
				$params_title = $trackObj->title;
			} else {
				$params_title  = "------" ;
				$log->error("Couldn't determine title for $url");
			}	
		}
		
		return {
			title  => $params_title,
			artist => $params_desc,
			album  => '',
			cover  => $params_icon,
			icon   => $params_icon,
		};
	}
	
#  Get the streamtype - iplayer initially but will change shortly after stream starts playing and plugin has chosen which format to play.
	my $stream       = $song->streamUrl;
	my $track        = Slim::Schema::RemoteTrack->fetch($stream);

	my ($streamtype) = ( $stream =~ m/^(dash|hls|dashflac|http|https|iplayer):\/\//);

	my $icon    = $params_icon || $song->pluginData('icon') ;
	my $codec	= ($track && $track->content_type) ? $track->content_type : $song->pluginData('codec') ;
	my $bitrate = ($track && $track->bitrate)      ? (int ($track->bitrate/1000) . Slim::Utils::Strings::string('KBPS') . ' VBR')  : undef;

#  If AOD then no need to check for BBCOnAir - current track detaisl

	if ($type eq 'aod') {
		$logonair->debug(" Get Metadata for On-demand item $url");
		my $metainfo  = getcurrentdata($song , $client->songElapsedSeconds());
		my $albumtext ;

#  Add data for the More menu to indicate which segment of the On-Demand program is playing.
		if ( defined($metainfo)) {
			$albumtext = $metainfo->{'title'} ;
			$albumtext .= " - " . $metainfo->{'artist'} if (defined($metainfo->{'artist'}));
			$song->pluginData('playingseg',$albumtext);
			
#	Swap album & artist as it is better for Material display			
			($params_desc, $albumtext) = ($albumtext, $params_desc);
		};

		$song->pluginData('vfd_title',$params_title);
		$song->pluginData('vfd_livetext', $albumtext);
		
		return {
			title    => $params_title,
			artist   => $params_desc,
			album    => $albumtext,
			cover    => $params_icon,
			icon     => $params_icon,
			type     => $codec,
			bitrate  => $bitrate,
			duration => $track && $track->secs,
		}
	}

#  Live streams from here on - get current track data if available and OnAir details provided

#  Check if stream is not playing.  If Paused then just display stream data with "paused"
#  Possibility stream is stopped not paused - may need to check.
# Get some of the basic metadata items ready.
	my $title  = $params_title || Slim::Music::Info::getCurrentTitle($client, $url) || Slim::Music::Info::title($url);
	my $album  = $params_desc;
	my $icon   = $params_icon;
	my $artist ;

	if (! $client->isPlaying) {   #  Not sure if just isPlaying is correct or better isPlaying('really') or even isPaused ??
								  #  not "really playing" happens when stream has just started but is not actually playing - maybe no connection to source or when stream is paused.

		$logonair->debug(" Get Metadata for client that is not isPlaying");
		
		if ($song->pluginData->{'rp_name'}  ) {
			$title  = $song->pluginData->{'rp_name'};
			$album  = $song->pluginData->{'rp_description'} ;
			$artist = $song->pluginData->{'rp_servicename'};
			$icon   = $song->pluginData->{'rp_icon'} if (! $prefs->get('showmenuicon'));
		};		

		if (  $client->isPaused) {
			$title = $client->string('PLUGIN_BBCIPLAYER_PAUSED') . ' - ' . $title ;
		} else {
			$title = $client->string('PLUGIN_BBCIPLAYER_STOPPED'). ' - ' . $title;  # when stopped show station name.			
		}
		
		$song->pluginData('vfd_livetext', $artist);
		$song->pluginData('vfd_title',$title);
		return {
			title    => $title,
			artist   => $artist,
			album    => $album,
			cover    => $icon,
			icon     => $icon,
			type     => $codec,
			bitrate  => $bitrate,
		};
	}

#  If not already running, Start the timers to fetch current track information - OnAir should only be for iplayer 'live' URLs
	if (!$song->pluginData('bbconair') && $prefs->get('onair_txt') && $params{'onair'}) {
		require Plugins::BBCiPlayer::BBCOnAir;
		$log->info("Starting BBConair");
		$logonair->info("Starting BBConair");
		
#		Only some stations support current data - enabled by "track=on" on iplayer:// URL
		my $segments = $params{'track'} eq 'on' ?  1 : 0;

		# store bbconair object in song so we close livetext when song is destroyed
		$song->pluginData('bbconair' => Plugins::BBCiPlayer::BBCOnAir->new($params{'onair'}, $song, $segments));
	}

# Live streams  - 4 outcomes based on two factors
#     (i)  no delay or stream skipped back
#     (ii) current track content availability - BBCOnAir data available or not. Complicated - programme data is definite but Current track is not always available.

# Only Dash streams can be skipped backward - so show live metdata if not DASH (i.e. HLS, MP3) or not skipped back 

#  DASH streams with delay - first check for possibility of skipped back or paused streams (e.g.player went to sleep and paused for many hours).
	my $delaysecs = 0;
	if ($streamtype eq 'dash') {
		$delaysecs =  int( Time::HiRes::time()  - $song->pluginData('streamtime') - 	$song->pluginData('availabilitystarttime'));

#  Fixup delay if delay is not supported (i.e. more than 3 hours) on a live stream.
#  can happen as this routine is called before stream has actually started playing.

		if ($delaysecs > THREE_HOURS || $delaysecs < 0 ) {
			if ( $client->isPlaying && defined($song->pluginData('liveskipbackstate')) && $song->pluginData('liveskipbackstate') ne 'live') {
				$log->error(" Invalid estimated delay $delaysecs streamtime=". $song->pluginData('streamtime')) 
			}
	# Since has gone on too long - reset back to "Live" no delay when play resumes.
			$log->info(" Reset paused time to zero - invalid estimated delay $delaysecs streamtime=". $song->pluginData('streamtime')); 
			$song->pluginData('liveskipbacktime',0);
			$song->pluginData('liveskipbackstate','live');
			$delaysecs = 0;
		}
	}

# Set up the "live" program metadata 

# Use programme data if available 
	if ($song->pluginData->{'rp_name'} ) {

		$logonair->info("Got RP data ".$song->pluginData->{'rp_name'} );
		
		$title  = $song->pluginData->{'rp_name'};
		$artist = $song->pluginData->{'rp_description'} ;
		$album  = $song->pluginData->{'rp_servicename'};
		$icon   = $song->pluginData->{'rp_icon'} if (! $prefs->get('showmenuicon'));
	};

#  Use current track data if available - move things around if necessary
	if ($song->pluginData->{'track'})  {
		$logonair->info("Got current track data ".$song->pluginData->{'track'} );

		$album  = $artist;
		$artist = $title;
		$title  = $song->pluginData->{'artist'} . ' - '. $song->pluginData->{'track'} ;
		$icon   = $song->pluginData->{'icon'} if (! $prefs->get('showmenuicon'));
	} 

	$song->pluginData('vfd_livetext' , $title);
	$song->pluginData('vfd_title'    , $song->pluginData->{'rp_servicename'});

# NO delay is HLS, MP3 or a DASH with less than 60  (in case player audio is buffered)

	if ($delaysecs <= 60 ) {
		
		$logonair->debug("Return metdata for short/no delay stream  delay=$delaysecs");

		return {
			title    => $title,
			artist   => $artist,
			album    => $album,
			cover    => $icon,
			icon     => $icon,
			type     => $codec,
			bitrate  => $bitrate,
		}		
	}

#  If live stream has a "skip back" - modify the titles with a "delayed" message.
#  If skipped back - chgeck if current program data is valiud or else justr show default stream info.

#      Often the playing point is within current program so display current program while stream time is within the program broadcast "start" and "stop" time.
#      If the "time" of the playing point is before the program start time  or after the program end time - then display the sttaion name with a delay message. 

#  In skip back - handle delay in "minutes"

	my $delaymins =  int($delaysecs / 60);
	$title	= sprintf("%s - %d %s", $params_title , $delaymins, $client->string('PLUGIN_BBCIPLAYER_SKIPBACKDELAY'));
	$artist = $params_desc;
	$album  = undef;
	$icon   = $params_icon || $song->pluginData('icon');

	if ($song->pluginData->{'rp_icon'} && 
	  	$song->pluginData('streamtime') > $song->pluginData->{'rp_starttime'} &&	
	    $song->pluginData('streamtime') < $song->pluginData->{'rp_stoptime'}     ) {
				$artist = $song->pluginData->{'rp_name'};					
				$album  = $song->pluginData->{'rp_description'};					
				$icon   = $song->pluginData->{'rp_icon'} if (! $prefs->get('showmenuicon'));
	} ;
				
	if ($song->pluginData->{'skipbackdisplay'}){
			my $progtime = int(Time::HiRes::time() + $song->pluginData->{'dash_timediff'} - $song->pluginData->{'liveskipbacktime'});	
			$log->info(" Streamtime = ". $song->pluginData('streamtime') . "Prog time=$progtime Pc time = " . Time::HiRes::time() . ' time dif = '. $song->pluginData->{'dash_timediff'} . 'stoptime='. $song->pluginData->{'skipbackdisplaystop'});
			if ($progtime > $song->pluginData->{'skipbackdisplaystop'}) {
				delete $song->pluginData->{'skipbackdisplay'};
			}
			$artist = $song->pluginData('skipbackdisplayartist');
			$album  = $song->pluginData('skipbackdisplayalbum');								
			$icon   = $song->pluginData('skipbackdisplayicon') if (! $prefs->get('showmenuicon'));
	};
	
	$song->pluginData('vfd_livetext' , $artist);
	$song->pluginData('vfd_title'    , $title);
	$logonair->debug("Return metadata for long delay - delay=$delaysecs");

	return {
		title    => $title,
		artist   => $artist,
		album    => $album,
		cover    => $icon,
		icon     => $icon,
		type     => $codec || ($track && $track->content_type),
		bitrate  => $bitrate,
		duration => $track && $track->secs,
	};
}

sub bufferThreshold { 
	my ( $class, $client, $url) = @_;
	$log->info(" buffer threshold ". $prefs->get('threshold'). " url= $url");	

#	if (lc($url) =~/^dash:\/\//) {
#	  return  Plugins::BBCiPlayer::DASH::bufferThreshold($class, $client, $url);	
#	}

#  Make sure MP3 128k stream don't get default 255 which is too long.
#
	return 50 if (lc($url) =~/^http:\/\/bbcmedia.ic.llnwd.net\/stream\//) ;
	return $prefs->get('threshold')	;

#	return 255;

}


sub prefOrder {
	my $class = shift;
	my $client = shift;
	my $type  = shift;

	my @prefOrder;

	my @playerFormats = exists &Slim::Player::CapabilitiesHelper::supportedFormats 
		? Slim::Player::CapabilitiesHelper::supportedFormats($client) 
		: $client->formats;

	my $transcode = $prefs->get('transcode');

	for my $format (split(/,/, $prefs->get("prefOrder_$type"))) {

		my $testFormat = $format;
		$testFormat =~ s/flash//;
		$testFormat =~ s/hls/aac/;
		$testFormat =~ s/dashflac/flc/;
		$testFormat =~ s/dash/aac/;

		for my $playerFormat (@playerFormats) {
			if ($testFormat eq $playerFormat ||
				($transcode && exists &Slim::Player::TranscodingHelper::checkBin && 
				 Slim::Player::TranscodingHelper::checkBin("$testFormat-$playerFormat-*-*")) ) {

				push @prefOrder, $format;
				last;
			}
		}
	}

	return @prefOrder;
}



sub _parseSegmentsInfo {
	my $http = shift;
	my $song = $http->params('song');
	
#	$log->error("Dumper of http response \n". Dumper($http->content));
	my $segmentdata = eval { from_json($http->content) };
	if ( $@ || $segmentdata->{error} ) {
		$log->warn("error parsing segment metadata json data" . $@ . $segmentdata->{error});
		# If we get an error, poll doing nothing until the stream finishes
		# It's better than destroying our own object, as we'll just come back again!
		return;
	}
	
	my $segments = $segmentdata->{'segment_events'} ;
	my @seglist;
	foreach my $segment (@$segments) {
	
		push @seglist, { 'offset'   => $segment->{'version_offset'} , 
			'duration' => $segment->{'segment'}->{'duration'} ,
			'title'    => $segment->{'segment'}->{'title'} , 
			'artist'   => $segment->{'segment'}->{'artist'} } ;
	}
	$song->pluginData->{"segmentlist"} = \@seglist ;
	$log->debug("Dump of segments info \n". Dumper($segments));
	$log->debug("Dump of segment list\n". Dumper(\@seglist));
}

sub _getSegmentsInfo {
# Output segments info API
# http://open.live.bbc.co.uk/aps/programmes/b08hpvx5/segments.json
#
# Input dash URLs of the sort
# dash://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-syndication-dash/proto/http/vpid/m0003zg7|
# or
#dash://aod-dash-ww-live.akamaized.net/usp/auth/vod/piff_abr_full_audio/c793fe-m0003zg7/vf_m0003zg7_dac73859-5c8b-4be3-8ca7-ebee06614bb1.ism/pc_hd_abr_v2_nonuk_dash_master.mpd?__gda__=1555213063_6a80ac59226bb71593210c3a98a0faa9|

	my $songurl = shift;
	my $song    = shift;
	
	$songurl =~ m/http\/vpid\/([a-zA-Z0-9]+)|$/;
	$songurl =~ m/vod\/piff_abr_full_audio\/[a-zA-Z0-9]+-([a-zA-Z0-9]+)\// if ( !defined($1)) ;
	
	my $segmentsurl = "http://www.bbc.co.uk/programmes/$1/segments.json";
	$log->info("Fetch segments url $segmentsurl");
	Slim::Networking::SimpleAsyncHTTP->new(
		\&_parseSegmentsInfo,
		sub {
			my $self    = shift;
			my $httperror   = shift;
			if ($httperror =~ m/^404/) {
				$log->debug("error 404 - no segment list for this programme");
				$song->pluginData->{"segmentlist"} = () ;
			} else {
				$log->error("Segment fetch http error = ". $httperror . " url=$segmentsurl");
			}
		},
		{
			cache  => 1, # allow use of cached response
			song   => $song,
		},
	)->get($segmentsurl);

}

sub getcurrentdata  {
	my $song    = shift ;
	my $elapsed = shift;

	my $seglist = $song->pluginData->{"segmentlist"}; 
	return unless (defined($seglist));

	my $seekdata = $song->seekdata();
	$elapsed = $elapsed + $seekdata->{'timeOffset'} if (defined($seekdata->{'timeOffset'})); 
	
	my $i = 0;
	foreach my $segment (@$seglist) {
		$i++ and next if ( $elapsed > $segment->{'offset'} ); 
	}
	
	return if ($i == 0);
		
#	$log->debug("secs $elapsed i=$i  offset=".${$seglist}[$i-1]{'offset'} . "title=". ${$seglist}[$i-1]{'title'} . "artist=".${$seglist}[$i-1]{'artist'});
	
	return {'artist' => ${$seglist}[$i-1]{'artist'}, 'title' =>${$seglist}[$i-1]{'title'} };
}



1;
