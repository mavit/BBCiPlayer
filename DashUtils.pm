package Plugins::BBCiPlayer::DashUtils;

# Utils for DASH protocol handler
#
# (c) 2015, bpa baltontwo@eircom.net
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;
use bytes;

use Slim::Utils::Log;

use constant SHOW_BYTES => 32;
use Data::Dumper;
my $log = logger('plugin.bbciplayer.dash');

my  %atom_handler = (
	'ftyp' => \&process_ftyp_atom,
	'styp' => \&process_styp_atom,
	'free' => \&process_free_atom,
	'moov' => \&process_moov_atom,
	'mvhd' => \&process_mvhd_atom,
	'trak' => \&process_trak_atom,
	'tkhd' => \&process_tkhd_atom,
	'tref' => \&process_tref_atom,
	'trgr' => \&process_trgr_atom,
	'edts' => \&process_edts_atom,
	'mdia' => \&process_mdia_atom,
	'mdhd' => \&process_mdhd_atom,
	'hdlr' => \&process_hdlr_atom,
	'soun' => \&process_soun_atom,
	'minf' => \&process_minf_atom,
	'smhd' => \&process_smhd_atom,
	'dinf' => \&process_dinf_atom,
	'dref' => \&process_dref_atom,
	'url ' => \&process_url__atom,
	'stbl' => \&process_stbl_atom,
	'stsd' => \&process_stsd_atom,
	'mp4a' => \&process_mp4a_atom,
	'fLaC' => \&process_flac_atom,
	'dfLa' => \&process_dfla_atom,
	'esds' => \&process_esds_atom,
	'stts' => \&process_stts_atom,
	'stsc' => \&process_stsc_atom,
	'stco' => \&process_stco_atom,
	'stsz' => \&process_stsz_atom,
	'mvex' => \&process_mvex_atom,
	'trex' => \&process_trex_atom,
	
	'moof' => \&process_moof_atom,
	'mfhd' => \&process_mfhd_atom,
	'mthd' => \&process_mthd_atom,
	'traf' => \&process_traf_atom,
	'tfhd' => \&process_tfhd_atom,
	'tfdt' => \&process_tfdt_atom,
	'trun' => \&process_trun_atom,
	'mdat' => \&process_mdat_atom,
	
	);

sub process_atom {
    my $atom_type = shift;
    my $atom_size = shift;
    my $segment   = shift;
    my $segpos    = shift;
    
    return $atom_handler{$atom_type}(substr($segment,$segpos,$atom_size-8),$atom_size) if (defined($atom_handler{$atom_type})) ;

	$log->error("******** no process routine for atom $atom_type size $atom_size");
	$log->error(sprintf("Atom body %0*v2X\n", "  ", substr($segment,$segpos,$atom_size-8),$atom_size)); 
    return;
}
    
sub get_next_atom {
    my $segment = shift @_;
	if (length($segment) < 4 ) {
		$log->error(sprintf("Short Segment =  %0*v2X\n",$segment));
		return;
	}

    my $atom_size = unpack("N", substr($segment, 0, 4));
    my $atom_type = substr($segment,4,4);

    if ($atom_size == 0) {
		$log->error("\n Atom size zero - content here to end should be mdat. Atom type=$atom_type  ");
    } elsif ($atom_size == 1) {
		$log->error("\nAtom size 1 - extralarge - not with isobff") ;
    } ;
    
    return($atom_type, $atom_size);
    
}    

sub process_ftyp_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    $log->debug(sprintf("Process ftyp Atom body %0*v2X\n", "  ", $atom_body)); 

    my %result;
    $result{'brand'}             = substr($atom_body,0,4);
    $result{'minor_version'}     = unpack("N", substr($atom_body, 4, 4));
    my @compatible;
    for (my $i=8; $i < $atom_size; $i += 4) {
		$log->debug("i=$i  atom_size = $atom_size \n");
		push @compatible, substr($atom_body,$i,4);
	}
	$result{'compatible_brands'} = \@compatible;
	$log->info("Ftyp Atom ". Dumper(\%result));
    return \%result ;
}

sub process_styp_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    $log->debug(sprintf("Process styp Atom body %0*v2X\n", "  ", $atom_body)); 

    my %result;
    $result{'brand'}             = substr($atom_body,0,4);
    $result{'minor_version'}     = unpack("N", substr($atom_body, 4, 4));
    my @compatible;
    for (my $i=8; $i < $atom_size; $i += 4) {
		$log->debug("i=$i  atom_size = $atom_size \n");
		push @compatible, substr($atom_body,$i,4);
	}
	$result{'compatible_brands'} = \@compatible;
	$log->info("Ftyp Atom ". Dumper(\%result));

    return \%result ;
}


sub process_free_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    $log->debug(sprintf("Process free Atom body %0*v2X\n", "  ", $atom_body)); 

    if ((length($atom_body) % 8) ) {
		$log->info("ftyp atom wrong length: Length is ". length($atom_body) ." should be multple of 8"); 
		return;
    }

    return {'data' => $atom_body} ;

}

sub process_container {
    my $atom_id   = shift @_;
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);

    my $segment = $atom_body;
    my $segpos = 0;
    my %result ;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process $atom_id Atom body %0*v2X\n", "  ",substr($atom_body,0,$max_len))); 
    
    while ($segpos < length($segment)){
		
#		$log->info(sprintf("$atom_id segment starting $segpos %0*v2X\n", " ", substr($segment,$segpos,32)));
		my ($subatom_type, $subatom_size) = get_next_atom(substr($segment,$segpos));
   
		$segpos += 8;
   
#		$log->info("$atom_id subatom type = $subatom_type size = $subatom_size $atom_id segpos=$segpos\n"));
		$result{$subatom_type} = process_atom($subatom_type,$subatom_size, $segment,$segpos);
		$segpos += $subatom_size - 8;
	}
 
    return \%result ;

}

sub process_moov_atom {
    my $atom_body = shift @_;
   	return process_container('moov',$atom_body);
}

sub process_minf_atom {
    my $atom_body = shift @_;
   	return process_container('minf',$atom_body);
}

sub process_dinf_atom {
    my $atom_body = shift @_;
   	return process_container('dinf',$atom_body);
}


sub process_mvhd_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process mvhd Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    my $atom_base = 0;
    
    $result{'version'} 				 = unpack("C", substr($atom_body,  0, 1));
    $result{'flags'}   				 = unpack("N", "\x00".substr($atom_body,  1, 3));
    if ($result{'version'} == 1) {
#		$result{'creation_time'}     = unpack("Q", substr($atom_body, 4, 8));
#		$result{'modification_time'} = unpack("Q", substr($atom_body, 12, 8));
		$result{'timescale'}         = unpack("N", substr($atom_body, 20, 4));
#		$result{'duration'}          = unpack("Q", substr($atom_body, 24, 8));
		$atom_base = 32;
		

	} else {
		$result{'creation_time'}     = unpack("N", substr($atom_body, 4, 4));
		$result{'modification_time'} = unpack("N", substr($atom_body, 8, 4));
		$result{'timescale'}         = unpack("N", substr($atom_body, 12, 4));
		$result{'duration'}          = unpack("N", substr($atom_body, 16, 4));
		$atom_base = 20;
	}

	$result{'rate'}      =  unpack("N", substr($atom_body, $atom_base,    4));
	$result{'volume'}    =  unpack("N", substr($atom_body, $atom_base+4,  4));
	$result{'reserved1'} =  unpack("n", substr($atom_body, $atom_base+8,  2));
	$result{'reserved2'} = [unpack("N", substr($atom_body, $atom_base+10, 4)),
							unpack("N", substr($atom_body, $atom_base+14, 4)) ];
	$result{'matrix'} = [];						

	for (my $i=0 ; $i < 9 ; $i++) {
		push @{$result{'matrix'}} , unpack("N", substr($atom_body, $atom_base+18+($i*4), 4) );						
	} 
	$result{'predefined'} = [];						
	for (my $i=0 ; $i < 6 ; $i++) {
		push @{$result{'predefined'}} , unpack("N", substr($atom_body, $atom_base+54+($i*4), 4) );						
	}
	
	$result{'next_track_ID'} = unpack("N", substr($atom_body, $atom_base+78, 4));
#	$log->debug("mvhd result ". Dumper(\%result));
	
    return  \%result ;

}

sub process_tkhd_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process mvhd Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    my $atom_base = 0;
    
    $result{'version'} = unpack("C", substr($atom_body,  0, 1));
    $result{'flags'}   = unpack("N", "\x00".substr($atom_body,  1, 3));
    if ($result{'version'} == 1) {
#		$result{'creation_time'}     = unpack("Q", substr($atom_body, 4, 8));
#		$result{'modification_time'} = unpack("Q", substr($atom_body, 12, 8));
		$result{'track_ID'}          = unpack("N", substr($atom_body, 20, 4));
		$result{'reserved'}          = unpack("N", substr($atom_body, 24, 4));
#		$result{'duration'}          = unpack("Q", substr($atom_body, 28, 8));
		$atom_base = 36;
		

	} else {
		$result{'creation_time'}     = unpack("N", substr($atom_body,  4, 4));
		$result{'modification_time'} = unpack("N", substr($atom_body,  8, 4));
		$result{'track_ID'}          = unpack("N", substr($atom_body, 12, 4));
		$result{'reserved'}          = unpack("N", substr($atom_body, 16, 4));
		$result{'duration'}          = unpack("N", substr($atom_body, 20, 4));
		$atom_base = 24;
	}

	$result{'reserved1'}      = [unpack("N", substr($atom_body, $atom_base+ 0, 4)),
							     unpack("N", substr($atom_body, $atom_base+ 4, 4))];
	$result{'layer'}           = unpack("n", substr($atom_body, $atom_base+ 8, 2));
	$result{'alternate_group'} = unpack("n", substr($atom_body, $atom_base+10, 2));
	$result{'volume'}          = unpack("n", substr($atom_body, $atom_base+12, 2));
	$result{'reserved2'}       = unpack("n", substr($atom_body, $atom_base+14, 2));
	$result{'matrix'} = [];						

	for (my $i=0 ; $i < 9 ; $i++) {
		push @{$result{'matrix'}} , unpack("N", substr($atom_body, $atom_base+16+($i*4), 4) );
	} 

 	$result{'width'}           = unpack("N", substr($atom_body, $atom_base+52, 4));
 	$result{'height'}          = unpack("N", substr($atom_body, $atom_base+56, 4));

#	$log->debug("tkhd result ". Dumper(\%result));
    return  \%result;

}

sub process_mfhd_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process mfhd Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    

    $result{'version'} = unpack("C", substr($atom_body,  0, 1));
    $result{'flags'}   = unpack("N", "\x00".substr($atom_body,  1, 3));
    
	$result{'sequence_number'}  = unpack("N", substr($atom_body,  4, 4));
	
    return  \%result ;

}

sub process_tfhd_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process tfhd Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    
    $result{'version'}     =   unpack("C",        substr($atom_body,  0, 1));
    $result{'tf_flags'}    =   unpack("N", "\x00".substr($atom_body,  1, 3));
    
	$result{'track_ID'}                 = unpack("N", substr($atom_body,  4, 4));
	my $atom_base = 8;
	if ( $result{'tf_flags'} & 0x1) {
#		$result{'base_data_offset'}     = unpack("Q", substr($atom_body,  $atom_base, 8)) ;
		$atom_base += 8;
	};
	if ( $result{'tf_flags'} & 0x2) {
		$result{'sample_description_index'} = unpack("N", substr($atom_body,  $atom_base, 4)) ;
		$atom_base += 4;
	}	
	if ( $result{'tf_flags'} & 0x8) {
		$result{'default_sample_duration'} = unpack("N", substr($atom_body,  $atom_base, 4)) ;
		$atom_base += 4;
	}	
	if ( $result{'tf_flags'} & 0x10) {
		$result{'default_sample_size'} = unpack("N", substr($atom_body,  $atom_base, 4)) ;
		$atom_base += 4;
	}	
	if ( $result{'tf_flags'} & 0x20) {
		$result{'default_sample_flags'} = unpack("N", substr($atom_body, $atom_base, 4)) ;
		$atom_base += 4;
	}	
	
    return  \%result ;

}

sub process_tfdt_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process tfdt Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    
    $result{'version'}                 = unpack("C", substr($atom_body,  0, 1));
    $result{'flags'}                   = unpack("N", "\x00".substr($atom_body,  1, 3));
	if ($result{'version'} == 1) {
		$log->debug("Can't do a 64 bit unpack ");
#		$result{'baseMediaDecodeTime'} = unpack("Q", substr($atom_body,  4, 8));
	} else {
		$result{'baseMediaDecodeTime'} = unpack("N", substr($atom_body,  4, 4));
	}
    return  \%result ;

}

sub process_trun_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
    $log->debug(sprintf("Process trun Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    my $atom_base = 0;
    
    $result{'version'}                 = unpack("C", substr($atom_body,  0, 1));
    $result{'tr_flags'}                = unpack("N", "\x00".substr($atom_body,  1, 3));

	$result{'sample_count'}            = unpack("N", substr($atom_body,  4, 4));
	if ( $result{'tr_flags'} & 0x1) {
		$result{'data_offset'}         = unpack("N", substr($atom_body,  8, 4)) ;
		$atom_base += 4;
	};
	if ( $result{'tr_flags'} & 0x4) {
		$result{'first_sample_flags'} = unpack("N", substr($atom_body,  8+$atom_base, 4)) ;
		$atom_base += 4;
	}
	
	my @samples;
	
	for (my $i = 0; $i < $result{'sample_count'} ; $i++) {
		my %sample;
		if ( $result{'tr_flags'} & 0x100) {
			$sample{'sample_duration'} = unpack("N", substr($atom_body,  8+$atom_base, 4)); 
			$atom_base += 4;
		};
		if ( $result{'tr_flags'} & 0x200) {
			$sample{'sample_size'} = unpack("N", substr($atom_body,  8+$atom_base, 4)) ;
			$atom_base += 4;
		};
		if ( $result{'tr_flags'} & 0x400) {
			$sample{'sample_flags'} = unpack("N", substr($atom_body,  8+$atom_base, 4)) ;
			$atom_base += 4;
		};
		
		if ( $result{'tr_flags'} & 0x800) {
			if ($result{'version'} ==1) {
				$sample{'sample_composition_time_offset'} = unpack("N", substr($atom_body,  8+$atom_base, 4)) ;
			} else {
# Make the result signed.
				$sample{'sample_composition_time_offset'} = unpack("N", substr($atom_body,  8+$atom_base, 4)) ;
			
			};
		}
		push @samples, \%sample ;
	}
	
	$result{'samples'} = \@samples;
    return  \%result ;

}


sub process_mdhd_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process mdhd Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    my $atom_base = 0;
    
    $result{'version'}                 = unpack("C", substr($atom_body,  0, 1));
    $result{'flags'}                   = unpack("N", "\x00".substr($atom_body,  1, 3));
    if ($result{'version'} == 1) {
#		$result{'creation_time'}     = unpack("Q", substr($atom_body, 4, 8));
#		$result{'modification_time'} = unpack("Q", substr($atom_body, 12, 8));
		$result{'timescale'}         = unpack("N", substr($atom_body, 20, 4));
#		$result{'duration'}          = unpack("Q", substr($atom_body, 24, 8));
		$atom_base = 32;
	} else {
		$result{'creation_time'}     = unpack("N", substr($atom_body, 4, 4));
		$result{'modification_time'} = unpack("N", substr($atom_body, 8, 4));
		$result{'timescale'}         = unpack("N", substr($atom_body, 12, 4));
		$result{'duration'}          = unpack("N", substr($atom_body, 16, 4));
		$atom_base = 20;
	}
	my $temp = unpack("n", substr($atom_body, $atom_base, 2));
	
    $result{'pad'}    = $temp >> 15;
    $result{'language'} = chr((($temp >> 10) & 0x1F) + 0x60).
                          chr((($temp >>  5) & 0x1F) + 0x60).
                          chr(( $temp        & 0x1F) + 0x60); 

    return  \%result ;

}


sub process_hdlr_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process hdlr Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}         = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}           = unpack("N", "\x00".substr($atom_body,  1, 3));
	$result{'pre_defined'}     = unpack("N",  substr($atom_body,  4, 4));
	$result{'handler_type'}    =              substr($atom_body,  8, 4);
	$result{'reserved'}        = [unpack("N", substr($atom_body, 12, 4)),
								  unpack("N", substr($atom_body, 16, 4)),
								  unpack("N", substr($atom_body, 20, 4)),
								  ];
	$result{'name'}				= substr($atom_body, 24);
    return  \%result ;
}

sub process_smhd_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process smhd Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
    
    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));
	$result{'balance'}     = unpack("n", substr($atom_body, 4, 2));
	$result{'reserved'}    = unpack("n", substr($atom_body, 6, 2));

    return  \%result ;

}
sub process_dref_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process smhd Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));
  
	$result{'entry_count'} = unpack("N", substr($atom_body, 4, 4));

	for (my $i = 0 ; $i < $result{'entry_count'} ; $i++) {
		
	}
    return  \%result ;

}
sub process_stsd_atom {

    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process stsd length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));

	$result{'entry_count'} = unpack("N", substr($atom_body, 4, 4));
	my $atom_base = 0;

	$result{'entries'} = {};
	for (my $i = 0 ; $i < $result{'entry_count'} ; $i++) {
# Assumed entry if Audio
		my ($subatom_type, $subatom_size) = get_next_atom(substr($atom_body, 8));
		$result{'entries'}{$subatom_type} = process_atom($subatom_type,$subatom_size, $atom_body,16);
	}
    return  \%result ;
	
}
sub process_mp4a_atom {
	
	my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process mp4a length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 
	
		$result{'reserved'}  =                          substr($atom_body,  0, 6) ;
		$result{'data_reference_index'}  =  unpack("n", substr($atom_body,  6, 2));
		$result{'reserved2'}             = [unpack("N", substr($atom_body,  8, 4)),
		                                    unpack("N", substr($atom_body, 12, 4))];
		$result{'channelcount'}          =  unpack("n", substr($atom_body, 16, 2));
		$result{'samplesize'}            =  unpack("n", substr($atom_body, 18, 2));
		$result{'predefined'}            =  unpack("n", substr($atom_body, 20, 2));
		$result{'reserved3'}             =  unpack("n", substr($atom_body, 22, 2));
		$result{'samplerate'}            =  unpack("N", substr($atom_body, 24, 4));
		if ($atom_size > 28) {
			# Assumed entry if Audio
			my ($subatom_type, $subatom_size) = get_next_atom(substr($atom_body, 28));
			$result{$subatom_type} = process_atom($subatom_type,$subatom_size, $atom_body,36);
		}
    return  \%result ;
}


sub process_flac_atom {
	
	my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
#    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
    my $max_len =  ($atom_size > 28) ? 48 : $atom_size;
    
    $log->info(sprintf "Process fLaC length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len)); 
	
		$result{'reserved'}  =                          substr($atom_body,  0, 6) ;
		$result{'data_reference_index'}  =  unpack("n", substr($atom_body,  6, 2));
		$result{'reserved2'}             = [unpack("N", substr($atom_body,  8, 4)),
		                                    unpack("N", substr($atom_body, 12, 4))];
		$result{'channelcount'}          =  unpack("n", substr($atom_body, 16, 2));
		$result{'samplesize'}            =  unpack("n", substr($atom_body, 18, 2));
		$result{'predefined'}            =  unpack("n", substr($atom_body, 20, 2));
		$result{'reserved3'}             =  unpack("n", substr($atom_body, 22, 2));
		$result{'samplerate'}            =  unpack("N", substr($atom_body, 24, 4));
		if ($atom_size > 28) {
			# Assumed entry if Audio
			my ($subatom_type, $subatom_size) = get_next_atom(substr($atom_body, 28));
			$result{$subatom_type} = process_atom($subatom_type,$subatom_size, $atom_body,36);
		}
    return  \%result ;
}

sub process_dfla_atom {
	
	my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
#    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
    my $max_len =  $atom_size ;
    $log->debug(sprintf "Process dfLa length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len)); 
    
    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));
    
    my @metablocks;
    my $i = 4;
    while ($i < ($atom_size -8)) {
	my %metablock;
	my $tmp;
	
	$tmp =  unpack("C",  substr($atom_body,  $i, 1));
	$metablock{'LastMetadataBlockFlag'} = (($tmp & 0x80) == 0x80);
	$metablock{'BlockType'} = ($tmp & 0x7f);
	$metablock{'BlockLength' } = unpack("N", "\x00".substr($atom_body,  $i+1, 3));

	$log->debug("Expected Block type 0 - found type ".  $metablock{'BlockType'} . " Length ". $metablock{'BlockLength' } . "\n");
	if ( $metablock{'BlockType'} == 0 ) {
	  my %streaminfo;
	  $streaminfo{'Block_type'} ='StreamInfo';
	  $streaminfo{'minblocksize'} = unpack("n", substr($atom_body, $i+4, 2));
	  $streaminfo{'maxblocksize'} = unpack("n", substr($atom_body, $i+6, 2));
	  $streaminfo{'minframesize'} = unpack("N", "\x00".substr($atom_body,  $i+8, 3));
	  $streaminfo{'maxframesize'} = unpack("N", "\x00".substr($atom_body,  $i+11, 3));

# Sample rate is 20 bits - 16 bits plus high nibble of next byte

	  $streaminfo{'samplerate'}  = unpack("n", substr($atom_body, $i+14, 2));
	  $tmp = unpack("C",  substr($atom_body,  $i+16, 1));
	  my $lownibble  = $tmp & 0x0F;
	  my $highnibble = ($tmp >> 4) & 0x0F;
	  $streaminfo{'samplerate'} = ($streaminfo{'samplerate'} << 4 ) + $highnibble;

# Channels are the next 3 bit which are top 3 bits of lower nibble so shift right once to drop least significiant bit.

	  $streaminfo{'channels'} = ($lownibble >> 1) + 1;

# The least significant if 1 then add 0x100 to next 4 bits (i.e.high nibble of next byte) .

	  $tmp = unpack("C",  substr($atom_body,  $i+17, 1));
	  $streaminfo{'bitspersample'} = (($tmp >> 4) &0x0F)  + 1;
	  $streaminfo{'bitspersample'} += 0x100 if (($lownibble & 0x01) == 1);

	  $streaminfo{'totalsamples'} = unpack("N", substr($atom_body, $i + 18, 4)) + (($tmp & 0x0F) << 32);
	  push @metablocks,\%streaminfo;
	}   
	
	$i = $i +  $metablock{'BlockLength' };
    }

    $result{'metablocks'} = \@metablocks;
    $result{'metablockheader'} = substr($atom_body, 4,34);
    $log->debug(sprintf "\nProcess metablockheader body %0*v2X\n", "  ", $result{'metablockheader'}); 

    return  \%result ;
}



sub process_esds_atom {
	
	my $atom_body = shift @_;
    my $atom_size = length($atom_body);
   
    my %result;
    my $tag;
    my $tag_size;
    my $dummy;
    
  
    $log->info(sprintf("Process esds length $atom_size Atom body %0*v2X\n", "  ", $atom_body)); 
    
    $result{'version'}     = unpack("C",        substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));
    
    $tag                   = unpack("C",        substr($atom_body,  4, 1));
    if ($tag != 0x03) {
		$log->error("Unexpected tag value $tag expected 03\n");
		return {"bad tag $tag not 03"};
	};
	
    $tag_size              = unpack("C",        substr($atom_body,  5, 1));
    my $es_id              = unpack("n",        substr($atom_body,  6, 2));
    $result{'esflags'}     = unpack("C",        substr($atom_body,  8, 1));
    
    $tag                   = unpack("C",        substr($atom_body,  9, 1));
    if ($tag != 0x04) {
		$log->error("Unexpected tag value $tag expected 04\n");
		return {"bad tag $tag not 04"};
	};
    $tag_size               = unpack("C",        substr($atom_body, 10, 1));
    $result{'objectTypeId'} = unpack("C",        substr($atom_body, 11, 1));
    $result{'buffersizedb'} = unpack("N",        substr($atom_body, 12, 4));
    $result{'maxbitrate'}   = unpack("N",        substr($atom_body, 16, 4));
    $result{'avgbitrate'}   = unpack("N",        substr($atom_body, 20, 4));

    $tag                   = unpack("C",         substr($atom_body, 24, 1));
    if ($tag != 0x05) {
		$log->error("Unexpected tag value $tag expected 05\n");
		return {"bad tag $tag not 05"};
	};
    $tag_size               = unpack("C",        substr($atom_body, 25, 1));
	$log->info("Esds tag 05 size $tag_size");
    my $audiospecificconfig = unpack("N",        substr($atom_body, 26, 4));
    
    $result{'AudioObjectType'} =  $audiospecificconfig >> 27;
    $result{'FreqIndex'}       = ($audiospecificconfig >> 23)  & 0x0F; 
    $result{'channelConfig'}   = ($audiospecificconfig >> 19)  & 0x0F; 
    my $offset = $tag_size -4;
    
    $tag                   = unpack("C",        substr($atom_body,  30+$offset, 1));
    if ($tag != 0x06) {
		$log->error("Unexpected tag value $tag expected 06\n");
		$log->error(sprintf("Dumpesds atom body length %d  body %0*v2X\n", $atom_size,"  ",  $atom_body));
		return {"bad tag $tag not 06"};
	};
    $tag_size               = unpack("C",        substr($atom_body, 31+$offset, 1));
    
    $result{'version'}     = unpack("C",        substr($atom_body,  0, 1));
	$log->info("Esds result ". Dumper(\%result));

    return  \%result ;
}



sub process_stts_atom {

    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process stts length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));

	$result{'entry_count'} = unpack("N", substr($atom_body, 4, 4));
	my @samples;
	for (my $i = 0 ; $i < $result{'entry_count'} ; $i++) {
		my $sample_count = unpack("N", substr($atom_body,  8+($i*8), 4));
		my $sample_delta = unpack("N", substr($atom_body, 12+($i*8), 4));
		push @samples, ($sample_count,$sample_delta);
	}
	$result{'samples'} = \@samples;
    return  \%result ;
	
}

sub process_stsc_atom {

    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process stsc length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));

	$result{'entry_count'} = unpack("N", substr($atom_body, 4, 4));
	my @samples;
	for (my $i = 0 ; $i < $result{'entry_count'} ; $i++) {
		my $first_chunk              = unpack("N", substr($atom_body,  8+($i*12), 4));
		my $samples_per_chunk        = unpack("N", substr($atom_body, 12+($i*12), 4));
		my $sample_description_index = unpack("N", substr($atom_body, 16+($i*12), 4));
		
		push @samples, ($first_chunk,$samples_per_chunk, $sample_description_index);
	}
	$result{'samples'} = \@samples;

    return  \%result ;
	
}

sub process_stsz_atom {

    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process stsz length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));

	$result{'sample_size'}  = unpack("N", substr($atom_body, 4, 4));
	$result{'sample_count'} = unpack("N", substr($atom_body, 8, 4));

	if ($result{'sample_size'} == 0) {
		my @samples;
		for (my $i = 0 ; $i < $result{'sample_count'} ; $i++) {
			my $entry_size        = unpack("N", substr($atom_body,  12+($i*4), 4));
			push @samples, $entry_size;
		}
		$result{'entry_size'} = \@samples;
	}	
    return  \%result ;
	
}

sub process_stco_atom {

    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process stco length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));

	$result{'entry_count'}  = unpack("N", substr($atom_body, 4, 4));

	my @samples;
	for (my $i = 0 ; $i < $result{'entry_count'} ; $i++) {
		my $chunk_offset    = unpack("N", substr($atom_body,  8+($i*4), 4));
		push @samples, $chunk_offset;
	}
	$result{'chunk_offset'} = \@samples;
	
    return  \%result ;
}



sub process_trex_atom {

    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    
    my %result;
    
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process trex length $atom_size Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    $result{'version'}     = unpack("C",  substr($atom_body,  0, 1));
    $result{'flags'}       = unpack("N", "\x00".substr($atom_body,  1, 3));

	$result{'track_ID'}                         = unpack("N", substr($atom_body,  4, 4));
	$result{'default_sample_description_index'} = unpack("N", substr($atom_body,  8, 4));
	$result{'default_sample_duration'}          = unpack("N", substr($atom_body, 12, 4));
	$result{'default_sample_size'}              = unpack("N", substr($atom_body, 16, 4));
	$result{'default_sample_flags'}             = unpack("N", substr($atom_body, 20, 4));

    return  \%result ;
	
}


sub process_stbl_atom {
    my $atom_body = shift @_;
   	return process_container('stbl',$atom_body);
}

sub process_trak_atom {
    my $atom_body = shift @_;
   	return process_container('trak',$atom_body);
}

sub process_mdia_atom {
    my $atom_body = shift @_;
   	return process_container('mdia',$atom_body);
}

sub process_mvex_atom {
 my $atom_body = shift @_;
   	return process_container('mvex',$atom_body);
}

sub process_moof_atom {
    my $atom_body = shift @_;
   	return process_container('moof',$atom_body);
}

sub process_traf_atom {
    my $atom_body = shift @_;
   	return process_container('traf',$atom_body);
}

sub process_mdat_atom {
    my $atom_body = shift @_;
    my $atom_size = length($atom_body);
    my $max_len =  ($atom_size > SHOW_BYTES) ? SHOW_BYTES : $atom_size;
#    $log->debug(sprintf("Process mdat Atom body %0*v2X\n", "  ", substr($atom_body,0,$max_len))); 

    return { 'data' => $atom_body };

}

sub mp4dflaToFlacHeader {	

}

sub mp4esdsToADTSHeader {	

# AAAAAAAA AAAABCCD EEFFFFGH HHIJKLMM MMMMMMMM MMMOOOOO OOOOOOPP 
#
# Header consists of 7 bytes without CRC.
#
# Letter	Length (bits)	Description
# A	12	syncword 0xFFF, all bits must be 1
# B	1	MPEG Version: 0 for MPEG-4, 1 for MPEG-2
# C	2	Layer: always 0
# D	1	set to 1 as there is no CRC 
# E	2	profile, the MPEG-4 Audio Object Type minus 1
# F	4	MPEG-4 Sampling Frequency Index (15 is forbidden)
# G	1	private bit, guaranteed never to be used by MPEG, set to 0 when encoding, ignore when decoding
# H	3	MPEG-4 Channel Configuration (in the case of 0, the channel configuration is sent via an inband PCE)
# I	1	originality, set to 0 when encoding, ignore when decoding
# J	1	home, set to 0 when encoding, ignore when decoding
# K	1	copyrighted id bit, the next bit of a centrally registered copyright identifier, set to 0 when encoding, ignore when decoding
# L	1	copyright id start, signals that this frame's copyright id bit is the first bit of the copyright id, set to 0 when encoding, ignore when decoding
# M	13	frame length, this value must include 7 bytes of header 
# O	11	Buffer fullness
# P	2	Number of AAC frames (RDBs) in ADTS frame minus 1, for maximum compatibility always use 1 AAC frame per ADTS frame

	my $mp4esds         = shift @_;
	my $framelength		= shift @_;

	my $profile         =  $mp4esds->{'AudioObjectType'}; 
	$log->error("Unusual AudioObjectType $profile" ) if (($profile != 5) && ($profile != 2));
	$profile = 2 if ($profile == 5); # Fix because Touch and Radio cannot handle ADTS header of AAC Main.
    my $frequency_index =  $mp4esds->{'FreqIndex'};       
    my $channel_config  =  $mp4esds->{'channelConfig'};   

	my $finallength     = $framelength + 7;       
	my @ADTSHeader      = (0xFF,0xF1,0,0,0,0,0xFC);
	my $adts ='';
	
#   $ADTSHeader[0] = 0xff;
#   $ADTSHeader[1] = 0xf1;
    $ADTSHeader[2] = (((($profile & 0x3) - 1)  << 6)   + ($frequency_index << 2) + ($channel_config >> 2));
#    $ADTSHeader[2] = (((($profile - 1)  & 0x3) << 6)   + ($frequency_index << 2) + ($channel_config >> 2));
    $ADTSHeader[3] = ((($channel_config & 0x3) << 6)   + ($finallength >> 11));
    $ADTSHeader[4] = ( ($finallength & 0x7ff) >> 3);
    $ADTSHeader[5] = ((($finallength & 7) << 5) + 0x1f) ;
#   $ADTSHeader[6] = 0xfc;
	$adts = pack("CCCCCCC",@ADTSHeader);

	return $adts;
}	

sub parseMpeg4 {
	
	my $segment = shift @_;
	my $segpos  = 0;
	my %parsed_atoms;

    while ($segpos < length($segment)) {
		my ($atom_type, $atom_size) = get_next_atom(substr($segment,$segpos));
		$segpos += 8;
   
		$log->debug("Atom type = $atom_type size = $atom_size\n");
		my $current_atom= process_atom($atom_type,$atom_size, $segment,$segpos);
		$parsed_atoms{$atom_type} = $current_atom;
		$segpos += $atom_size -8;
	}
	return %parsed_atoms;
}

sub convertDashSegtoADTS{
	
	my $mp4esds     = shift @_;
	my $dashsegment = shift @_;
	my $mp4trun		= shift @_;
	
	my $segpos = 0;
	my $sample_count = $mp4trun->{'sample_count'};
	
	my $adtssegment ='';
	foreach my $sample (@{$mp4trun->{'samples'}}) {
		$adtssegment .= mp4esdsToADTSHeader($mp4esds ,$sample->{'sample_size'});
		$adtssegment .= substr($dashsegment,$segpos, $sample->{'sample_size'});
		$segpos += $sample->{'sample_size'};
	}
	return $adtssegment;
}

sub convertDashSegtoFlac{
	
	my $dashsegment = shift @_;
	my $mp4trun		= shift @_;
	
	my $segpos = 0;
	my $sample_count = $mp4trun->{'sample_count'};
	
	my $flacsegment ='';
	foreach my $sample (@{$mp4trun->{'samples'}}) {
#		$log->error(sprintf "Process Flac sample (%5d ) ", $sample->{'sample_size'});
#		$log->error(sprintf "  %0*v2X\n", "  ",  substr($dashsegment,$segpos, 32)); 
		$flacsegment .= substr($dashsegment,$segpos, $sample->{'sample_size'});
		$segpos += $sample->{'sample_size'};
	}
	return $flacsegment;
}
	
	
