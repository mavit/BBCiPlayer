package Plugins::BBCiPlayer::RecentTracks;

use strict;

use Slim::Utils::Log;

use Date::Parse;
use JSON::XS::VersionOneAndTwo;
use Slim::Utils::Prefs;

use Data::Dumper;

my $log = logger('plugin.bbciplayer');
my $prefs = preferences('plugin.bbciplayer');


sub parse {
	my $class  = shift;
	my $http   = shift;

	my $params = $http->params('params');
	my $url    = $params->{'url'};

	my $segdata = ${$http->contentRef}; 
	my @menu;

	my $parsed_segdata = eval { from_json($segdata) };

	if ( $@  ) {
		$log->error( "error parsing segments data  " . $@ . "\n". Dumper($segdata));
		return;
	}


	my $segments        = $parsed_segdata->{'data'};;
	if (scalar(@{$segments}) eq 0) {
		push @menu, {
		'name'	      => 'No recent tracks available',
		};
	};

	my $imageres = $prefs->get('imageres');
	my $now = time();

	
	foreach my $segment (@{$segments}) {
		my $image_url = $segment->{'image_url'};
		my $artist    = $segment->{'titles'}->{'primary'};
		my $track     = $segment->{'titles'}->{'secondary'};
		my $when      = $segment->{'offset'}->{'label'};
		$image_url  =~ s/\{recipe\}/$imageres/;
		$log->info(sprintf("%-15s  %s - %s   Image: %s\n", $when, $artist, $track, $image_url));

		my $item_text;
		if ($when eq 'Now Playing') {
			$item_text = sprintf("Now playing      %s - %s",$artist,$track); 
		} elsif ($when eq 'Less Than a Minute Ago') {
			$item_text = sprintf("< 1 min ago       %s - %s",$artist,$track); 
		} elsif ($when eq '1 Minute Ago') {
			$item_text = sprintf("  1 min ago       %s - %s",$artist,$track); 
		} elsif ($when =~ m/(\d+) Minutes Ago/ ) {
			$item_text = sprintf("%3d mins ago      %s - %s",$1,$artist,$track); 
		} else {
			$log->error(" Can't parse playing time \"$when\" ");
			next;
		};
		
		push @menu,{	'name'  =>  $item_text,
						'icon'  =>  $image_url,
						'cover'  =>  $image_url,
			};
	}
		
	
	return {
		'name'    => $params->{'feedTitle'},
		'items'   => \@menu,
		'type'    => 'opml',
		'nocache' => 1,
		'cachetime' => 0,
	};
}

1;
