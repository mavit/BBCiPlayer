package Plugins::BBCiPlayer::Plugin;

# Plugin to play live and on demand BBC radio streams
# (c) Triode, 2007-2015, triode1@btinternet.com
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;

use base qw(Slim::Plugin::OPMLBased);

use File::Spec::Functions qw(:ALL);
use Slim::Utils::Prefs;
use Slim::Utils::Log;
use Scalar::Util qw(blessed);
use Slim::Utils::Strings qw(cstring);

use Data::Dumper;

use Plugins::BBCiPlayer::iPlayer;
use Plugins::BBCiPlayer::RTMP;
use Plugins::BBCiPlayer::HLS;
use Plugins::BBCiPlayer::DASH;

my $prefs = preferences('plugin.bbciplayer');


my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.bbciplayer.rtmp',
	'defaultLevel' => 'ERROR',
	'description'  => getDisplayName(),
});

my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.bbciplayer.hls',
	'defaultLevel' => 'ERROR',
	'description'  => getDisplayName(),
});

my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.bbciplayer.dash',
	'defaultLevel' => 'ERROR',
	'description'  => getDisplayName(),
});


my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.bbciplayer.livetxt',
	'defaultLevel' => 'ERROR',
	'description'  => getDisplayName(),
});


my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.bbciplayer.bbconair',
	'defaultLevel' => 'ERROR',
	'description'  => getDisplayName(),
});

my $log = Slim::Utils::Log->addLogCategory({
	'category'     => 'plugin.bbciplayer',
	'defaultLevel' => 'ERROR',
	'description'  => getDisplayName(),
});


$prefs->migrate(4, sub {
	$prefs->set('imageres', '160x160');
	1;
});


$prefs->migrate(3, sub {
	$prefs->set('showmenuicon', 1);
	1;
});


$prefs->migrate(2, sub {
	$prefs->set('prefOrder_live', 'dash,hls,mp3,flashaac');
	$prefs->set('prefOrder_aod', 'dash,hls,mp3,flashaac');
	1;
});

sub initPlugin {
	my $class = shift;

	$prefs->init({ prefOrder_live => 'dash,hls,mp3,flashaac', 
					prefOrder_aod => 'dash,hls,mp3,flashaac',
					imageres => '160x160',
					transcode => 1, livetxt_classic_line => 0, 
					is_app => 0 , backskiptime => 15, forwardskiptime => 1,
					Dashspeed_aod => 1, Dashspeed_live => 1,
					menuurl => 0, loadmenuurl => '',
					livedelaystart => 15 , threshold => 255 });

	my $file = catdir( $class->_pluginDataFor('basedir'), 'menu.opml' );

	
		# Track Info item: skip back/forward X seconds or to beginning of program
	Slim::Menu::TrackInfo->registerInfoProvider( DashLiveRew => (
		before => 'bottom',
		func   => \&trackSkipBackMenu,
	) );
	
	# Segment Info menu 
	Slim::Menu::TrackInfo->registerInfoProvider( moresegmentsinfo => (
		before => 'bottom',
		func   => \&segmentsInfoMenu,
	) );
	
	

	$class->SUPER::initPlugin(
		feed   => Slim::Utils::Misc::fileURLFromPath($file),
		tag    => 'bbciplayer',
		is_app => $class->can('nonSNApps') && $prefs->get('is_app') ? 1 : undef,
		menu   => 'radios',
		weight => 1,
	);

	if (!$::noweb) {
		require Plugins::BBCiPlayer::Settings;
		Plugins::BBCiPlayer::Settings->new;
	}

	# hide iplayer:// and hls:// urls from track info displays...
	my $trackInfoUrl = Slim::Menu::TrackInfo->getInfoProvider->{'url'};
	my $old = $trackInfoUrl->{'func'};

	$trackInfoUrl->{'func'} = sub {
		my $info = &$old(@_);
		return undef if $info->{'label'} eq 'URL' && $info->{'name'} =~ /^iplayer:\/\/|^hls:\/\/|^dash:\/\/|^dashflac:\/\//;
		return $info;
	};
}

sub getDisplayName { 'PLUGIN_BBCIPLAYER' }

sub playerMenu { shift->can('nonSNApps') && $prefs->get('is_app') ? undef : 'RADIO' }


sub  SkipBackTime {
	my ($client, $cb, $newPos, $skipped_text) = @_;
	
	$log->info("SkipbackTime subroutine to $newPos");
	my $song = Slim::Player::Source::playingSong($client);
	my $livetime = $song->pluginData('streamtime');

	Slim::Player::Source::gototime($client, $newPos);
	
	$cb->({
		items => [{
			name        => $skipped_text,
			showBriefly => 1,
			nowPlaying  => 1, # then return to Now Playing
		}]
	});
}
	
sub SkipBacktoProgramStart {
	my ($client, $cb, $params) = @_;
	my $song = Slim::Player::Source::playingSong($client);
	
	$log->info("Skip back to start of program ");

	my $pctime   = Time::HiRes::time();
	$log->info('Time now = '. $pctime 
					. ' UTC difftime ='.    $song->pluginData->{'dash_timediff'}  
					. ' starttime ='   .    $song->pluginData->{'rp_starttime'}  
					. ' service name ='.	$song->pluginData->{'rp_name'}        
					. ' description  ='.	$song->pluginData->{'rp_description'} );
	
	my $newPos   = int ($pctime - $song->pluginData->{'rp_starttime'} +  $song->pluginData->{'dash_timediff'} );

	$song->pluginData('liveskipbackreq',$song->pluginData->{'rp_starttime'});
	$song->pluginData('liveskipbackstate','pending');
	$song->pluginData('skipbackdisplay',1);
	$song->pluginData('skipbackdisplayalbum', $song->pluginData->{'rp_description'});
	$song->pluginData('skipbackdisplayartist',$song->pluginData->{'rp_name'});
	$song->pluginData('skipbackdisplayicon',  $song->pluginData->{'rp_icon'});
	$song->pluginData('skipbackdisplaystop',  $song->pluginData->{'rp_stoptime'});

	my $position = Slim::Player::Source::songTime($client);			
	$log->info(sprintf("Skipping from position %d back to program start at %d (%d)", $position, $newPos,$song->pluginData->{'rp_starttime'}));
	my $statusmsg = sprintf($client->string('PLUGIN_BBCIPLAYER_SKIPBACKSTATUS'),int($newPos/60));
	SkipBackTime($client,$cb,$song->pluginData->{'rp_starttime'}, $statusmsg);
}

sub SkipForwardDeltaTime {
	my ($client, $cb, $params) = @_;

	my $song = Slim::Player::Source::playingSong($client);	
	my $streamtime = $song->pluginData('streamtime');
	my $newPos   = ($prefs->get('forwardskiptime') * 60) + $streamtime;
# see if newpos is withing about 1 min of live then make stream live 
	my $livedelta = ($song->pluginData('dash_timediff') + Time::HiRes::time()) - $newPos ;

	$log->info("streamtime=$streamtime newpos=$newPos  livedelta=$livedelta Now=". Time::HiRes::time() . " Timediff=". $song->pluginData('dash_timediff')); 	

	
	if ($livedelta > 70 ) {

		$song->pluginData('liveskipbackreq',$newPos);
		$song->pluginData('liveskipbackstate','pending');

		my $position = Slim::Player::Source::songTime($client);			
		$log->debug(sprintf("Skipping from position %s forward by %s", $position, $newPos));
		my $statusmsg = sprintf($client->string('PLUGIN_BBCIPLAYER_DASHFORWARDSKIPMSG'),$prefs->get('forwardskiptime'));
		SkipBackTime($client,$cb,$newPos,$statusmsg);
	} else {
		
# Short delay - cancel skip back.		
		$song->pluginData('liveskipbacktime',0);
		$song->pluginData('liveskipbackstate','live');
# Delete the metadata so all new stuff gets loaded.
		$song->pluginData->{'track' } = undef;
		$song->pluginData->{'artist'} = undef;

		delete $song->pluginData->{'rp_starttime'};
		delete $song->pluginData->{'rp_stoptime'};
		delete $song->pluginData->{'rp_name'} ;
		delete $song->pluginData->{'rp_description'};
		delete $song->pluginData->{'rp_servicename'};
		delete $song->pluginData->{'rp_icon'};		
		
		SkipBackTime($client,$cb,0,$client->string('PLUGIN_BBCIPLAYER_CANCELSKIPBACKSTATUS'));
	}

}

sub SkipBackwardDeltaTime {
	my ($client, $cb, $params) = @_;

	my $song = Slim::Player::Source::playingSong($client);	
	my $streamtime = $song->pluginData('streamtime');
	my $livestreamtime = $song->pluginData('dash_timediff') + Time::HiRes::time();

	$log->info("Streamtime = $streamtime livestreamtime = $livestreamtime");
				
	my $newPos   =  $streamtime - ($prefs->get('backskiptime') * 60);
	$song->pluginData('liveskipbackreq',$newPos);
	$song->pluginData('liveskipbackstate','pending');
#
# If we skip back before programn start and were displaying program info - turn offproigram info
#
	if (($newPos < $song->pluginData->{'rp_starttime'}) && ( $song->pluginData('skipbackdisplay') == 1 )) {
		delete $song->pluginData->{'skipbackdisplay'};
	}
				
	my $position = Slim::Player::Source::songTime($client);			
	$log->debug(sprintf("Skipping from position %s backward to %s", $position, $newPos));
	
	my $statusmsg = sprintf($client->string('PLUGIN_BBCIPLAYER_DASHBACKWARDSKIPMSG'),$prefs->get('backskiptime'));

	SkipBackTime($client,$cb,$newPos,$statusmsg);

}

sub CancelSkipBack {
	my ($client, $cb, $params) = @_;
				
	$log->info("Cancel skip back time ");
	
	my $song = Slim::Player::Source::playingSong($client);	
	$song->pluginData('liveskipbackstate','live');
	$song->pluginData('liveskipbacktime',0);

# Delete the metadata so all new stuff gets loaded.
	$song->pluginData->{'track' } = undef;
	$song->pluginData->{'artist'} = undef;

	delete $song->pluginData->{'rp_starttime'};
	delete $song->pluginData->{'rp_stoptime'};
	delete $song->pluginData->{'rp_name'} ;
	delete $song->pluginData->{'rp_description'};
	delete $song->pluginData->{'rp_servicename'};
	delete $song->pluginData->{'rp_icon'};
	
	SkipBackTime($client,$cb,0,$client->string('PLUGIN_BBCIPLAYER_CANCELSKIPBACKSTATUS'));
}

sub trackSkipBackMenu {
	my ( $client, $url, $track, $remoteMeta ) = @_;
	my $song = Slim::Player::Source::playingSong($client);

	$log->info("In TrackInfo Menu url=$url  stream format=".  $song->streamformat() . " song plugindata=". $song->pluginData('contenttype')) if defined($song);
	
	return unless $url && $client && $client->isPlaying;
	return unless ( $song->pluginData('transport') eq 'dash-dynamic') ;	
	return unless ( $song->pluginData('contenttype') eq 'aac');  # Better test needed in future - quick hack to stop skip on Flac format

	my $livestreamtime = $song->pluginData('dash_timediff') + Time::HiRes::time();

	$log->info(" skipback state " .$song->pluginData('liveskipbackstate') . " livestreamtime = $livestreamtime currenttime = ". $song->pluginData('streamtime'));

	return unless defined($song->pluginData('liveskipbackstate'));
	
	my @menus;

	if ($song->pluginData('bbconair') && ( $song->pluginData('rp_starttime') > 0 )) {
		push @menus, {
						name => $client->string('PLUGIN_BBCIPLAYER_SKIPBACKPROGRAMSTART'),
						url	 => \&SkipBacktoProgramStart,
						nextWindow => 'parent',
					};
	}
	
	if ($prefs->get('backskiptime')  != 0) {
		push @menus, {
						name => sprintf($client->string('PLUGIN_BBCIPLAYER_DASHBACKWARDSKIPMSG'), $prefs->get('backskiptime') ) ,
						url	 => \&SkipBackwardDeltaTime,
						nextWindow => 'parent',
					};
	};
	
	if (($prefs->get('forwardskiptime') != 0) && ($song->pluginData('liveskipbackstate') ne 'live')) {
		push @menus, {
						name => sprintf($client->string('PLUGIN_BBCIPLAYER_DASHFORWARDSKIPMSG'), $prefs->get('forwardskiptime') ),
						url	 => \&SkipForwardDeltaTime,
						nextWindow => 'parent',
					};
	};

	if (($song->pluginData('liveskipbackstate') ne 'live')) {
	push @menus, {
					name => $client->string('PLUGIN_BBCIPLAYER_CANCELSKIPBACK'),
					url	 => \&CancelSkipBack,
					nextWindow => 'parent',
				};
	}
	return \@menus;
}


sub segmentsInfoMenu {
	my ( $client, $url, $track, $remoteMeta ) = @_;

	my $song = Slim::Player::Source::playingSong($client);

	$log->debug("In SegmentsInfo Menu url=$url");

	return unless $url && $client && $client->isPlaying;
	return unless ($song->pluginData('transport') eq 'dash-static');

	if ($url =~ /^iplayer:\/\//) {
		
		my ($urltype, $urlparams) = $url =~ /iplayer:\/\/(live|aod)\?(.*)$/;
		return unless ($urltype eq 'aod');
	} elsif ( !(( $url =~ /dash:\/\/open.live.bbc.co.uk\/mediaselector\// ) || ( $url =~ /usp\/auth\/vod\/piff_abr_full_audio\// ) )) {
		return;
	};
	

	my $seglist = $song->pluginData->{"segmentlist"};
#	return unless defined($seglist);
	
	if ( !defined ($seglist)) {
		return if ($remoteMeta->{'duration'} < (15*60));
		my $segdur = ($remoteMeta->{'duration'} < (121*60)) ? 600: 900; # 10min segment for short prgram 15 mins segment for long programs  
		my @segmentlist;
		my $segcount = 1;
		for (my $offset = $segdur; $offset < $remoteMeta->{'duration'};) {
			
			push @segmentlist, {'offset' => $offset, 'title' => " Segment $segcount"};
			$offset += $segdur;
			$segcount++;
		}
		$seglist = \@segmentlist;
	};	

	my $items = [];
	push @$items, {'name' => "Playing: ". $song->pluginData->{"playingseg"},
				nextWindow => 'parent'};

	foreach my $segment (@$seglist) {
		use integer;
		my $mins = $segment->{'offset'} / 60;
		my $secs = $segment->{'offset'} % 60;
		
		my @timeparts = gmtime($segment->{'offset'});
		my $timestr = ($timeparts[2] == 0) ? sprintf ("%d:%02d ",@timeparts[1,0]) : sprintf ("%d:%02d:%02d ",@timeparts[2,1,0]) ;
			
#		push @$items, {'name' => sprintf("%3d:%02d %s - %s", $mins,$secs,$segment->{'title'}, $segment->{'artist'}),
		push @$items, {'name' => $timestr . $segment->{'title'} ." - ". $segment->{'artist'},
				url  => sub {
					my ($client, $cb, $params) = @_;
					my $position = Slim::Player::Source::songTime($client);
					$log->debug(sprintf("Skipping from position %d back to %d", $position, $segment->{'offset'}));
					Slim::Player::Source::gototime($client, $segment->{'offset'});
			
					$cb->({
						items => [{
							name        => $segment->{'title'}. " - ".  $segment->{'artist'},
							showBriefly => 1,
							nowPlaying  => 1, # then return to Now Playing
						}]
					});
				},
				nextWindow => 'parent'
			};
	};
			
	return {
		name => cstring($client, 'PLUGIN_BBCIPLAYER_SEGMENTSINFO'),
		type => 'outline',
		items => $items,
	}; 
}
1;
