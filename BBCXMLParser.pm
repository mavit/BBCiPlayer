package Plugins::BBCiPlayer::BBCXMLParser;

# Plugin to play live and on demand BBC radio streams
# (c) Triode, 2007-2015, triode1@btinternet.com
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;

use Slim::Utils::Log;
use Slim::Utils::Prefs;
use XML::Simple;
use Date::Parse;
use URI::Escape qw(uri_escape_utf8 uri_unescape);

use Data::Dumper;

my $log = logger('plugin.bbciplayer');
my $prefs = preferences('plugin.bbciplayer');

# split option string into options and filters
# valid options are:
# filter:<name>=<regexp> - filter to only include where <name> matches <regexp> 
# byday                  - include by day menus
# bykey                  - include menus grouped by brand/serial/title
# nocache                - don't cache parsed result [cacheing is per url so may need to turn off]
# reversedays            - list days in reverse order (use with byday)
# reverse                - list entries in reverse order (in list or by key display)

sub _getopts {
	my $class  = shift;
	my $optstr = shift;
	my $opts   = shift;
	my $filters= shift;

	for my $opt (split /\&/, $optstr) {
		if    ($opt =~ /filter:(.*)=(.*)/) { $filters->{lc($1)} = $2 }
		elsif ($opt =~ /(.*)=(.*)/       ) { $opts->{lc($1)}    = $2 } 
		else                               { $opts->{lc($opt)}  =  1 }
	}
}

sub parse {
    my $class  = shift;
    my $http   = shift;
    my $optstr = shift;

    my $params = $http->params('params');
    my $url    = $params->{'url'};
	my $opts   = {};
	my $filters= {};
	
	my $imageres = $prefs->get('imageres');


	$class->_getopts($optstr, $opts, $filters);

	my $maxdays = (exists($filters->{'maxdays'})) ?  $filters->{'maxdays'} : 7 ;
	$maxdays = 30 if ($maxdays > 30);
	
	delete $filters->{'maxdays'};
	my $now = time();

	my $xml = eval {
		XMLin( 
			$http->contentRef,
#			KeyAttr    => { parent => 'type', link => 'transferformat', image => 'entity_type' },
			KeyAttr    => { parent => 'type', image => 'entity_type' },
			GroupTags  => { links => 'link', parents => 'parent' },
			ForceArray => [ 'parent', 'link' ]
		)
	};

	if ($@) {
		$log->error("$@");
		return;
	}

	my @weekdays = qw(Sunday Monday Tuesday Wednesday Thursday Friday Saturday);
	my @months   = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);

	my $today = (localtime($now))[3];

	my %byDay;
	my %byKey;
	my %list;
 
	my %filter;
	my %programmeSeen; # Used to eliminate programme repeats
	
	# parse xml response into menu entries
    
	ENTRY: for my $entry (@{$xml->{'entry'}}) {

		my $title = $entry->{'title'};  

		# move info to top level of entry so we can filter on it
#		$entry->{'hlsurl'} = $entry->{'links'}->{'hls'}->{'content'};
#		$entry->{'dashurl'} = $entry->{'links'}->{'dash'}->{'content'};
		
		foreach my $link ( @{$entry->{'links'}} ) {
			if ( $link->{'type'} eq 'bbcsounds' ) {
			        $entry->{'bbcsoundsurl'} = $link->{'content'};
			} elsif ( $link->{'type'} eq 'mediaselector' && exists($link->{'transferformat'})) {
				if (      $link->{'transferformat'} eq 'hls' ) {
					$entry->{'hlsurl'}  = $link->{'content'};
				} elsif ( $link->{'transferformat'} eq 'dash') {
					$entry->{'dashurl'} = $link->{'content'};
				} else {
					$log->error(" Unknown transferformat type ". Dumper(\$link));
				}
			} else {
					$log->error(" Unknown link type ". Dumper(\$link));
			}	
		}
		
		
		for my $info (keys %{$entry->{'parents'}}) {
			$entry->{ lc($info) } = $entry->{'parents'}->{ $info }->{'content'};
		}

		# filter out entries which don't match the filter criteria specified by $optstr
		# note that the filter value is expected to be a regex, this provides a 'wild-card' mechanism		
		for my $filter (keys %$filters) {
			if (!$entry->{$filter} || $entry->{$filter} !~ $filters->{$filter}) {
				$log->info("ignoring $title [$filter=$entry->{$filter} filter=$filters->{$filter}]");
				next ENTRY;
			}
		}
		
		my $availStart = str2time($entry->{'availability'}->{'start'});

		# $availEnd will be "undef" when $entry->{'availability'}->{'end'} is blank.
		# Typically occurs when the feed item relates to a future programme (feed includes 48 hours
		# of future programming). But not always...
		my $availEnd   = str2time($entry->{'availability'}->{'end'}) ;   # result is "undef" if date is blank or invalid

		# don't include if the program is not available now (feed includes programs which can't be played)

#   MRW - fix to exclude test from executing and ignoring programs.
if (0) {
		# MRW note 10 May 2020: Some programmes, (Radio 4 In Our Time, Tweet of the Day, and others)
		# have been observed with availability start and end firmly stated to be 2015, 2016, 2019, whatever.
		# This despite the fact that the broadcasts are current. 
		# So curtail this test for now, as it seems to be generating too many false negatives.
 		if ($availStart > $now || $availEnd && $availEnd < $now) {
 			$log->info("ignoring $title [start=$availStart end=$availEnd now=$now]");
 			next ENTRY;
 		}
}		

		my $start = str2time($entry->{'broadcast'}->{'start'});
		my $end   = str2time($entry->{'broadcast'}->{'end'}) ;

		# Do not include programmes whose broadcasts have not yet finished. Add 10 minutes for safety.
		# We need this check because we choose to ignore blank 'availability->end'. See above.
		if (($end + 600) > $now) {
			$log->info("ignoring broadcast not yet ended: $title [start=$start now=$now], start: " .
				$entry->{'broadcast'}->{'start'} . " end: " . $entry->{'broadcast'}->{'end'});
			next ENTRY;
		}
		
		# Since early September 2018, the quantity of available items has been substantially
		# increased, to about a month's worth. Let's restrict it to the last seven days.
		# Doesn't deal with summer time properly, but not important.
		if ( (int($now/86400) - int($start/86400)) > $maxdays) {
			$log->info("ignoring broadcast more than $maxdays days ago: $title [start=$start now=$now]");
			next ENTRY;
		};
		
		my $duration = $entry->{'broadcast'}->{'duration'};
		my ($min, $hour, $day, $wday) = (localtime($start))[1,2,3,6];

		# Strip dates from the title
		if ($title =~ /(.*?), \d+\/\d+\/\d+/) {
			$title = $1;
		}

		$log->is_info && $log->info("$title $weekdays[$wday] $hour:$min $entry->{url}");
		
# Determine the program parent PID  (i.e. not the pid attribute in the entry but separate piod element) as XMLin puts borth pids into an array.
# Second entry is usually what we want.
		$log->error("Entry array should have just two pids ".scalar (@{$entry->{'pid'}}) ) unless (scalar (@{$entry->{'pid'}}) == 2) ;
		my $program_parent_pid = $entry->{'pid'}[1];

		my $version_pid = $entry->{'broadcast'}->{'version_pid'};
		$log->warn("No version_pid for $title !") unless $version_pid;

		if ( $program_parent_pid eq $version_pid ) {
			$program_parent_pid = $entry->{'pid'}[0];
		}

		my $playlist_url = "http://www.bbc.co.uk/programmes/$program_parent_pid/playlist.json";

		# group by key - brand/series/title
		my $key;
		if ($opts->{'bykey'}) {
			if    ($entry->{'brand'} ) { $key = $entry->{'brand'};  $filter{$key} = "?filter:brand=\Q${key}\E";  }
			elsif ($entry->{'series'}) { $key = $entry->{'series'}; $filter{$key} = "?filter:series=\Q${key}\E"; }
			else                       { $key = $title;             $filter{$key} = "?filter:title=\Q${key}\E";  }
			# Note that we needed to quote the value supplied to the filter, because it is expected to be a
			# perl regex. Usually of no consequence, but occasionally titles show up containing punctuation.			
		}
	
		my $icon;
		if ( exists($entry->{'images'}->{'image'}->{'content'}) ) {
			$icon = $entry->{'images'}->{'image'}->{'content'};
		} else {
			$icon = $entry->{'images'}->{'image'}->{'episode'}->{'content'};
		} ;

		$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;

		# Check that hlsurl and dashurl include the essential 'vpid' stream id. Since about April/May 2020
		# it has been observed that this may be left blank until rather too near the time of broadcast. The
		# user could easily have an earlier cached copy of the availability schedule without the vpid element.

		# The deficient url's might be simply deleted (as they are of no use). However the vpid does seems to
		# end up being the current broadcast version pid. ($entry->{'broadcast'}->{'version_pid'})
		# So we take a punt with that.

		if (defined($entry->{hlsurl})) {
			# Should look like the first of these two:
			#  http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/vpid/m000hn5v/mediaset/audio-syndication/proto/http
			#  http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/vpid//mediaset/audio-syndication/proto/http
			my ($firstPart, $vpid, $lastPart) = $entry->{hlsurl} =~ m|^(.+vpid/)(.*)(/mediaset.+)$|;
			unless ($2) {
				delete $entry->{hlsurl}; # No use as is.
				$entry->{hlsurl} = "$1" . $version_pid . "$3" if $version_pid;
				if   ($version_pid) { $log->warn("hlsurl  lacks VPID element. Took a guess at $version_pid. Start: $entry->{'broadcast'}->{'start'} Title: $title") }
				else                { $log->warn("hlsurl  lacks VPID element. Deleted it because no info. Start: $entry->{'broadcast'}->{'start'} Title: $title") }
			}
		}

		if (defined($entry->{dashurl})) {
			# Should look like the first of these two:
			#  http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-syndication-dash/proto/http/vpid/m000hn5v
			#  http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-syndication-dash/proto/http/vpid/
			my ($firstPart, $vpid) = $entry->{dashurl} =~ m|^(.+vpid/)(.*)$|;
			unless ($2) {
				delete $entry->{dashurl}; # No use as is.
				$entry->{dashurl} = "$1" . $version_pid if $version_pid;;
				if   ($version_pid) { $log->warn("dashurl lacks VPID element. Took a guess at $version_pid. Start: $entry->{'broadcast'}->{'start'} Title: $title") }
				else                { $log->warn("dashurl lacks VPID element. Deleted it because no info. Start: $entry->{'broadcast'}->{'start'} Title: $title") }
			}
		}
		
 		my $iplayerparms ='';
 		$iplayerparms  .= "hls=$entry->{hlsurl}&"               if (defined($entry->{hlsurl}));
 		$iplayerparms  .= "dash=$entry->{dashurl}&"             if (defined($entry->{dashurl}));
 		$iplayerparms  .= "mp3=$entry->{bbcsoundsurl}&"   if (defined($entry->{bbcsoundsurl}));
 		
 		 $iplayerparms  .=  "dur=$duration&icon=$icon&title=" . uri_escape_utf8($title) .
							"&desc=" . uri_escape_utf8($entry->{'synopsis'}) ;

		$byDay{$day}{$start} = {
			'name'        => sprintf("%02d:%02d %s", $hour, $min, $title),
			'url'         => $playlist_url,
			'icon'        => $icon,
			'type'        => 'playlist',
			'description' => $entry->{'synopsis'},
			'on_select'   => 'play',
			'parser'      => "Plugins::BBCiPlayer::PlaylistParser?$iplayerparms",				
		} if ($opts->{'byday'});


		# We choose not to see repeated programmes in the ‘byKey’ and ‘list’ menus,
		# it's just noise.
		# The programme pid should be the ideal key, but that's lost in an array of
		# two pids due to the way the XML is built and XMLin processes it.
		# So we use 'broadcast->version_pid' as our key, which is part of the
		# stream selector, easily retrieved, and just as good.
		# Actually, it might even be better ! E.g. "In Our Time" is repeated, but the
		# repeat is a shortened version. Same programme pid, but different version_pid.

		my $version_pid = $entry->{'broadcast'}->{'version_pid'};
		$log->warn("No version_pid for $title !") unless $version_pid;

		# Test for a version pid, in case xml changes and breaks things...
		unless ($version_pid && $programmeSeen{$version_pid}) {
			
			# Construct a textual part of the 'byKey' title by eliminating its first part,
			# which will be identical to '$key'.
			# e.g. "John Finnemore's Souvenir Programme: Series 4: Episode 5" has
			# Brand "John Finnemore's Souvenir Programme" and Series "Series 4". In this
			# case $key would be the brand.

			my $byKeyTitle = $title;
			$byKeyTitle =~ s/^\Q${key}\E:\s*//g;
 			$byKey{$key}{$start} = {
				'name'        => sprintf("%s - %02d:%02d %s", $byKeyTitle, $hour, $min, $day == $today ? 'Today' : $weekdays[$wday]),
				'url'         => $playlist_url,
				'icon'        => $icon,
				'type'        => 'playlist',
				'description' => $entry->{'synopsis'},
				'on_select'   => 'play',
				'parser'      => "Plugins::BBCiPlayer::PlaylistParser?$iplayerparms",				
			} if ($opts->{'bykey'});

			$list{$start} = {
				'name'        => sprintf("%02d:%02d %s - %s", $hour, $min, $day == $today ? 'Today' : $weekdays[$wday], $title),
				'url'         => $playlist_url,
				'icon'        => $icon,
				'type'        => 'playlist',
				'description' => $entry->{'synopsis'},
				'on_select'   => 'play',
				'parser'      => "Plugins::BBCiPlayer::PlaylistParser?$iplayerparms",				
			} unless ($opts->{'byday'} || $opts->{'bykey'});
			$programmeSeen{$version_pid} = 1 if $version_pid;
		}
		else {
			$log->is_info && $log->info("Supressing repeat: $title");
		}
	}

	# create menus
	
	my @menu;

	# create the by day menu
	if ($opts->{'byday'}) {

		my $first = $opts->{'reversedays'} ? $today : ($today - 15) % 32;
		my $day = $first;

		do {
			my @submenu;
			my $wday;
			my $mon;

			my @times = sort keys %{$byDay{$day}};
			
			for my $time (@times) {
				push @submenu, $byDay{$day}{$time};
				$wday ||= (localtime($time))[6];
				$mon  ||= (localtime($time))[4];
			}

			if (@submenu) {
				push @menu, {
					'name'  => ($day == $today ? 'Today' : "$weekdays[$wday]") .
						      ($maxdays < 8 ? '' : "  $day-$months[$mon]"),
					'items' => \@submenu,
					'icon'  => Plugins::BBCiPlayer::Plugin->_pluginDataFor('icon'),
					'type'  => 'opml',
				};
			}
			
			$day = $opts->{'reversedays'} ? ($day - 1) % 32 : ($day + 1) % 32;
			
		} while ($day != $first);
	}

	# create the by brand/series/title menu entries, promoting single entrys to top level
	if ($opts->{'bykey'}) {

		for my $key (sort keys %byKey) {

			my @times = $opts->{'reverse'} ? reverse sort keys %{$byKey{$key}} : sort keys %{$byKey{$key}};

			if (scalar @times == 1) {

				# only one entry of this title - put it at the top level
				$byKey{$key}{$times[0]}->{'name'} = $key;
				push @menu, $byKey{$key}{$times[0]};
				
			} else {
				
				my @submenu;
				my $icon = Plugins::BBCiPlayer::Plugin->_pluginDataFor('icon');
				
				# create sub menu ordered by start date
				for my $time (@times) {
					my $mon      = (localtime($time))[4];
					my $day  = (localtime($time))[3];
					$byKey{$key}{$time}->{'name'} = $byKey{$key}{$time}->{'name'} . " $day $months[$mon]" unless ($maxdays < 8);
					push @submenu,  $byKey{$key}{$time};
					$icon = $byKey{$key}{$time}->{'icon'} if $byKey{$key}{$time}->{'icon'};
				}
				
				$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;
				
				push @menu, {
					'name'    => $key,
					'items'   => \@submenu,
					'type'    => 'opml',
					'icon'    => $icon,
					# add info to allow this menu to be bookmarked as a favorite
					# We use a url fragment identifier to identify this particular item, as
					# LMS will cache the generated XML feed against the full url, including the
					# fragment id. Therefore it will be distinguished from the 'normal' XML feed,
					# which has no fragment id.
					# The bookmarking option is not available in Jive or ip3k, only through the web
					# interface. But nevertheless useful for bookmarking series like, say,
					# R3 'Composer of the Week'.
					# - fetched means it is not fetched again while browsing into this menu
					'url'     => "$url#$key",
					'parser'  => 'Plugins::BBCiPlayer::BBCXMLParser' . $filter{$key},
					'fetched' => 1,
				};
			}
		}
	}

	# add in list entries (if byday, bykey not used)
	my @times = $opts->{'reverse'} ? reverse sort keys %list : sort keys %list;
	
	for my $time (@times) {
		push @menu, $list{$time};
	}

	# return xmlbrowser hash
	return {
		'name'    => $params->{'feedTitle'},
		'items'   => \@menu,
		'type'    => 'opml',
		'nocache' => $opts->{'nocache'},
	};
}

1;
