package Plugins::BBCiPlayer::Settings;

# Plugin to play live and on demand BBC radio streams
# (c) Triode, 2007-2015, triode1@btinternet.com
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Modified: 04/2015 by utgg
#           Added http polling for OnAir text information
#

use strict;

use base qw(Slim::Web::Settings);

use Slim::Utils::Prefs;
use File::Slurp;
use File::Spec::Functions qw(:ALL);
use File::Temp qw(tempfile);
use Slim::Networking::SimpleAsyncHTTP;
use XML::Simple;
use Slim::Utils::Strings qw(string);
use Slim::Utils::Log;
use Data::Dumper;

my $log   = logger('plugin.bbciplayer');

sub name {
	return 'PLUGIN_BBCIPLAYER';
}

sub page {
	return 'plugins/BBCiPlayer/settings/basic.html';
}

sub prefs {
	return (preferences('plugin.bbciplayer'), qw(prefOrder_live prefOrder_aod transcode
												 radiovis_txt radiovis_slide livetxt_classic_line 
												 onair_txt supplier_pref is_app 
												 backskiptime forwardskiptime Dashspeed_aod Dashspeed_live
												 menuurl loadmenuurl showmenuicon livedelaystart threshold imageres
												  ));
}

sub beforeRender {
	my $class = shift;
	my $params= shift;

	if ($params->{'prefs'}->{'pref_prefOrder_live'} =~ /flash/ || $params->{'prefs'}->{'pref_prefOrder_aod'} =~ /flash/) {
		require Plugins::BBCiPlayer::RTMP;
	}

#   Fix - show_app had output from "can" function which was a code reference 
#   which it seems was executed in Template and not just treated as a value.
	$params->{'show_app'} = Slim::Plugin::Base->can('nonSNApps') ? 1 : 0 ;
	$params->{'prefs'}->{'pref_loadmenuurl'} = 0;
	$params->{'prefs'}->{'pref_menuurl'} = 'http://downloads.sourceforge.net/project/bpaplugins/default-menu.opml';


	my %fmtmap = (
		dash     => 'DASH',
		hls      => 'HLS',
		mp3      => 'MP3',
		flashaac => 'FlashAAC',
		flashmp3 => 'FlashMP3',
		dashflac => 'DashFlac',
	);

	my @opts = (
		'hls,dash,mp3,flashaac,flashmp3',
		'hls,mp3,flashaac,flashmp3',
		'hls,dash,mp3,flashaac',
		'hls,mp3,flashaac',
		'hls,dash',
		'hls',
		'dash,hls,mp3,flashaac',
		'dash,mp3,flashaac',
		'dash,hls',
		'dash',
		'dashflac,dash',
	);

	my @prefOpts = ();

	for my $opt (@opts) {
		push @prefOpts, { opt => $opt, disp => join(" > ", map { $fmtmap{$_} } split(/,/, $opt)) };
	}

	my @prefskipOpts  = ( 	{ backtime =>  0, backtimetext =>  'disabled'},
							{ backtime =>  1, backtimetext =>  '1 min'},
							{ backtime =>  2, backtimetext =>  '2 min'},
							{ backtime =>  5, backtimetext =>  '5 min'},
							{ backtime => 10, backtimetext => '10 min'},
							{ backtime => 15, backtimetext => '15 min'},
							{ backtime => 30, backtimetext => '30 min'} );
	
	my @preflivedelayOpts =({ livedelay =>    0, livedelaytext =>  'no delay'},
							{ livedelay =>    5, livedelaytext =>  ' 5 secs'},
							{ livedelay =>   10, livedelaytext =>  '10 secs'},
							{ livedelay =>   15, livedelaytext =>  '15 secs'},
							{ livedelay =>   30, livedelaytext =>  '30 secs'},
							{ livedelay =>   45, livedelaytext =>  '45 secs'},
							{ livedelay =>   61, livedelaytext =>  '1 min'},
							{ livedelay =>  121, livedelaytext =>  '2 min'},
							{ livedelay =>  181, livedelaytext =>  '3 min'},
							{ livedelay =>  241, livedelaytext =>  '4 min'},
							{ livedelay =>  301, livedelaytext =>  '5 min'});

	my @prefthresholdOpts =({ threshold =>    20,  thresholdtext =>  '20'},
							{ threshold =>    50,  thresholdtext =>  '50'},
							{ threshold =>    100, thresholdtext => '100'},
							{ threshold =>    150, thresholdtext => '150'},
							{ threshold =>    255, thresholdtext => '255'} );


	my @prefimageresOpts  =({ imageres  =>   "160x160",  imagerestext  => '160x160'},
							{ imageres  =>   "240x240",  imagerestext =>  '240x240'},
							{ imageres  =>   "320x320",  imagerestext =>  '320x320'},
							{ imageres  =>   "480x480",  imagerestext =>  '480x480'},
							{ imageres  =>   "160x90",   imagerestext =>  '160x90'},
							{ imageres  =>   "240x135",  imagerestext =>  '240x135'},
							{ imageres  =>   "320x180",  imagerestext =>  '320x180'},
							{ imageres  =>   "480x270",  imagerestext =>  '480x270'},
							{ imageres  =>   "640x360",  imagerestext =>  '640x360'});


	$params->{'opts'}          = \@prefOpts;
	$params->{'skipopts'}      = \@prefskipOpts;
	$params->{'livedelayopts'} = \@preflivedelayOpts;
	$params->{'thresholdopts'} = \@prefthresholdOpts;
	$params->{'imageresopts'}  = \@prefimageresOpts;
	
	
}

#sub handler {
#	my ($class, $client, $params) = @_;
#	$log->debug("BBCiPlayer::Settings->handler() called." . Dumper($params));
#
#	return $class->SUPER::handler( $client, $params );
#}

sub handler {
	my ($class, $client, $params, $callback, @args) = @_;
#	$log->info("handler called " . Dumper(keys($params)));

	
	if ( $params->{'pref_loadmenuurl'} ) {
		$class->LoadMenuUrl($client, $params, $callback, \@args);
		return;
	}

	my $body = $class->SUPER::handler($client, $params);
	return $callback->( $client, $params, $body, @args );
	
}

sub LoadMenuUrl {
	my ( $class, $client, $params, $callback, $args ) = @_;

	my $url = $params->{'pref_menuurl'};
	
	my $ecb = sub {
		my ( $error ) = @_;
		
		$log->info( "Error loading replacement menu.opml error: $error" );
		$params->{warning} .= '<span style="color: red;">' .string( 'PLUGIN_BBCIPLAYER_LOADMENU_ERROR', $error ) . '</span>';
	
		$class->saveSettings( $client, $params, $callback, $args );
	};


	if ( $url ) {
		$log->info( "Trying to get replacement menu.opml from: $url" );
		
		$class->getMenuOpmlAsync(
			sub {
					my ( $content ) = @_;
					my $tempmenu;
					my $success = 0;
					my $error = " ";
					my $menudir = Plugins::BBCiPlayer::Plugin->_pluginDataFor('basedir') ;
					my $menufile = catdir( $menudir, 'menu.opml' );
					my $menubackup = $menufile . ".backup";

					(undef, $tempmenu) = tempfile(TEMPLATE => 'menuopml-XXXXXX', DIR => $menudir, SUFFIX => '.tmp',
													UNLNK => 0, OPEN => 0);	
					$log->debug(" Menu temp file name: $tempmenu");			
					if (write_file( $tempmenu, \$content )) {
						if (rename ($menufile, $menubackup)) {
							if (rename ($tempmenu, $menufile)) {
								$log->info("Successful Write of menu.opml"); 
								$success =1 ;
							} else {
								$log->error("Failed renamed $tempmenu to $menufile"); 
								$error = "couldn't rename temp file to menu.opml ";
							}
						} else {
							$log->error("Failed renamed $menufile to $menubackup"); 
							$error = "couldn't rename menu.opml file to menu.opml.backup ";
							}
					} else {
						$log->error("Failed Write $tempmenu"); 
						$error = "couldn't write new  menu.opml file to $tempmenu ";

					};
					delete $params->{saveSettings};
					if ($success) {
						$params->{warning} .= '<span style="color: green;">' .string( 'PLUGIN_BBCIPLAYER_LOADMENU_SUCCESS', $error ) . '</span>';
					} else {
						$params->{warning} .= '<span style="color: red;">' .string( 'PLUGIN_BBCIPLAYER_LOADMENU_FAILED', $error ) . '</span>';
					}

					$class->saveSettings( $client, $params, $callback, $args );
				},
			$ecb,
			{
				url     => $url,
				timeout => 15,
			}
		);
	}
	else {
		$ecb->(string('PLUGIN_BBCIPLAYER_LOADMENU_FAILED'));
	}
}

sub getMenuOpmlAsync {
	my $class = shift;
	my ( $cb, $ecb, $params ) = @_;
		
	my $url = $params->{'url'};

	$log->info(" Successful read of new menu.opml URL");
	if (Slim::Music::Info::isFileURL($url)) {

		my $path    = Slim::Utils::Misc::pathFromFileURL($url);

		# read_file from File::Slurp
		my $content = eval { read_file($path) };
		if ( $content ) {
			return $cb->( $content, $params );
		} else {
			return $ecb->( "Unable to open file '$path'", $params );
		}
	}

	my $http = Slim::Networking::SimpleAsyncHTTP->new(
		\&gotMenuOpmlHTTP, \&gotMenuOpmlErrorHTTP, {

			'params'  => $params,
			'cb'      => $cb,
			'ecb'     => $ecb,
			'cache'   => 1,
			'expires' => $params->{'expires'},
			'Timeout' => $params->{'timeout'},
	});

	$log->info("Async request: $url");
	
	my $ua = Slim::Utils::Misc::userAgentString();
	$ua =~ s{iTunes/4.7.1}{Mozilla/5.0};

#	my %headers = (
#		'User-Agent'   => $ua,
#		'Icy-Metadata' => '',
#	);

#	$http->get( $url, %headers );
	
	$http->get( $url );
}

sub gotMenuOpmlHTTP {
	my $http = shift;
	my $params = $http->params();
	
	my $ct = $http->headers()->content_type;

	$log->debug("Content type is $ct url " . $http->url  );

	my $xml = eval { XMLin( $http->contentRef)};

	if ($@) {
		# call ecb
		my $ecb = $params->{'ecb'};
		$log->error("BBCiPlayer load menu.opml XML Parse failed: $@");
		$ecb->( string('PLUGIN_BBCIPLAYER_XML_FAILED') . "XML parse error", $params->{'params'} );
		return;
	}

	if ( !ref $xml || ref $xml ne 'HASH' ) {
		# call ecb
		my $ecb = $params->{'ecb'};
		$ecb->( string('PLUGIN_BBCIPLAYER_XML_FAILED') . " XML empty", $params->{'params'} );
		return;
	}

	# call cb
	my $cb = $params->{'cb'};
	$cb->( ${$http->contentRef}, $params->{'params'} );

	undef($http);
}

sub gotMenuOpmlErrorHTTP {
	my $http = shift;
	my $params = $http->params();

	logError(" Loading BBCiPlayer menu.opml from ", join("\n", $http->url, $http->error));

	# call ecb
	my $ecb = $params->{'ecb'};
	$ecb->( $http->error, $params->{'params'} );
}

sub saveSettings {
	my ( $class, $client, $params, $callback, $args ) = @_;
	
	my $body = $class->SUPER::handler($client, $params);
	return $callback->( $client, $params, $body, @$args );
}

1;
