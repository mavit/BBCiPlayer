package Plugins::BBCiPlayer::PlaylistParser;

use strict;

use Slim::Utils::Prefs;
use JSON::XS::VersionOneAndTwo;
use Slim::Utils::Log;
use Data::Dumper;
use URI::Escape qw(uri_unescape);
use Encode;

my $log   = logger('plugin.bbciplayer');
my $prefs = preferences('plugin.bbciplayer');


sub _getopts {
	my $class  = shift;
	my $optstr = shift;
	my $opts   = shift;

	for my $opt (split /\&/, $optstr) {
		if ($opt =~ /(.*)=(.*)/       ) { $opts->{lc($1)}    = $2 } 
		else                            { $opts->{lc($opt)}  =  1 }
	}
}
sub uri_unescape_utf8 {
    my $str = shift;
    return Encode::decode('utf8', uri_unescape($str));
}

sub parse {
    my $class  = shift;
    my $http   = shift;
    my $optstr = shift;
    my $params = $http->params('params');
    my $url    = $params->{'url'};

	my $opts   = {};

	$class->_getopts($optstr, $opts);

	$log->info("Playlist URL  =". $url );
	$log->info("Options selected for parser :".Dumper(\$opts));

	my $imageres = $prefs->get('imageres');

	my $json = eval { from_json( Encode::decode_utf8(${$http->contentRef} )) };	
	if ($@) {
		$log->error("$@");
		return;
	};

	my $playlistitem = $json->{'defaultAvailableVersion'}->{'smpConfig'};
	my $radioitem;
	my $itemduration;
	foreach my $item  (@{$playlistitem->{'items'}}) {
		if (($item->{'kind'} eq 'radioProgramme') ||( $item->{'kind'} eq 'programme') ) {
			$radioitem = $item;
			$itemduration = $item->{'duration'};
			last;
		};
	}
	
	if ( ! defined($radioitem)) {
		$log->error("playlistitem no radioprogramme or programme found \n", Dumper($playlistitem));
		return;
	}; 
	
	$itemduration = $opts->{dur} unless defined($itemduration);
### iplayer://aod?dash=http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-syndication-dash/proto/http/vpid/p073nw9c
	my $stream ='iplayer://aod?';	
	my $pid = $json->{'defaultAvailableVersion'}->{'pid'};

	if (scalar (@{$json->{'defaultAvailableVersion'}->{'types'}}) == 1) {
			my $programtype = $json->{'defaultAvailableVersion'}->{'types'}[0];
			
			if (( $programtype eq 'Original') || ($programtype eq 'Shortened') || ($programtype eq 'Other')  )  {
					$stream .= "dash=$opts->{dash}&" if (defined ($opts->{'dash'}) ) ;
					$stream .= "hls=$opts->{hls}&"   if (defined ($opts->{'hls'}) ) ;
			} elsif ($programtype eq 'Podcast') {
					$stream .= "mp3=http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-nondrm-download/proto/http/vpid/$pid.mp3&";
					$stream .= "dash=http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-syndication-dash/proto/http/vpid/$pid&";
			} elsif ($programtype eq 'Lengthened') {
					$stream .= "mp3=http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-nondrm-download/proto/http/vpid/$pid.mp3&";
					$stream .= "dash=http://open.live.bbc.co.uk/mediaselector/6/redir/version/2.0/mediaset/audio-syndication-dash/proto/http/vpid/$pid&";
			} else {
					$log->error("Unknown program type: $programtype for program $pid");
			};
	} else {
		$log->error(" Unexpected number of types :\n" . Dumper(@{$json->{'defaultAvailableVersion'}->{'types'}}));
	}; 

	$opts->{'icon'} =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;
	
	$stream .= "dur=$itemduration&icon=$opts->{icon}&title=$opts->{title}&desc=$opts->{desc}";

	return {
		'type'  => 'opml',
		'items' => [ {
			'name'        => uri_unescape_utf8($opts->{'title'}),
			'url'         => $stream,
			'type'        => 'audio',
			'icon'        => $opts->{'icon'},
			'description' => uri_unescape_utf8($opts->{'desc'}),
		} ],
		'cachetime' => 0,
		'replaceparent' => 1,
	};

}


# Local Variables:
# tab-width:4
# indent-tabs-mode:t
# End:

1;
