package Plugins::BBCiPlayer::HLS;

# HLS protocol handler
#
# (c) Triode, 2015, triode1@btinternet.com
#
# Released under GPLv2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

use strict;

use base qw(IO::Handle);

use Slim::Utils::Errno;
use Slim::Utils::Log;
use Slim::Utils::Prefs;

use URI::Escape qw(uri_unescape);
use Encode;

use AnyEvent::HTTP;
use URI;
use bytes;

use constant PRE_FETCH => 1; # number of chunks to prefetch
use constant HTTP_GET_TIMEOUT        => 30;
use constant IO_SELECT_FIXED => (Slim::Utils::Versions->compareVersions($::VERSION, '7.9.1') >= 0);

my $log = logger('plugin.bbciplayer.hls');
my $prefs = preferences('plugin.bbciplayer');

Slim::Player::ProtocolHandlers->registerHandler('hls', __PACKAGE__);

my $useragentstring = Slim::Utils::Misc::userAgentString() ;


my $codecIds = {
	"mp4a.40.1" => "AAC",
	"mp4a.40.2" => "AAC LC",
	"mp4a.40.5" => "AAC SBR",
};


sub uri_unescape_utf8 {
	my $str = shift;
	return Encode::decode('utf8', uri_unescape($str));
}


sub new {
	my $class = shift;
	my $args = shift;

	my $song     = $args->{'song'};
	my $url      = ($song->can('streamUrl') ? $song->streamUrl : $song->{'streamUrl'}) || $args->{'url'};
	my $seekdata = $song->can('seekdata') ? $song->seekdata : $song->{'seekdata'};
	my $start    = $seekdata->{'timeOffset'};

	if ($start) {
		if($song->can('startOffset')) {
			$song->startOffset($start);
		} else {
			$song->{startOffset} = $start;
		}
		$args->{'client'}->master->remoteStreamStartTime(Time::HiRes::time() - $start);
	}

	$url =~ s/^hls/http/;
	$url =~ s/\|$//;

	$log->debug("open $url $start");

	my $self = $class->SUPER::new;

	${*$self}{'song'} = $song;
	${*$self}{'_chunks'} = [];
	${*$self}{'_pl_noupdate'} = 0;
	${*$self}{'_played'} = 0;
	# HLS Listen Again streaming requires cookies, otherwise '403' error
	# Curiously, HLS Live Streaming does not need cookies
	${*$self}{'_cookies'} = {};

	$self->_fetchPL($url, $start);

	return $self;
}

sub _make_BaseUrl {

	my $mpdurl  = shift;
	my $baseurl = shift;
	
	if ($baseurl =~ /^http:\/\//) {
	  return $baseurl;
	}
	  
	my $url = URI->new_abs($baseurl,$mpdurl);
	return $url->canonical->as_string;
}

use constant HLS_FETCHDELAY_WARNING => 1000; # msecs
use constant HLS_PERSISTENT_TIMEOUT => 20;
use constant HTTP_GET_TIMEOUT       => 30;

sub _fetchPL {
	my $self = shift;
	my $url  = shift;
	my $start= shift || 0;

	$log->info("fetch playlist: $url start: $start, refetch=" . ${*$self}{'_pl_refetch'});

	my $old_refetch = ${*$self}{'_pl_refetch'} ;
	${*$self}{'_pl_refetch'} = undef;	

# Save guard object so that timer etc get cancelled if stream is stopped when ${*$self} object is destroyed.	
	${*$self}{'_pl_hlsfetch'} = http_request (	GET => $url,
					headers => { 'User-Agent' => $useragentstring },
					timeout => HTTP_GET_TIMEOUT,
					cookie_jar => ${*$self}{'_cookies'},

					sub {
							my ($body, $hdr) = @_;
							if ($hdr->{'Status'} eq '200') {
								_parsePL($body, $hdr, $self, $url, $start, $old_refetch);
							} else {
								$log->error("Closing stream - Fetch HLS error HTTP status = ". $hdr->{'Status'} . " Reason: ". $hdr->{'Reason'});
						  		${*$self}{'_pl_refetch'} = time() + 10;
								${*$self}{'_pl_url'}  = $url;
								${*$self}{'_pl_hlsfetch'} = undef;
								$self->close;
							}
					});
}

sub _parsePL {
	
	my ($body, $hdr, $self, $url, $start, $refetch) = @_;

	my $hls_url = $hdr->{'URL'};

	my $is_debug = $log->is_debug;
	$is_debug && $log->debug("got: $url start: $start  refetch=$refetch");

	my @lines = split(/\n/, $body);

	my $line1 = shift @lines;
	if ($line1 !~ /#EXTM3U/) {
		$log->warn("bad m3u file: $url");
		$self->close;
		return;
	}

	my $duration = 0;
	my $lastint;
	my $chunknum;
	my @chunks = ();
	
	my $notendlist = 1;

	while ($#lines >= 0) {
		
		my $line = shift @lines;

		if ($line =~ /#EXT-X-STREAM-INF:(.*)/) {
			foreach my $params (split(/,/, $1)) {
				if ($params =~ /BANDWIDTH=(\d+)/) {
					$is_debug && $log->debug("bandwidth: $1");
					my $stream = ${*$self}{'song'}->streamUrl;
					my $track  = Slim::Schema::RemoteTrack->fetch($stream);
					$track->bitrate($1);
					my $songTrack = ${*$self}{'song'}->track;
					Slim::Music::Info::setBitrate($songTrack, $1);
				} elsif ($params =~ /CODECS="(.*)"/) {
					my $codec = $codecIds->{$1};
					if ($codec) {
						$is_debug && $log->debug("codecs: $1 $codec");
						${*$self}{'song'}->pluginData('codec', $codec);
					}
				}
			}

			my $redurl = shift @lines;
			
			my $baseUrl = _make_BaseUrl($hls_url,$redurl);

#  hack to change the redirected Radio 3 vlow URLs from a 48kbps to a 320kbps
#  this unofficially gives non UK access to 320kbps without VPN
			$baseUrl =~ s/three-audio%3d48000/three-audio%3d320000/	;	

			$is_debug && $log->debug("redirect: $baseUrl");
			$self->_fetchPL($baseUrl, $start);
			return;
		}

		if ($line =~ /#EXT-X-MEDIA-SEQUENCE:(\d+)/) {
			$is_debug && $log->debug("#EXT-X-MEDIA-SEQUENCE: $1");
			$chunknum = $1;
			next;
		}

		if ($line =~ /#EXT-X-ENDLIST/) {
			$is_debug && $log->debug("#EXT-X-ENDLIST");
			${*$self}{'_pl_noupdate'} = 1;
			$notendlist = 0;
		}

		if ($line =~ /#EXTINF:(.*),/) {
			$duration += $1;
			$lastint  =  $1;

			my $chunkurl = shift @lines;

			if ($chunkurl !~ /^http:/) {
				# relative url
				$is_debug && $log->debug("relative url: $chunkurl");
				my ($urlbase) = $url =~ /(.*)\//;
				$chunkurl = $urlbase . "/" . $chunkurl;
				$is_debug && $log->debug("conveted to: $chunkurl");
			}

			if ($chunknum > ${*$self}{'_played'} && (!$start || !defined($duration) || $duration > $start)) {
				push @chunks, { url => $chunkurl, chunknum => $chunknum, len => $1 };
			}
			$chunknum++;
		}
	}
	
	# On first Playlist (i.e. _played = 0 ) and not a live stream (i.e. no ENDLIST tag) - as per IETF Spec. shorten to last 3 segments in playlist if more than 3 segments in the playlist
# This means start playing near to realtime rather than perhaps 30 mins earlier - possibility for later implementation of "skip back"
	if ( (${*$self}{'_played'} == 0 ) && $notendlist && (scalar (@chunks) > 3))  {
		@chunks = splice @chunks, -3 , 3;
	} 

	if (${*$self}{'_pl_noupdate'} && $duration) {
		my $remotetrack = Slim::Schema::RemoteTrack->fetch(${*$self}{'song'}->streamUrl);
		$remotetrack->secs(int($duration + 0.5));
		${*$self}{'song'}->duration(int($duration + 0.5));
	}
	
	
	if ($log->is_info) {
		$log->info(sub { "existing chunks: [" . join(",", map { $_->{'chunknum'} } @{${*$self}{'_chunks'}}) . "]" });
		$log->info(sub { "new chunks: [" . join(",", map { $_->{'chunknum'} } @chunks) . "]" });
	}

	$AnyEvent::HTTP::PERSISTENT_TIMEOUT = HLS_PERSISTENT_TIMEOUT;

	if (scalar @chunks) {

		if (scalar @{${*$self}{'_chunks'}} > 0) {
			
			# only add on chunks which and more recent than existing chunk list
			for my $new (@chunks) {
				if ($new->{'chunknum'} > ${*$self}{'_chunks'}->[-1]->{'chunknum'}) {
					push @{${*$self}{'_chunks'}}, $new;
				};
			}

			if ($log->is_info) {
				$log->info("merged chunklist now " . scalar @{${*$self}{'_chunks'}} . " chunks");
				$log->info(sub { "chunks: [" . join(",", map { $_->{'chunknum'} } @{${*$self}{'_chunks'}}) . "]" });
			}

		} else {

			# new chunklist - fetch initial chunks
			${*$self}{'_chunks'} = \@chunks;

			$is_debug && $log->debug("new chunklist " . scalar @chunks . " chunks");

			for my $i(0 .. PRE_FETCH) {
				$self->_fetchChunk($chunks[$i]) if $chunks[$i];
			}

			${*$self}{'song'}->owner->master->currentPlaylistUpdateTime(Time::HiRes::time());

		}
	}

	if (!${*$self}{'_pl_noupdate'}) {
		${*$self}{'_pl_refetch'} = time() + ($lastint || 10);
		${*$self}{'_pl_url'}     = $url;
	}
}
#
# New fetchChunk which uses AnyEvent::HTTP to use HTTP 1.1 persistent TCP connection.
#
sub _fetchChunk {
	my $self  = shift;
	my $chunk = shift;

	if (my $url = $chunk->{'url'}) {

		$chunk->{'fetching'} = 1;
		$chunk->{'requesttime'} = Time::HiRes::time();
		$chunk->{'requestobj'}  = http_request (	GET => $url,
						headers => { 'User-Agent' => $useragentstring },
						timeout => HTTP_GET_TIMEOUT,
						cookie_jar => ${*$self}{'_cookies'},
					sub {
							my ($body, $hdr) = @_;
							my $fetchtime = int((Time::HiRes::time() - $chunk->{'requesttime'}) *1000);
							
#							Status of 20x means HTTP request completed OK
							if ($hdr->{'Status'} =~ /20[01234]/ ) { 
								if ($log->is_debug && ($fetchtime > HLS_FETCHDELAY_WARNING)) {
									$log->debug("Chunk fetch status ". $hdr->{'Status'} . " Long chunk fetch time $fetchtime" );
								} elsif ($fetchtime > 1500 ) {  # log a message for long fetch delay 1.5sec
									$log->error("Chunk fetch status ". $hdr->{'Status'} . " Long chunk fetch time $fetchtime   ". ${*$self}{'chunkdelay'}   );
								};
								$log->is_info && $log->info("Fetched [$chunk->{chunknum}] size " . length($body));
								delete $chunk->{'fetching'};
								delete $chunk->{'requestobj'};
								$chunk->{'chunkref'} = \$body;
							} elsif ( ($hdr->{'Status'} >= 400) && ($hdr->{'Status'} <= 511)) {
								$log->error("Aborting stream - fatal error fetching [$chunk->{chunknum}] Status=" . $hdr->{'Status'} . " Reason: ". $hdr->{'Reason'}. " Fetch time $fetchtime ms ");
								$log->error("Failed URL: ".$hdr->{'URL'} );
								$self->close;
								delete $chunk->{'fetching'};
								delete $chunk->{'requestobj'};
							} else {
								$log->error("Retrying after error fetching [$chunk->{chunknum}] Status=" . $hdr->{'Status'} . " Reason: ". $hdr->{'Reason'}. " Fetch time $fetchtime ms ");
								delete $chunk->{'fetching'};
								delete $chunk->{'requestobj'};
							}; 	
						}
			);
	}
}

sub isRemote { 1 }

sub isAudio { 1 }

sub canSeek {
	my ($class, $client, $song) = @_;

	return $song->duration ? 1 : 0;
}

sub getSeekData {
	my ($class, $client, $song, $newtime) = @_;
	
	return { timeOffset => $newtime };
}

sub getMetadataFor {
	my $class  = shift;
	my $client = shift;
	my $url    = shift;

	$log->info(" HLS getmetadatafor $url");

	if ($client && (my $song = $client->currentSongForUrl($url))) {
		
		my $baseUrl = $song->pluginData('baseUrl');	
		$log->info(" plugindata BaseURl=$baseUrl");
		my ($type, $params) = $baseUrl =~ /iplayer:\/\/(live|aod)\?(.*)$/;
		my %params = map { $_ =~ /(.*?)=(.*)/; $1 => $2; } split(/&/, $params);
		my $title = uri_unescape_utf8($params{'title'}) || Slim::Music::Info::getCurrentTitle($client, $url) || Slim::Music::Info::title($url);
		my $desc  = uri_unescape_utf8($params{'desc'});
		my $icon = $params{'icon'};

		if ($type eq 'aod')  {
			my $imageres = $prefs->get('imageres');
			$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;			
	
			return {
				title    => $title,
				artist   => $desc,
				cover    => $icon,
				icon     => $icon,
			};
			
		} elsif ($type eq 'live')  {
			if ($song->pluginData('bbconair')  && ! $prefs->get('showmenuicon') && $song->pluginData->{'rp_icon'} ) {
				$icon = $song->pluginData->{'rp_icon'} ;
			}
	
			my $imageres = $prefs->get('imageres');
			$icon =~ s/ichef.bbci.co.uk\/images\/ic\/\d+x\d+\//ichef.bbci.co.uk\/images\/ic\/$imageres\//;			
			return {
				title    => $title . ': '. $song->pluginData('track'),
				artist   => $song->pluginData('artist') ,
				album    => $song->pluginData('album') , 
				cover    => $icon,
				icon     => $icon,
			};
		}
	}
	return undef;
}




sub contentType { 'aac' }

sub formatOverride { 'aac' }

sub getIcon {
	my ($class, $url) = @_;

	my $handler;

	if ( ($handler = Slim::Player::ProtocolHandlers->iconHandlerForURL($url)) && ref $handler eq 'CODE' ) {
		return &{$handler};
	}
	
	return Plugins::BBCiPlayer::Plugin->_pluginDataFor('icon');
}

sub close {
	my $self = shift;
# Just to be safe - explicitly, delete the cookie hash as http_request may still be in progress.
	delete ${*$self}{'_cookies'};
	${*$self}{'_close'} = 1;
}

sub sysread {
	my $self  = $_[0];
	# return in $_[1]
	my $maxBytes = $_[2];

	if (${*$self}{'_close'}) {
		$log->debug("closing");
		return 0;
	}

	my $chunks = ${*$self}{'_chunks'};
	my $chunkref;
	my $ret = 0;

	if (scalar @$chunks) {
		$chunkref = $chunks->[0]->{'chunkref'};
	} elsif (${*$self}{'_pl_noupdate'}) {
		$log->debug("no chunks left - closing");
		return 0;
	}

	if ($chunkref) {

		$_[1] = "";
		my $nextpos = $chunks->[0]->{'nextpos'} ||= 0;

		while ($nextpos <= length($$chunkref) - 188 && length($_[1]) < $maxBytes - 188) {
			my $pre = unpack("N", substr($$chunkref, $nextpos, 4));
			my $pos = $nextpos + 4;
			$nextpos += 188;
			
			if ($pre & 0x00000020) {
				$pos += 1 + unpack("C", substr($$chunkref, $pos, 1));
			}
			if ($pre & 0x00400000) {
				my ($start, $optlen) = unpack("NxxxxC", substr($$chunkref, $pos, 9));
				if ($start == 0x000001c0) { 
					$pos += 9 + $optlen;
				} else {
					next;
				}
			}
			
			$_[1] .= substr($$chunkref, $pos, $nextpos - $pos);
			$ret  += $nextpos - $pos;
		}

		if ($nextpos >= length($$chunkref)) {
			my $played = shift @$chunks;
			${*$self}{'_played'} = $played->{'chunknum'};
			$log->is_info && $log->info("played [$played->{chunknum}]");
		} else {
			$chunks->[0]->{'nextpos'} = $nextpos;
		}
	
	}

	# refetch playlist if not a fixed list
	if (${*$self}{'_pl_refetch'} && time() > ${*$self}{'_pl_refetch'}) {
		$self->_fetchPL(${*$self}{'_pl_url'});
	}

	# fetch more chunks
	for my $i (0 .. PRE_FETCH) {
		my $new = $chunks->[$i];
		if ($new && !$new->{'chunkref'} && !$new->{'fetching'}) {
			$self->_fetchChunk($new);
			last;
		}
	}
	
	return $ret if $ret;

	# otherwise come back later - use EINTR as we don't have a file handle to put on select
	# In July 2018 a bug in Select was fixed - so can return EWOULDBLOCK necessary for no delay with live streams on Windows 10
	$! = IO_SELECT_FIXED ? EWOULDBLOCK : EINTR;

	return undef;
}

1;
